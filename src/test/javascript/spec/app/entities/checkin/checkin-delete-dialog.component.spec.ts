/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { RotaurbanacucumberTestModule } from '../../../test.module';
import { CheckinDeleteDialogComponent } from 'app/entities/checkin/checkin-delete-dialog.component';
import { CheckinService } from 'app/entities/checkin/checkin.service';

describe('Component Tests', () => {
  describe('Checkin Management Delete Component', () => {
    let comp: CheckinDeleteDialogComponent;
    let fixture: ComponentFixture<CheckinDeleteDialogComponent>;
    let service: CheckinService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [RotaurbanacucumberTestModule],
        declarations: [CheckinDeleteDialogComponent]
      })
        .overrideTemplate(CheckinDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(CheckinDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CheckinService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
