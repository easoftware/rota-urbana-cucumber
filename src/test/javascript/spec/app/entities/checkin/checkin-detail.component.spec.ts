/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { RotaurbanacucumberTestModule } from '../../../test.module';
import { CheckinDetailComponent } from 'app/entities/checkin/checkin-detail.component';
import { Checkin } from 'app/shared/model/checkin.model';

describe('Component Tests', () => {
  describe('Checkin Management Detail Component', () => {
    let comp: CheckinDetailComponent;
    let fixture: ComponentFixture<CheckinDetailComponent>;
    const route = ({ data: of({ checkin: new Checkin(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [RotaurbanacucumberTestModule],
        declarations: [CheckinDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(CheckinDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(CheckinDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.checkin).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
