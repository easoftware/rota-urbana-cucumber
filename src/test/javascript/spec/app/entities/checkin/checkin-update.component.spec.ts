/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { RotaurbanacucumberTestModule } from '../../../test.module';
import { CheckinUpdateComponent } from 'app/entities/checkin/checkin-update.component';
import { CheckinService } from 'app/entities/checkin/checkin.service';
import { Checkin } from 'app/shared/model/checkin.model';

describe('Component Tests', () => {
  describe('Checkin Management Update Component', () => {
    let comp: CheckinUpdateComponent;
    let fixture: ComponentFixture<CheckinUpdateComponent>;
    let service: CheckinService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [RotaurbanacucumberTestModule],
        declarations: [CheckinUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(CheckinUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CheckinUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CheckinService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Checkin(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Checkin();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
