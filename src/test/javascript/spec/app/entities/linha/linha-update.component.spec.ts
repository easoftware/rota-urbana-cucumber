/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { RotaurbanacucumberTestModule } from '../../../test.module';
import { LinhaUpdateComponent } from 'app/entities/linha/linha-update.component';
import { LinhaService } from 'app/entities/linha/linha.service';
import { Linha } from 'app/shared/model/linha.model';

describe('Component Tests', () => {
  describe('Linha Management Update Component', () => {
    let comp: LinhaUpdateComponent;
    let fixture: ComponentFixture<LinhaUpdateComponent>;
    let service: LinhaService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [RotaurbanacucumberTestModule],
        declarations: [LinhaUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(LinhaUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(LinhaUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(LinhaService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Linha(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Linha();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
