/* tslint:disable max-line-length */
import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { take, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { LinhaService } from 'app/entities/linha/linha.service';
import { ILinha, Linha, TipoDeRota } from 'app/shared/model/linha.model';

describe('Service Tests', () => {
  describe('Linha Service', () => {
    let injector: TestBed;
    let service: LinhaService;
    let httpMock: HttpTestingController;
    let elemDefault: ILinha;
    let expectedResult;
    let currentDate: moment.Moment;
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = {};
      injector = getTestBed();
      service = injector.get(LinhaService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new Linha(
        0,
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        false,
        currentDate,
        'AAAAAAA',
        'AAAAAAA',
        false,
        false,
        false,
        TipoDeRota.ONIBUS
      );
    });

    describe('Service methods', () => {
      it('should find an element', async () => {
        const returnedFromService = Object.assign(
          {
            lastUpdate: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );
        service
          .find(123)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: elemDefault });
      });

      it('should create a Linha', async () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            lastUpdate: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            lastUpdate: currentDate
          },
          returnedFromService
        );
        service
          .create(new Linha(null))
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should update a Linha', async () => {
        const returnedFromService = Object.assign(
          {
            nome: 'BBBBBB',
            codigo: 'BBBBBB',
            url: 'BBBBBB',
            completa: true,
            lastUpdate: currentDate.format(DATE_TIME_FORMAT),
            comentario: 'BBBBBB',
            itinerarioTotalEncoding: 'BBBBBB',
            emAvaliacao: true,
            faltaCadastrarPontosPesquisa: true,
            semob: true,
            tipoDeRota: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            lastUpdate: currentDate
          },
          returnedFromService
        );
        service
          .update(expected)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should return a list of Linha', async () => {
        const returnedFromService = Object.assign(
          {
            nome: 'BBBBBB',
            codigo: 'BBBBBB',
            url: 'BBBBBB',
            completa: true,
            lastUpdate: currentDate.format(DATE_TIME_FORMAT),
            comentario: 'BBBBBB',
            itinerarioTotalEncoding: 'BBBBBB',
            emAvaliacao: true,
            faltaCadastrarPontosPesquisa: true,
            semob: true,
            tipoDeRota: 'BBBBBB'
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            lastUpdate: currentDate
          },
          returnedFromService
        );
        service
          .query(expected)
          .pipe(
            take(1),
            map(resp => resp.body)
          )
          .subscribe(body => (expectedResult = body));
        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Linha', async () => {
        const rxPromise = service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
