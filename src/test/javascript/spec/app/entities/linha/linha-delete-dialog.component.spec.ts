/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { RotaurbanacucumberTestModule } from '../../../test.module';
import { LinhaDeleteDialogComponent } from 'app/entities/linha/linha-delete-dialog.component';
import { LinhaService } from 'app/entities/linha/linha.service';

describe('Component Tests', () => {
  describe('Linha Management Delete Component', () => {
    let comp: LinhaDeleteDialogComponent;
    let fixture: ComponentFixture<LinhaDeleteDialogComponent>;
    let service: LinhaService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [RotaurbanacucumberTestModule],
        declarations: [LinhaDeleteDialogComponent]
      })
        .overrideTemplate(LinhaDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(LinhaDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(LinhaService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
