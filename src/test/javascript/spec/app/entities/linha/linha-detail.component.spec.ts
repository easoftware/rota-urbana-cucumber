/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { RotaurbanacucumberTestModule } from '../../../test.module';
import { LinhaDetailComponent } from 'app/entities/linha/linha-detail.component';
import { Linha } from 'app/shared/model/linha.model';

describe('Component Tests', () => {
  describe('Linha Management Detail Component', () => {
    let comp: LinhaDetailComponent;
    let fixture: ComponentFixture<LinhaDetailComponent>;
    const route = ({ data: of({ linha: new Linha(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [RotaurbanacucumberTestModule],
        declarations: [LinhaDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(LinhaDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(LinhaDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.linha).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
