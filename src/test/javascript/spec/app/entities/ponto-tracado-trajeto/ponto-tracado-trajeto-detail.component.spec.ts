/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { RotaurbanacucumberTestModule } from '../../../test.module';
import { PontoTracadoTrajetoDetailComponent } from 'app/entities/ponto-tracado-trajeto/ponto-tracado-trajeto-detail.component';
import { PontoTracadoTrajeto } from 'app/shared/model/ponto-tracado-trajeto.model';

describe('Component Tests', () => {
  describe('PontoTracadoTrajeto Management Detail Component', () => {
    let comp: PontoTracadoTrajetoDetailComponent;
    let fixture: ComponentFixture<PontoTracadoTrajetoDetailComponent>;
    const route = ({ data: of({ pontoTracadoTrajeto: new PontoTracadoTrajeto(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [RotaurbanacucumberTestModule],
        declarations: [PontoTracadoTrajetoDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(PontoTracadoTrajetoDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(PontoTracadoTrajetoDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.pontoTracadoTrajeto).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
