/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { RotaurbanacucumberTestModule } from '../../../test.module';
import { PontoTracadoTrajetoDeleteDialogComponent } from 'app/entities/ponto-tracado-trajeto/ponto-tracado-trajeto-delete-dialog.component';
import { PontoTracadoTrajetoService } from 'app/entities/ponto-tracado-trajeto/ponto-tracado-trajeto.service';

describe('Component Tests', () => {
  describe('PontoTracadoTrajeto Management Delete Component', () => {
    let comp: PontoTracadoTrajetoDeleteDialogComponent;
    let fixture: ComponentFixture<PontoTracadoTrajetoDeleteDialogComponent>;
    let service: PontoTracadoTrajetoService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [RotaurbanacucumberTestModule],
        declarations: [PontoTracadoTrajetoDeleteDialogComponent]
      })
        .overrideTemplate(PontoTracadoTrajetoDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(PontoTracadoTrajetoDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(PontoTracadoTrajetoService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
