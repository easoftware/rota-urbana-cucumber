/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { RotaurbanacucumberTestModule } from '../../../test.module';
import { PontoTracadoTrajetoUpdateComponent } from 'app/entities/ponto-tracado-trajeto/ponto-tracado-trajeto-update.component';
import { PontoTracadoTrajetoService } from 'app/entities/ponto-tracado-trajeto/ponto-tracado-trajeto.service';
import { PontoTracadoTrajeto } from 'app/shared/model/ponto-tracado-trajeto.model';

describe('Component Tests', () => {
  describe('PontoTracadoTrajeto Management Update Component', () => {
    let comp: PontoTracadoTrajetoUpdateComponent;
    let fixture: ComponentFixture<PontoTracadoTrajetoUpdateComponent>;
    let service: PontoTracadoTrajetoService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [RotaurbanacucumberTestModule],
        declarations: [PontoTracadoTrajetoUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(PontoTracadoTrajetoUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(PontoTracadoTrajetoUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(PontoTracadoTrajetoService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new PontoTracadoTrajeto(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new PontoTracadoTrajeto();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
