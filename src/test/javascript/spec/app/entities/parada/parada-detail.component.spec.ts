/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { RotaurbanacucumberTestModule } from '../../../test.module';
import { ParadaDetailComponent } from 'app/entities/parada/parada-detail.component';
import { Parada } from 'app/shared/model/parada.model';

describe('Component Tests', () => {
  describe('Parada Management Detail Component', () => {
    let comp: ParadaDetailComponent;
    let fixture: ComponentFixture<ParadaDetailComponent>;
    const route = ({ data: of({ parada: new Parada(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [RotaurbanacucumberTestModule],
        declarations: [ParadaDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(ParadaDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ParadaDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.parada).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
