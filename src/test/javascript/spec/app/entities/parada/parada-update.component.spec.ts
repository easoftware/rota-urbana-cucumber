/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { RotaurbanacucumberTestModule } from '../../../test.module';
import { ParadaUpdateComponent } from 'app/entities/parada/parada-update.component';
import { ParadaService } from 'app/entities/parada/parada.service';
import { Parada } from 'app/shared/model/parada.model';

describe('Component Tests', () => {
  describe('Parada Management Update Component', () => {
    let comp: ParadaUpdateComponent;
    let fixture: ComponentFixture<ParadaUpdateComponent>;
    let service: ParadaService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [RotaurbanacucumberTestModule],
        declarations: [ParadaUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(ParadaUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ParadaUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ParadaService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Parada(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Parada();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
