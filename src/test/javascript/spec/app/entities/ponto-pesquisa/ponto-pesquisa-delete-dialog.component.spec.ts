/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { RotaurbanacucumberTestModule } from '../../../test.module';
import { PontoPesquisaDeleteDialogComponent } from 'app/entities/ponto-pesquisa/ponto-pesquisa-delete-dialog.component';
import { PontoPesquisaService } from 'app/entities/ponto-pesquisa/ponto-pesquisa.service';

describe('Component Tests', () => {
  describe('PontoPesquisa Management Delete Component', () => {
    let comp: PontoPesquisaDeleteDialogComponent;
    let fixture: ComponentFixture<PontoPesquisaDeleteDialogComponent>;
    let service: PontoPesquisaService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [RotaurbanacucumberTestModule],
        declarations: [PontoPesquisaDeleteDialogComponent]
      })
        .overrideTemplate(PontoPesquisaDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(PontoPesquisaDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(PontoPesquisaService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
