/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { RotaurbanacucumberTestModule } from '../../../test.module';
import { PontoPesquisaUpdateComponent } from 'app/entities/ponto-pesquisa/ponto-pesquisa-update.component';
import { PontoPesquisaService } from 'app/entities/ponto-pesquisa/ponto-pesquisa.service';
import { PontoPesquisa } from 'app/shared/model/ponto-pesquisa.model';

describe('Component Tests', () => {
  describe('PontoPesquisa Management Update Component', () => {
    let comp: PontoPesquisaUpdateComponent;
    let fixture: ComponentFixture<PontoPesquisaUpdateComponent>;
    let service: PontoPesquisaService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [RotaurbanacucumberTestModule],
        declarations: [PontoPesquisaUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(PontoPesquisaUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(PontoPesquisaUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(PontoPesquisaService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new PontoPesquisa(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new PontoPesquisa();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
