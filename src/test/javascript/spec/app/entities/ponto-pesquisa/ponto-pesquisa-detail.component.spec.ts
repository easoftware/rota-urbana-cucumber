/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { RotaurbanacucumberTestModule } from '../../../test.module';
import { PontoPesquisaDetailComponent } from 'app/entities/ponto-pesquisa/ponto-pesquisa-detail.component';
import { PontoPesquisa } from 'app/shared/model/ponto-pesquisa.model';

describe('Component Tests', () => {
  describe('PontoPesquisa Management Detail Component', () => {
    let comp: PontoPesquisaDetailComponent;
    let fixture: ComponentFixture<PontoPesquisaDetailComponent>;
    const route = ({ data: of({ pontoPesquisa: new PontoPesquisa(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [RotaurbanacucumberTestModule],
        declarations: [PontoPesquisaDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(PontoPesquisaDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(PontoPesquisaDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.pontoPesquisa).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
