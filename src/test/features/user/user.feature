Feature: User management

    Scenario: Retrieve administrator user
        When Eu busco usuario 'admin'
        Then the user is found
        And his last name is 'Administrator'

    Scenario: Retrieve desconhecido
        When Eu busco usuario 'adailton'
        Then the user is not found

    Scenario: Retrieve administrator user role
        When Eu busco usuario 'admin'
        Then the user is found
        And his role name contains 'ROLE_USER'

    Scenario: Retrieve administrator ADMIN role
        When Eu busco usuario 'admin'
        Then the user is found
        And his role name contains 'ROLE_ADMIN'

    Scenario: Retrieve administrator email
        When Eu busco usuario 'admin'
        Then the user is found
        And his email contains 'admin@localhost'

    Scenario: Retrieve administrator roles size
        When Eu busco usuario 'admin'
        Then the user is found
        And his role list size is 2

    Scenario: Retrieve user and validate name and roles
        When Eu busco usuario 'user'
        Then the user is found
        And his role list size is 1
        And his role name contains 'ROLE_USER'
        And his last name is 'User'


    Scenario Outline: Retrieve administrator user
        When Eu busco usuario '<user>'
        Then the user is found
        And his last name is '<name>'
        Examples:
            | user   | name          |
            | admin  | Administrator |
            | user   | User          |
