Feature: Gerenciamento de cadastro de Pais

    Scenario: Buscar Brasil
        When busco pais por sigla 'BR'
        Then o pais é encontrado
        And o seu nome é igual a 'Brasil' OU 'BRASIL'

    Scenario: Buscar Brasil
        When busco pais por sigla 'AR'
        Then o pais é encontrado
        And o seu nome é igual a 'Argentina' OU 'ARGENTINA'

    Scenario: Buscar um que não existe
        When busco pais por sigla 'XX'
        Then o pais não é encontrado

