package br.com.rotaurbana.cucumber.stepdefs;

import br.com.rotaurbana.web.rest.LinhaResource;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.hamcrest.Matchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class LinhaStepDefs extends StepDefs {

    @Autowired
    private LinhaResource linhaResource;

    private MockMvc restUserMockMvc;

    @Before
    public void setup() {
        this.restUserMockMvc = MockMvcBuilders.standaloneSetup(linhaResource).build();
    }

    @When("I search linha {long}")
    public void i_search_user(Long id) throws Throwable {
        actions = restUserMockMvc.perform(get("/api/linhas/" + id)
                .accept(MediaType.APPLICATION_JSON));
    }

    @Then("the linha is not found")
    public void the_linha_is_found() throws Throwable {
        actions
            .andExpect(status().isNotFound());
    }



    @Then("his codigo is {string}")
    public void his_last_name_is(String codigo) throws Throwable {
        actions.andExpect(jsonPath("$.codigo").value(codigo));
    }


}
