package br.com.rotaurbana.cucumber.stepdefs;

import br.com.rotaurbana.domain.Pais;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class PaisStepDefs extends StepDefs {

    Pais resultado ;

    @When("busco pais por sigla {string}")
    public void pais_busca(String sigla) throws Throwable {
        if(sigla.equals("BR")){
            resultado = new Pais();
            resultado.setSigla("BR");
            resultado.setNome("Brasil");
        }else if(sigla.equals("AR")){
            resultado = new Pais();
            resultado.setSigla("AR");
            resultado.setNome("Argentina");
        }

    }

    @Then("o pais é encontrado")
    public void the_pais_is_found() throws Throwable {
        if(resultado==null)
            throw new Exception();
    }

    @Then("o pais não é encontrado")
    public void the_pais_is_not_found() throws Throwable {
        if(resultado!=null)
            throw new Exception();
    }

    @And("o seu nome é igual a {string} OU {string}")
    public void nomeIgual(String nome1, String nome2) throws Throwable {
        if( !resultado.getNome().equals(nome1) && !resultado.getNome().equals(nome2) )
            throw new Exception();
    }

}
