package br.com.rotaurbana.cucumber.stepdefs;

import cucumber.api.java.Before;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import org.hamcrest.Matchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import br.com.rotaurbana.web.rest.UserResource;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class UserStepDefs extends StepDefs {

    @Autowired
    private UserResource userResource;

    private MockMvc restUserMockMvc;

    @Before
    public void setup() {
        this.restUserMockMvc = MockMvcBuilders.standaloneSetup(userResource).build();
    }

    @When("Eu busco usuario {string}")
    public void i_search_user(String userId) throws Throwable {
        actions = restUserMockMvc.perform(get("/api/users/" + userId)
                .accept(MediaType.APPLICATION_JSON));
    }

    @Then("the user is found")
    public void the_user_is_found() throws Throwable {
        actions
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
    }

    @Then("the user is not found")
    public void the_user_is_not_found() throws Throwable {
        actions
            .andExpect(status().isNotFound());
    }

    @Then("his last name is {string}")
    public void his_last_name_is(String lastName) throws Throwable {
        actions.andExpect(jsonPath("$.lastName").value(lastName));
    }

    @Then("his role name contains {string}")
    public void his_role_contains(String role) throws Throwable {
        actions.andExpect(jsonPath("$.authorities", Matchers.hasItem(role)));
    }

    @Then("his email contains {string}")
    public void his_email_contains(String email) throws Throwable {
        actions.andExpect(jsonPath("$.email").value(email));
    }

    @Then("his role list size is {int}")
    public void his_role_list_size(Integer size) throws Throwable {
        actions.andExpect(jsonPath("$.authorities.length()").value(size));
    }
}
