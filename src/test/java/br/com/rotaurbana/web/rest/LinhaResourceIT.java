package br.com.rotaurbana.web.rest;

import br.com.rotaurbana.RotaurbanacucumberApp;
import br.com.rotaurbana.domain.Linha;
import br.com.rotaurbana.domain.PontoTracadoTrajeto;
import br.com.rotaurbana.domain.PontoPesquisa;
import br.com.rotaurbana.domain.Cidade;
import br.com.rotaurbana.domain.Linha;
import br.com.rotaurbana.domain.Parada;
import br.com.rotaurbana.repository.LinhaRepository;
import br.com.rotaurbana.service.LinhaService;
import br.com.rotaurbana.service.dto.LinhaDTO;
import br.com.rotaurbana.service.mapper.LinhaMapper;
import br.com.rotaurbana.web.rest.errors.ExceptionTranslator;
import br.com.rotaurbana.service.dto.LinhaCriteria;
import br.com.rotaurbana.service.LinhaQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import static br.com.rotaurbana.web.rest.TestUtil.sameInstant;
import static br.com.rotaurbana.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import br.com.rotaurbana.domain.enumeration.TipoDeRota;
/**
 * Integration tests for the {@Link LinhaResource} REST controller.
 */
@SpringBootTest(classes = RotaurbanacucumberApp.class)
public class LinhaResourceIT {

    private static final String DEFAULT_NOME = "AAAAAAAAAA";
    private static final String UPDATED_NOME = "BBBBBBBBBB";

    private static final String DEFAULT_CODIGO = "AAAAAAAAAA";
    private static final String UPDATED_CODIGO = "BBBBBBBBBB";

    private static final String DEFAULT_URL = "AAAAAAAAAA";
    private static final String UPDATED_URL = "BBBBBBBBBB";

    private static final Boolean DEFAULT_COMPLETA = false;
    private static final Boolean UPDATED_COMPLETA = true;

    private static final ZonedDateTime DEFAULT_LAST_UPDATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_LAST_UPDATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_COMENTARIO = "AAAAAAAAAA";
    private static final String UPDATED_COMENTARIO = "BBBBBBBBBB";

    private static final String DEFAULT_ITINERARIO_TOTAL_ENCODING = "AAAAAAAAAA";
    private static final String UPDATED_ITINERARIO_TOTAL_ENCODING = "BBBBBBBBBB";

    private static final Boolean DEFAULT_EM_AVALIACAO = false;
    private static final Boolean UPDATED_EM_AVALIACAO = true;

    private static final Boolean DEFAULT_FALTA_CADASTRAR_PONTOS_PESQUISA = false;
    private static final Boolean UPDATED_FALTA_CADASTRAR_PONTOS_PESQUISA = true;

    private static final Boolean DEFAULT_SEMOB = false;
    private static final Boolean UPDATED_SEMOB = true;

    private static final TipoDeRota DEFAULT_TIPO_DE_ROTA = TipoDeRota.ONIBUS;
    private static final TipoDeRota UPDATED_TIPO_DE_ROTA = TipoDeRota.CIRCUITO;

    @Autowired
    private LinhaRepository linhaRepository;

    @Mock
    private LinhaRepository linhaRepositoryMock;

    @Autowired
    private LinhaMapper linhaMapper;

    @Mock
    private LinhaService linhaServiceMock;

    @Autowired
    private LinhaService linhaService;

    @Autowired
    private LinhaQueryService linhaQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restLinhaMockMvc;

    private Linha linha;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final LinhaResource linhaResource = new LinhaResource(linhaService, linhaQueryService);
        this.restLinhaMockMvc = MockMvcBuilders.standaloneSetup(linhaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Linha createEntity(EntityManager em) {
        Linha linha = new Linha()
            .nome(DEFAULT_NOME)
            .codigo(DEFAULT_CODIGO)
            .url(DEFAULT_URL)
            .completa(DEFAULT_COMPLETA)
            .lastUpdate(DEFAULT_LAST_UPDATE)
            .comentario(DEFAULT_COMENTARIO)
            .itinerarioTotalEncoding(DEFAULT_ITINERARIO_TOTAL_ENCODING)
            .emAvaliacao(DEFAULT_EM_AVALIACAO)
            .faltaCadastrarPontosPesquisa(DEFAULT_FALTA_CADASTRAR_PONTOS_PESQUISA)
            .semob(DEFAULT_SEMOB)
            .tipoDeRota(DEFAULT_TIPO_DE_ROTA);
        return linha;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Linha createUpdatedEntity(EntityManager em) {
        Linha linha = new Linha()
            .nome(UPDATED_NOME)
            .codigo(UPDATED_CODIGO)
            .url(UPDATED_URL)
            .completa(UPDATED_COMPLETA)
            .lastUpdate(UPDATED_LAST_UPDATE)
            .comentario(UPDATED_COMENTARIO)
            .itinerarioTotalEncoding(UPDATED_ITINERARIO_TOTAL_ENCODING)
            .emAvaliacao(UPDATED_EM_AVALIACAO)
            .faltaCadastrarPontosPesquisa(UPDATED_FALTA_CADASTRAR_PONTOS_PESQUISA)
            .semob(UPDATED_SEMOB)
            .tipoDeRota(UPDATED_TIPO_DE_ROTA);
        return linha;
    }

    @BeforeEach
    public void initTest() {
        linha = createEntity(em);
    }

    @Test
    @Transactional
    public void createLinha() throws Exception {
        int databaseSizeBeforeCreate = linhaRepository.findAll().size();

        // Create the Linha
        LinhaDTO linhaDTO = linhaMapper.toDto(linha);
        restLinhaMockMvc.perform(post("/api/linhas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(linhaDTO)))
            .andExpect(status().isCreated());

        // Validate the Linha in the database
        List<Linha> linhaList = linhaRepository.findAll();
        assertThat(linhaList).hasSize(databaseSizeBeforeCreate + 1);
        Linha testLinha = linhaList.get(linhaList.size() - 1);
        assertThat(testLinha.getNome()).isEqualTo(DEFAULT_NOME);
        assertThat(testLinha.getCodigo()).isEqualTo(DEFAULT_CODIGO);
        assertThat(testLinha.getUrl()).isEqualTo(DEFAULT_URL);
        assertThat(testLinha.isCompleta()).isEqualTo(DEFAULT_COMPLETA);
        assertThat(testLinha.getLastUpdate()).isEqualTo(DEFAULT_LAST_UPDATE);
        assertThat(testLinha.getComentario()).isEqualTo(DEFAULT_COMENTARIO);
        assertThat(testLinha.getItinerarioTotalEncoding()).isEqualTo(DEFAULT_ITINERARIO_TOTAL_ENCODING);
        assertThat(testLinha.isEmAvaliacao()).isEqualTo(DEFAULT_EM_AVALIACAO);
        assertThat(testLinha.isFaltaCadastrarPontosPesquisa()).isEqualTo(DEFAULT_FALTA_CADASTRAR_PONTOS_PESQUISA);
        assertThat(testLinha.isSemob()).isEqualTo(DEFAULT_SEMOB);
        assertThat(testLinha.getTipoDeRota()).isEqualTo(DEFAULT_TIPO_DE_ROTA);
    }

    @Test
    @Transactional
    public void createLinhaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = linhaRepository.findAll().size();

        // Create the Linha with an existing ID
        linha.setId(1L);
        LinhaDTO linhaDTO = linhaMapper.toDto(linha);

        // An entity with an existing ID cannot be created, so this API call must fail
        restLinhaMockMvc.perform(post("/api/linhas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(linhaDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Linha in the database
        List<Linha> linhaList = linhaRepository.findAll();
        assertThat(linhaList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllLinhas() throws Exception {
        // Initialize the database
        linhaRepository.saveAndFlush(linha);

        // Get all the linhaList
        restLinhaMockMvc.perform(get("/api/linhas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(linha.getId().intValue())))
            .andExpect(jsonPath("$.[*].nome").value(hasItem(DEFAULT_NOME.toString())))
            .andExpect(jsonPath("$.[*].codigo").value(hasItem(DEFAULT_CODIGO.toString())))
            .andExpect(jsonPath("$.[*].url").value(hasItem(DEFAULT_URL.toString())))
            .andExpect(jsonPath("$.[*].completa").value(hasItem(DEFAULT_COMPLETA.booleanValue())))
            .andExpect(jsonPath("$.[*].lastUpdate").value(hasItem(sameInstant(DEFAULT_LAST_UPDATE))))
            .andExpect(jsonPath("$.[*].comentario").value(hasItem(DEFAULT_COMENTARIO.toString())))
            .andExpect(jsonPath("$.[*].itinerarioTotalEncoding").value(hasItem(DEFAULT_ITINERARIO_TOTAL_ENCODING.toString())))
            .andExpect(jsonPath("$.[*].emAvaliacao").value(hasItem(DEFAULT_EM_AVALIACAO.booleanValue())))
            .andExpect(jsonPath("$.[*].faltaCadastrarPontosPesquisa").value(hasItem(DEFAULT_FALTA_CADASTRAR_PONTOS_PESQUISA.booleanValue())))
            .andExpect(jsonPath("$.[*].semob").value(hasItem(DEFAULT_SEMOB.booleanValue())))
            .andExpect(jsonPath("$.[*].tipoDeRota").value(hasItem(DEFAULT_TIPO_DE_ROTA.toString())));
    }
    
    @SuppressWarnings({"unchecked"})
    public void getAllLinhasWithEagerRelationshipsIsEnabled() throws Exception {
        LinhaResource linhaResource = new LinhaResource(linhaServiceMock, linhaQueryService);
        when(linhaServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        MockMvc restLinhaMockMvc = MockMvcBuilders.standaloneSetup(linhaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restLinhaMockMvc.perform(get("/api/linhas?eagerload=true"))
        .andExpect(status().isOk());

        verify(linhaServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({"unchecked"})
    public void getAllLinhasWithEagerRelationshipsIsNotEnabled() throws Exception {
        LinhaResource linhaResource = new LinhaResource(linhaServiceMock, linhaQueryService);
            when(linhaServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));
            MockMvc restLinhaMockMvc = MockMvcBuilders.standaloneSetup(linhaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restLinhaMockMvc.perform(get("/api/linhas?eagerload=true"))
        .andExpect(status().isOk());

            verify(linhaServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    public void getLinha() throws Exception {
        // Initialize the database
        linhaRepository.saveAndFlush(linha);

        // Get the linha
        restLinhaMockMvc.perform(get("/api/linhas/{id}", linha.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(linha.getId().intValue()))
            .andExpect(jsonPath("$.nome").value(DEFAULT_NOME.toString()))
            .andExpect(jsonPath("$.codigo").value(DEFAULT_CODIGO.toString()))
            .andExpect(jsonPath("$.url").value(DEFAULT_URL.toString()))
            .andExpect(jsonPath("$.completa").value(DEFAULT_COMPLETA.booleanValue()))
            .andExpect(jsonPath("$.lastUpdate").value(sameInstant(DEFAULT_LAST_UPDATE)))
            .andExpect(jsonPath("$.comentario").value(DEFAULT_COMENTARIO.toString()))
            .andExpect(jsonPath("$.itinerarioTotalEncoding").value(DEFAULT_ITINERARIO_TOTAL_ENCODING.toString()))
            .andExpect(jsonPath("$.emAvaliacao").value(DEFAULT_EM_AVALIACAO.booleanValue()))
            .andExpect(jsonPath("$.faltaCadastrarPontosPesquisa").value(DEFAULT_FALTA_CADASTRAR_PONTOS_PESQUISA.booleanValue()))
            .andExpect(jsonPath("$.semob").value(DEFAULT_SEMOB.booleanValue()))
            .andExpect(jsonPath("$.tipoDeRota").value(DEFAULT_TIPO_DE_ROTA.toString()));
    }

    @Test
    @Transactional
    public void getAllLinhasByNomeIsEqualToSomething() throws Exception {
        // Initialize the database
        linhaRepository.saveAndFlush(linha);

        // Get all the linhaList where nome equals to DEFAULT_NOME
        defaultLinhaShouldBeFound("nome.equals=" + DEFAULT_NOME);

        // Get all the linhaList where nome equals to UPDATED_NOME
        defaultLinhaShouldNotBeFound("nome.equals=" + UPDATED_NOME);
    }

    @Test
    @Transactional
    public void getAllLinhasByNomeIsInShouldWork() throws Exception {
        // Initialize the database
        linhaRepository.saveAndFlush(linha);

        // Get all the linhaList where nome in DEFAULT_NOME or UPDATED_NOME
        defaultLinhaShouldBeFound("nome.in=" + DEFAULT_NOME + "," + UPDATED_NOME);

        // Get all the linhaList where nome equals to UPDATED_NOME
        defaultLinhaShouldNotBeFound("nome.in=" + UPDATED_NOME);
    }

    @Test
    @Transactional
    public void getAllLinhasByNomeIsNullOrNotNull() throws Exception {
        // Initialize the database
        linhaRepository.saveAndFlush(linha);

        // Get all the linhaList where nome is not null
        defaultLinhaShouldBeFound("nome.specified=true");

        // Get all the linhaList where nome is null
        defaultLinhaShouldNotBeFound("nome.specified=false");
    }

    @Test
    @Transactional
    public void getAllLinhasByCodigoIsEqualToSomething() throws Exception {
        // Initialize the database
        linhaRepository.saveAndFlush(linha);

        // Get all the linhaList where codigo equals to DEFAULT_CODIGO
        defaultLinhaShouldBeFound("codigo.equals=" + DEFAULT_CODIGO);

        // Get all the linhaList where codigo equals to UPDATED_CODIGO
        defaultLinhaShouldNotBeFound("codigo.equals=" + UPDATED_CODIGO);
    }

    @Test
    @Transactional
    public void getAllLinhasByCodigoIsInShouldWork() throws Exception {
        // Initialize the database
        linhaRepository.saveAndFlush(linha);

        // Get all the linhaList where codigo in DEFAULT_CODIGO or UPDATED_CODIGO
        defaultLinhaShouldBeFound("codigo.in=" + DEFAULT_CODIGO + "," + UPDATED_CODIGO);

        // Get all the linhaList where codigo equals to UPDATED_CODIGO
        defaultLinhaShouldNotBeFound("codigo.in=" + UPDATED_CODIGO);
    }

    @Test
    @Transactional
    public void getAllLinhasByCodigoIsNullOrNotNull() throws Exception {
        // Initialize the database
        linhaRepository.saveAndFlush(linha);

        // Get all the linhaList where codigo is not null
        defaultLinhaShouldBeFound("codigo.specified=true");

        // Get all the linhaList where codigo is null
        defaultLinhaShouldNotBeFound("codigo.specified=false");
    }

    @Test
    @Transactional
    public void getAllLinhasByUrlIsEqualToSomething() throws Exception {
        // Initialize the database
        linhaRepository.saveAndFlush(linha);

        // Get all the linhaList where url equals to DEFAULT_URL
        defaultLinhaShouldBeFound("url.equals=" + DEFAULT_URL);

        // Get all the linhaList where url equals to UPDATED_URL
        defaultLinhaShouldNotBeFound("url.equals=" + UPDATED_URL);
    }

    @Test
    @Transactional
    public void getAllLinhasByUrlIsInShouldWork() throws Exception {
        // Initialize the database
        linhaRepository.saveAndFlush(linha);

        // Get all the linhaList where url in DEFAULT_URL or UPDATED_URL
        defaultLinhaShouldBeFound("url.in=" + DEFAULT_URL + "," + UPDATED_URL);

        // Get all the linhaList where url equals to UPDATED_URL
        defaultLinhaShouldNotBeFound("url.in=" + UPDATED_URL);
    }

    @Test
    @Transactional
    public void getAllLinhasByUrlIsNullOrNotNull() throws Exception {
        // Initialize the database
        linhaRepository.saveAndFlush(linha);

        // Get all the linhaList where url is not null
        defaultLinhaShouldBeFound("url.specified=true");

        // Get all the linhaList where url is null
        defaultLinhaShouldNotBeFound("url.specified=false");
    }

    @Test
    @Transactional
    public void getAllLinhasByCompletaIsEqualToSomething() throws Exception {
        // Initialize the database
        linhaRepository.saveAndFlush(linha);

        // Get all the linhaList where completa equals to DEFAULT_COMPLETA
        defaultLinhaShouldBeFound("completa.equals=" + DEFAULT_COMPLETA);

        // Get all the linhaList where completa equals to UPDATED_COMPLETA
        defaultLinhaShouldNotBeFound("completa.equals=" + UPDATED_COMPLETA);
    }

    @Test
    @Transactional
    public void getAllLinhasByCompletaIsInShouldWork() throws Exception {
        // Initialize the database
        linhaRepository.saveAndFlush(linha);

        // Get all the linhaList where completa in DEFAULT_COMPLETA or UPDATED_COMPLETA
        defaultLinhaShouldBeFound("completa.in=" + DEFAULT_COMPLETA + "," + UPDATED_COMPLETA);

        // Get all the linhaList where completa equals to UPDATED_COMPLETA
        defaultLinhaShouldNotBeFound("completa.in=" + UPDATED_COMPLETA);
    }

    @Test
    @Transactional
    public void getAllLinhasByCompletaIsNullOrNotNull() throws Exception {
        // Initialize the database
        linhaRepository.saveAndFlush(linha);

        // Get all the linhaList where completa is not null
        defaultLinhaShouldBeFound("completa.specified=true");

        // Get all the linhaList where completa is null
        defaultLinhaShouldNotBeFound("completa.specified=false");
    }

    @Test
    @Transactional
    public void getAllLinhasByLastUpdateIsEqualToSomething() throws Exception {
        // Initialize the database
        linhaRepository.saveAndFlush(linha);

        // Get all the linhaList where lastUpdate equals to DEFAULT_LAST_UPDATE
        defaultLinhaShouldBeFound("lastUpdate.equals=" + DEFAULT_LAST_UPDATE);

        // Get all the linhaList where lastUpdate equals to UPDATED_LAST_UPDATE
        defaultLinhaShouldNotBeFound("lastUpdate.equals=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllLinhasByLastUpdateIsInShouldWork() throws Exception {
        // Initialize the database
        linhaRepository.saveAndFlush(linha);

        // Get all the linhaList where lastUpdate in DEFAULT_LAST_UPDATE or UPDATED_LAST_UPDATE
        defaultLinhaShouldBeFound("lastUpdate.in=" + DEFAULT_LAST_UPDATE + "," + UPDATED_LAST_UPDATE);

        // Get all the linhaList where lastUpdate equals to UPDATED_LAST_UPDATE
        defaultLinhaShouldNotBeFound("lastUpdate.in=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllLinhasByLastUpdateIsNullOrNotNull() throws Exception {
        // Initialize the database
        linhaRepository.saveAndFlush(linha);

        // Get all the linhaList where lastUpdate is not null
        defaultLinhaShouldBeFound("lastUpdate.specified=true");

        // Get all the linhaList where lastUpdate is null
        defaultLinhaShouldNotBeFound("lastUpdate.specified=false");
    }

    @Test
    @Transactional
    public void getAllLinhasByLastUpdateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        linhaRepository.saveAndFlush(linha);

        // Get all the linhaList where lastUpdate greater than or equals to DEFAULT_LAST_UPDATE
        defaultLinhaShouldBeFound("lastUpdate.greaterOrEqualThan=" + DEFAULT_LAST_UPDATE);

        // Get all the linhaList where lastUpdate greater than or equals to UPDATED_LAST_UPDATE
        defaultLinhaShouldNotBeFound("lastUpdate.greaterOrEqualThan=" + UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void getAllLinhasByLastUpdateIsLessThanSomething() throws Exception {
        // Initialize the database
        linhaRepository.saveAndFlush(linha);

        // Get all the linhaList where lastUpdate less than or equals to DEFAULT_LAST_UPDATE
        defaultLinhaShouldNotBeFound("lastUpdate.lessThan=" + DEFAULT_LAST_UPDATE);

        // Get all the linhaList where lastUpdate less than or equals to UPDATED_LAST_UPDATE
        defaultLinhaShouldBeFound("lastUpdate.lessThan=" + UPDATED_LAST_UPDATE);
    }


    @Test
    @Transactional
    public void getAllLinhasByComentarioIsEqualToSomething() throws Exception {
        // Initialize the database
        linhaRepository.saveAndFlush(linha);

        // Get all the linhaList where comentario equals to DEFAULT_COMENTARIO
        defaultLinhaShouldBeFound("comentario.equals=" + DEFAULT_COMENTARIO);

        // Get all the linhaList where comentario equals to UPDATED_COMENTARIO
        defaultLinhaShouldNotBeFound("comentario.equals=" + UPDATED_COMENTARIO);
    }

    @Test
    @Transactional
    public void getAllLinhasByComentarioIsInShouldWork() throws Exception {
        // Initialize the database
        linhaRepository.saveAndFlush(linha);

        // Get all the linhaList where comentario in DEFAULT_COMENTARIO or UPDATED_COMENTARIO
        defaultLinhaShouldBeFound("comentario.in=" + DEFAULT_COMENTARIO + "," + UPDATED_COMENTARIO);

        // Get all the linhaList where comentario equals to UPDATED_COMENTARIO
        defaultLinhaShouldNotBeFound("comentario.in=" + UPDATED_COMENTARIO);
    }

    @Test
    @Transactional
    public void getAllLinhasByComentarioIsNullOrNotNull() throws Exception {
        // Initialize the database
        linhaRepository.saveAndFlush(linha);

        // Get all the linhaList where comentario is not null
        defaultLinhaShouldBeFound("comentario.specified=true");

        // Get all the linhaList where comentario is null
        defaultLinhaShouldNotBeFound("comentario.specified=false");
    }

    @Test
    @Transactional
    public void getAllLinhasByItinerarioTotalEncodingIsEqualToSomething() throws Exception {
        // Initialize the database
        linhaRepository.saveAndFlush(linha);

        // Get all the linhaList where itinerarioTotalEncoding equals to DEFAULT_ITINERARIO_TOTAL_ENCODING
        defaultLinhaShouldBeFound("itinerarioTotalEncoding.equals=" + DEFAULT_ITINERARIO_TOTAL_ENCODING);

        // Get all the linhaList where itinerarioTotalEncoding equals to UPDATED_ITINERARIO_TOTAL_ENCODING
        defaultLinhaShouldNotBeFound("itinerarioTotalEncoding.equals=" + UPDATED_ITINERARIO_TOTAL_ENCODING);
    }

    @Test
    @Transactional
    public void getAllLinhasByItinerarioTotalEncodingIsInShouldWork() throws Exception {
        // Initialize the database
        linhaRepository.saveAndFlush(linha);

        // Get all the linhaList where itinerarioTotalEncoding in DEFAULT_ITINERARIO_TOTAL_ENCODING or UPDATED_ITINERARIO_TOTAL_ENCODING
        defaultLinhaShouldBeFound("itinerarioTotalEncoding.in=" + DEFAULT_ITINERARIO_TOTAL_ENCODING + "," + UPDATED_ITINERARIO_TOTAL_ENCODING);

        // Get all the linhaList where itinerarioTotalEncoding equals to UPDATED_ITINERARIO_TOTAL_ENCODING
        defaultLinhaShouldNotBeFound("itinerarioTotalEncoding.in=" + UPDATED_ITINERARIO_TOTAL_ENCODING);
    }

    @Test
    @Transactional
    public void getAllLinhasByItinerarioTotalEncodingIsNullOrNotNull() throws Exception {
        // Initialize the database
        linhaRepository.saveAndFlush(linha);

        // Get all the linhaList where itinerarioTotalEncoding is not null
        defaultLinhaShouldBeFound("itinerarioTotalEncoding.specified=true");

        // Get all the linhaList where itinerarioTotalEncoding is null
        defaultLinhaShouldNotBeFound("itinerarioTotalEncoding.specified=false");
    }

    @Test
    @Transactional
    public void getAllLinhasByEmAvaliacaoIsEqualToSomething() throws Exception {
        // Initialize the database
        linhaRepository.saveAndFlush(linha);

        // Get all the linhaList where emAvaliacao equals to DEFAULT_EM_AVALIACAO
        defaultLinhaShouldBeFound("emAvaliacao.equals=" + DEFAULT_EM_AVALIACAO);

        // Get all the linhaList where emAvaliacao equals to UPDATED_EM_AVALIACAO
        defaultLinhaShouldNotBeFound("emAvaliacao.equals=" + UPDATED_EM_AVALIACAO);
    }

    @Test
    @Transactional
    public void getAllLinhasByEmAvaliacaoIsInShouldWork() throws Exception {
        // Initialize the database
        linhaRepository.saveAndFlush(linha);

        // Get all the linhaList where emAvaliacao in DEFAULT_EM_AVALIACAO or UPDATED_EM_AVALIACAO
        defaultLinhaShouldBeFound("emAvaliacao.in=" + DEFAULT_EM_AVALIACAO + "," + UPDATED_EM_AVALIACAO);

        // Get all the linhaList where emAvaliacao equals to UPDATED_EM_AVALIACAO
        defaultLinhaShouldNotBeFound("emAvaliacao.in=" + UPDATED_EM_AVALIACAO);
    }

    @Test
    @Transactional
    public void getAllLinhasByEmAvaliacaoIsNullOrNotNull() throws Exception {
        // Initialize the database
        linhaRepository.saveAndFlush(linha);

        // Get all the linhaList where emAvaliacao is not null
        defaultLinhaShouldBeFound("emAvaliacao.specified=true");

        // Get all the linhaList where emAvaliacao is null
        defaultLinhaShouldNotBeFound("emAvaliacao.specified=false");
    }

    @Test
    @Transactional
    public void getAllLinhasByFaltaCadastrarPontosPesquisaIsEqualToSomething() throws Exception {
        // Initialize the database
        linhaRepository.saveAndFlush(linha);

        // Get all the linhaList where faltaCadastrarPontosPesquisa equals to DEFAULT_FALTA_CADASTRAR_PONTOS_PESQUISA
        defaultLinhaShouldBeFound("faltaCadastrarPontosPesquisa.equals=" + DEFAULT_FALTA_CADASTRAR_PONTOS_PESQUISA);

        // Get all the linhaList where faltaCadastrarPontosPesquisa equals to UPDATED_FALTA_CADASTRAR_PONTOS_PESQUISA
        defaultLinhaShouldNotBeFound("faltaCadastrarPontosPesquisa.equals=" + UPDATED_FALTA_CADASTRAR_PONTOS_PESQUISA);
    }

    @Test
    @Transactional
    public void getAllLinhasByFaltaCadastrarPontosPesquisaIsInShouldWork() throws Exception {
        // Initialize the database
        linhaRepository.saveAndFlush(linha);

        // Get all the linhaList where faltaCadastrarPontosPesquisa in DEFAULT_FALTA_CADASTRAR_PONTOS_PESQUISA or UPDATED_FALTA_CADASTRAR_PONTOS_PESQUISA
        defaultLinhaShouldBeFound("faltaCadastrarPontosPesquisa.in=" + DEFAULT_FALTA_CADASTRAR_PONTOS_PESQUISA + "," + UPDATED_FALTA_CADASTRAR_PONTOS_PESQUISA);

        // Get all the linhaList where faltaCadastrarPontosPesquisa equals to UPDATED_FALTA_CADASTRAR_PONTOS_PESQUISA
        defaultLinhaShouldNotBeFound("faltaCadastrarPontosPesquisa.in=" + UPDATED_FALTA_CADASTRAR_PONTOS_PESQUISA);
    }

    @Test
    @Transactional
    public void getAllLinhasByFaltaCadastrarPontosPesquisaIsNullOrNotNull() throws Exception {
        // Initialize the database
        linhaRepository.saveAndFlush(linha);

        // Get all the linhaList where faltaCadastrarPontosPesquisa is not null
        defaultLinhaShouldBeFound("faltaCadastrarPontosPesquisa.specified=true");

        // Get all the linhaList where faltaCadastrarPontosPesquisa is null
        defaultLinhaShouldNotBeFound("faltaCadastrarPontosPesquisa.specified=false");
    }

    @Test
    @Transactional
    public void getAllLinhasBySemobIsEqualToSomething() throws Exception {
        // Initialize the database
        linhaRepository.saveAndFlush(linha);

        // Get all the linhaList where semob equals to DEFAULT_SEMOB
        defaultLinhaShouldBeFound("semob.equals=" + DEFAULT_SEMOB);

        // Get all the linhaList where semob equals to UPDATED_SEMOB
        defaultLinhaShouldNotBeFound("semob.equals=" + UPDATED_SEMOB);
    }

    @Test
    @Transactional
    public void getAllLinhasBySemobIsInShouldWork() throws Exception {
        // Initialize the database
        linhaRepository.saveAndFlush(linha);

        // Get all the linhaList where semob in DEFAULT_SEMOB or UPDATED_SEMOB
        defaultLinhaShouldBeFound("semob.in=" + DEFAULT_SEMOB + "," + UPDATED_SEMOB);

        // Get all the linhaList where semob equals to UPDATED_SEMOB
        defaultLinhaShouldNotBeFound("semob.in=" + UPDATED_SEMOB);
    }

    @Test
    @Transactional
    public void getAllLinhasBySemobIsNullOrNotNull() throws Exception {
        // Initialize the database
        linhaRepository.saveAndFlush(linha);

        // Get all the linhaList where semob is not null
        defaultLinhaShouldBeFound("semob.specified=true");

        // Get all the linhaList where semob is null
        defaultLinhaShouldNotBeFound("semob.specified=false");
    }

    @Test
    @Transactional
    public void getAllLinhasByTipoDeRotaIsEqualToSomething() throws Exception {
        // Initialize the database
        linhaRepository.saveAndFlush(linha);

        // Get all the linhaList where tipoDeRota equals to DEFAULT_TIPO_DE_ROTA
        defaultLinhaShouldBeFound("tipoDeRota.equals=" + DEFAULT_TIPO_DE_ROTA);

        // Get all the linhaList where tipoDeRota equals to UPDATED_TIPO_DE_ROTA
        defaultLinhaShouldNotBeFound("tipoDeRota.equals=" + UPDATED_TIPO_DE_ROTA);
    }

    @Test
    @Transactional
    public void getAllLinhasByTipoDeRotaIsInShouldWork() throws Exception {
        // Initialize the database
        linhaRepository.saveAndFlush(linha);

        // Get all the linhaList where tipoDeRota in DEFAULT_TIPO_DE_ROTA or UPDATED_TIPO_DE_ROTA
        defaultLinhaShouldBeFound("tipoDeRota.in=" + DEFAULT_TIPO_DE_ROTA + "," + UPDATED_TIPO_DE_ROTA);

        // Get all the linhaList where tipoDeRota equals to UPDATED_TIPO_DE_ROTA
        defaultLinhaShouldNotBeFound("tipoDeRota.in=" + UPDATED_TIPO_DE_ROTA);
    }

    @Test
    @Transactional
    public void getAllLinhasByTipoDeRotaIsNullOrNotNull() throws Exception {
        // Initialize the database
        linhaRepository.saveAndFlush(linha);

        // Get all the linhaList where tipoDeRota is not null
        defaultLinhaShouldBeFound("tipoDeRota.specified=true");

        // Get all the linhaList where tipoDeRota is null
        defaultLinhaShouldNotBeFound("tipoDeRota.specified=false");
    }

    @Test
    @Transactional
    public void getAllLinhasByItinerarioIsEqualToSomething() throws Exception {
        // Initialize the database
        PontoTracadoTrajeto itinerario = PontoTracadoTrajetoResourceIT.createEntity(em);
        em.persist(itinerario);
        em.flush();
        linha.addItinerario(itinerario);
        linhaRepository.saveAndFlush(linha);
        Long itinerarioId = itinerario.getId();

        // Get all the linhaList where itinerario equals to itinerarioId
        defaultLinhaShouldBeFound("itinerarioId.equals=" + itinerarioId);

        // Get all the linhaList where itinerario equals to itinerarioId + 1
        defaultLinhaShouldNotBeFound("itinerarioId.equals=" + (itinerarioId + 1));
    }


    @Test
    @Transactional
    public void getAllLinhasByPontosPesquisaIsEqualToSomething() throws Exception {
        // Initialize the database
        PontoPesquisa pontosPesquisa = PontoPesquisaResourceIT.createEntity(em);
        em.persist(pontosPesquisa);
        em.flush();
        linha.addPontosPesquisa(pontosPesquisa);
        linhaRepository.saveAndFlush(linha);
        Long pontosPesquisaId = pontosPesquisa.getId();

        // Get all the linhaList where pontosPesquisa equals to pontosPesquisaId
        defaultLinhaShouldBeFound("pontosPesquisaId.equals=" + pontosPesquisaId);

        // Get all the linhaList where pontosPesquisa equals to pontosPesquisaId + 1
        defaultLinhaShouldNotBeFound("pontosPesquisaId.equals=" + (pontosPesquisaId + 1));
    }


    @Test
    @Transactional
    public void getAllLinhasByCidadeIsEqualToSomething() throws Exception {
        // Initialize the database
        Cidade cidade = CidadeResourceIT.createEntity(em);
        em.persist(cidade);
        em.flush();
        linha.setCidade(cidade);
        linhaRepository.saveAndFlush(linha);
        Long cidadeId = cidade.getId();

        // Get all the linhaList where cidade equals to cidadeId
        defaultLinhaShouldBeFound("cidadeId.equals=" + cidadeId);

        // Get all the linhaList where cidade equals to cidadeId + 1
        defaultLinhaShouldNotBeFound("cidadeId.equals=" + (cidadeId + 1));
    }


    @Test
    @Transactional
    public void getAllLinhasByRootIsEqualToSomething() throws Exception {
        // Initialize the database
        Linha root = LinhaResourceIT.createEntity(em);
        em.persist(root);
        em.flush();
        linha.setRoot(root);
        linhaRepository.saveAndFlush(linha);
        Long rootId = root.getId();

        // Get all the linhaList where root equals to rootId
        defaultLinhaShouldBeFound("rootId.equals=" + rootId);

        // Get all the linhaList where root equals to rootId + 1
        defaultLinhaShouldNotBeFound("rootId.equals=" + (rootId + 1));
    }


    @Test
    @Transactional
    public void getAllLinhasByParadasIsEqualToSomething() throws Exception {
        // Initialize the database
        Parada paradas = ParadaResourceIT.createEntity(em);
        em.persist(paradas);
        em.flush();
        linha.addParadas(paradas);
        linhaRepository.saveAndFlush(linha);
        Long paradasId = paradas.getId();

        // Get all the linhaList where paradas equals to paradasId
        defaultLinhaShouldBeFound("paradasId.equals=" + paradasId);

        // Get all the linhaList where paradas equals to paradasId + 1
        defaultLinhaShouldNotBeFound("paradasId.equals=" + (paradasId + 1));
    }


    @Test
    @Transactional
    public void getAllLinhasByChildrenIsEqualToSomething() throws Exception {
        // Initialize the database
        Linha children = LinhaResourceIT.createEntity(em);
        em.persist(children);
        em.flush();
        linha.addChildren(children);
        linhaRepository.saveAndFlush(linha);
        Long childrenId = children.getId();

        // Get all the linhaList where children equals to childrenId
        defaultLinhaShouldBeFound("childrenId.equals=" + childrenId);

        // Get all the linhaList where children equals to childrenId + 1
        defaultLinhaShouldNotBeFound("childrenId.equals=" + (childrenId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultLinhaShouldBeFound(String filter) throws Exception {
        restLinhaMockMvc.perform(get("/api/linhas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(linha.getId().intValue())))
            .andExpect(jsonPath("$.[*].nome").value(hasItem(DEFAULT_NOME)))
            .andExpect(jsonPath("$.[*].codigo").value(hasItem(DEFAULT_CODIGO)))
            .andExpect(jsonPath("$.[*].url").value(hasItem(DEFAULT_URL)))
            .andExpect(jsonPath("$.[*].completa").value(hasItem(DEFAULT_COMPLETA.booleanValue())))
            .andExpect(jsonPath("$.[*].lastUpdate").value(hasItem(sameInstant(DEFAULT_LAST_UPDATE))))
            .andExpect(jsonPath("$.[*].comentario").value(hasItem(DEFAULT_COMENTARIO)))
            .andExpect(jsonPath("$.[*].itinerarioTotalEncoding").value(hasItem(DEFAULT_ITINERARIO_TOTAL_ENCODING)))
            .andExpect(jsonPath("$.[*].emAvaliacao").value(hasItem(DEFAULT_EM_AVALIACAO.booleanValue())))
            .andExpect(jsonPath("$.[*].faltaCadastrarPontosPesquisa").value(hasItem(DEFAULT_FALTA_CADASTRAR_PONTOS_PESQUISA.booleanValue())))
            .andExpect(jsonPath("$.[*].semob").value(hasItem(DEFAULT_SEMOB.booleanValue())))
            .andExpect(jsonPath("$.[*].tipoDeRota").value(hasItem(DEFAULT_TIPO_DE_ROTA.toString())));

        // Check, that the count call also returns 1
        restLinhaMockMvc.perform(get("/api/linhas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultLinhaShouldNotBeFound(String filter) throws Exception {
        restLinhaMockMvc.perform(get("/api/linhas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restLinhaMockMvc.perform(get("/api/linhas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingLinha() throws Exception {
        // Get the linha
        restLinhaMockMvc.perform(get("/api/linhas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateLinha() throws Exception {
        // Initialize the database
        linhaRepository.saveAndFlush(linha);

        int databaseSizeBeforeUpdate = linhaRepository.findAll().size();

        // Update the linha
        Linha updatedLinha = linhaRepository.findById(linha.getId()).get();
        // Disconnect from session so that the updates on updatedLinha are not directly saved in db
        em.detach(updatedLinha);
        updatedLinha
            .nome(UPDATED_NOME)
            .codigo(UPDATED_CODIGO)
            .url(UPDATED_URL)
            .completa(UPDATED_COMPLETA)
            .lastUpdate(UPDATED_LAST_UPDATE)
            .comentario(UPDATED_COMENTARIO)
            .itinerarioTotalEncoding(UPDATED_ITINERARIO_TOTAL_ENCODING)
            .emAvaliacao(UPDATED_EM_AVALIACAO)
            .faltaCadastrarPontosPesquisa(UPDATED_FALTA_CADASTRAR_PONTOS_PESQUISA)
            .semob(UPDATED_SEMOB)
            .tipoDeRota(UPDATED_TIPO_DE_ROTA);
        LinhaDTO linhaDTO = linhaMapper.toDto(updatedLinha);

        restLinhaMockMvc.perform(put("/api/linhas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(linhaDTO)))
            .andExpect(status().isOk());

        // Validate the Linha in the database
        List<Linha> linhaList = linhaRepository.findAll();
        assertThat(linhaList).hasSize(databaseSizeBeforeUpdate);
        Linha testLinha = linhaList.get(linhaList.size() - 1);
        assertThat(testLinha.getNome()).isEqualTo(UPDATED_NOME);
        assertThat(testLinha.getCodigo()).isEqualTo(UPDATED_CODIGO);
        assertThat(testLinha.getUrl()).isEqualTo(UPDATED_URL);
        assertThat(testLinha.isCompleta()).isEqualTo(UPDATED_COMPLETA);
        assertThat(testLinha.getLastUpdate()).isEqualTo(UPDATED_LAST_UPDATE);
        assertThat(testLinha.getComentario()).isEqualTo(UPDATED_COMENTARIO);
        assertThat(testLinha.getItinerarioTotalEncoding()).isEqualTo(UPDATED_ITINERARIO_TOTAL_ENCODING);
        assertThat(testLinha.isEmAvaliacao()).isEqualTo(UPDATED_EM_AVALIACAO);
        assertThat(testLinha.isFaltaCadastrarPontosPesquisa()).isEqualTo(UPDATED_FALTA_CADASTRAR_PONTOS_PESQUISA);
        assertThat(testLinha.isSemob()).isEqualTo(UPDATED_SEMOB);
        assertThat(testLinha.getTipoDeRota()).isEqualTo(UPDATED_TIPO_DE_ROTA);
    }

    @Test
    @Transactional
    public void updateNonExistingLinha() throws Exception {
        int databaseSizeBeforeUpdate = linhaRepository.findAll().size();

        // Create the Linha
        LinhaDTO linhaDTO = linhaMapper.toDto(linha);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restLinhaMockMvc.perform(put("/api/linhas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(linhaDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Linha in the database
        List<Linha> linhaList = linhaRepository.findAll();
        assertThat(linhaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteLinha() throws Exception {
        // Initialize the database
        linhaRepository.saveAndFlush(linha);

        int databaseSizeBeforeDelete = linhaRepository.findAll().size();

        // Delete the linha
        restLinhaMockMvc.perform(delete("/api/linhas/{id}", linha.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<Linha> linhaList = linhaRepository.findAll();
        assertThat(linhaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Linha.class);
        Linha linha1 = new Linha();
        linha1.setId(1L);
        Linha linha2 = new Linha();
        linha2.setId(linha1.getId());
        assertThat(linha1).isEqualTo(linha2);
        linha2.setId(2L);
        assertThat(linha1).isNotEqualTo(linha2);
        linha1.setId(null);
        assertThat(linha1).isNotEqualTo(linha2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(LinhaDTO.class);
        LinhaDTO linhaDTO1 = new LinhaDTO();
        linhaDTO1.setId(1L);
        LinhaDTO linhaDTO2 = new LinhaDTO();
        assertThat(linhaDTO1).isNotEqualTo(linhaDTO2);
        linhaDTO2.setId(linhaDTO1.getId());
        assertThat(linhaDTO1).isEqualTo(linhaDTO2);
        linhaDTO2.setId(2L);
        assertThat(linhaDTO1).isNotEqualTo(linhaDTO2);
        linhaDTO1.setId(null);
        assertThat(linhaDTO1).isNotEqualTo(linhaDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(linhaMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(linhaMapper.fromId(null)).isNull();
    }
}
