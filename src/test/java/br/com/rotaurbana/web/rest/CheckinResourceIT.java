package br.com.rotaurbana.web.rest;

import br.com.rotaurbana.RotaurbanacucumberApp;
import br.com.rotaurbana.domain.Checkin;
import br.com.rotaurbana.domain.Linha;
import br.com.rotaurbana.repository.CheckinRepository;
import br.com.rotaurbana.service.CheckinService;
import br.com.rotaurbana.service.dto.CheckinDTO;
import br.com.rotaurbana.service.mapper.CheckinMapper;
import br.com.rotaurbana.web.rest.errors.ExceptionTranslator;
import br.com.rotaurbana.service.dto.CheckinCriteria;
import br.com.rotaurbana.service.CheckinQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static br.com.rotaurbana.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link CheckinResource} REST controller.
 */
@SpringBootTest(classes = RotaurbanacucumberApp.class)
public class CheckinResourceIT {

    private static final Double DEFAULT_LATITUDE = 1D;
    private static final Double UPDATED_LATITUDE = 2D;

    private static final Double DEFAULT_LONGITUDE = 1D;
    private static final Double UPDATED_LONGITUDE = 2D;

    @Autowired
    private CheckinRepository checkinRepository;

    @Autowired
    private CheckinMapper checkinMapper;

    @Autowired
    private CheckinService checkinService;

    @Autowired
    private CheckinQueryService checkinQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restCheckinMockMvc;

    private Checkin checkin;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final CheckinResource checkinResource = new CheckinResource(checkinService, checkinQueryService);
        this.restCheckinMockMvc = MockMvcBuilders.standaloneSetup(checkinResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Checkin createEntity(EntityManager em) {
        Checkin checkin = new Checkin()
            .latitude(DEFAULT_LATITUDE)
            .longitude(DEFAULT_LONGITUDE);
        return checkin;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Checkin createUpdatedEntity(EntityManager em) {
        Checkin checkin = new Checkin()
            .latitude(UPDATED_LATITUDE)
            .longitude(UPDATED_LONGITUDE);
        return checkin;
    }

    @BeforeEach
    public void initTest() {
        checkin = createEntity(em);
    }

    @Test
    @Transactional
    public void createCheckin() throws Exception {
        int databaseSizeBeforeCreate = checkinRepository.findAll().size();

        // Create the Checkin
        CheckinDTO checkinDTO = checkinMapper.toDto(checkin);
        restCheckinMockMvc.perform(post("/api/checkins")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(checkinDTO)))
            .andExpect(status().isCreated());

        // Validate the Checkin in the database
        List<Checkin> checkinList = checkinRepository.findAll();
        assertThat(checkinList).hasSize(databaseSizeBeforeCreate + 1);
        Checkin testCheckin = checkinList.get(checkinList.size() - 1);
        assertThat(testCheckin.getLatitude()).isEqualTo(DEFAULT_LATITUDE);
        assertThat(testCheckin.getLongitude()).isEqualTo(DEFAULT_LONGITUDE);
    }

    @Test
    @Transactional
    public void createCheckinWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = checkinRepository.findAll().size();

        // Create the Checkin with an existing ID
        checkin.setId(1L);
        CheckinDTO checkinDTO = checkinMapper.toDto(checkin);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCheckinMockMvc.perform(post("/api/checkins")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(checkinDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Checkin in the database
        List<Checkin> checkinList = checkinRepository.findAll();
        assertThat(checkinList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllCheckins() throws Exception {
        // Initialize the database
        checkinRepository.saveAndFlush(checkin);

        // Get all the checkinList
        restCheckinMockMvc.perform(get("/api/checkins?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(checkin.getId().intValue())))
            .andExpect(jsonPath("$.[*].latitude").value(hasItem(DEFAULT_LATITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].longitude").value(hasItem(DEFAULT_LONGITUDE.doubleValue())));
    }
    
    @Test
    @Transactional
    public void getCheckin() throws Exception {
        // Initialize the database
        checkinRepository.saveAndFlush(checkin);

        // Get the checkin
        restCheckinMockMvc.perform(get("/api/checkins/{id}", checkin.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(checkin.getId().intValue()))
            .andExpect(jsonPath("$.latitude").value(DEFAULT_LATITUDE.doubleValue()))
            .andExpect(jsonPath("$.longitude").value(DEFAULT_LONGITUDE.doubleValue()));
    }

    @Test
    @Transactional
    public void getAllCheckinsByLatitudeIsEqualToSomething() throws Exception {
        // Initialize the database
        checkinRepository.saveAndFlush(checkin);

        // Get all the checkinList where latitude equals to DEFAULT_LATITUDE
        defaultCheckinShouldBeFound("latitude.equals=" + DEFAULT_LATITUDE);

        // Get all the checkinList where latitude equals to UPDATED_LATITUDE
        defaultCheckinShouldNotBeFound("latitude.equals=" + UPDATED_LATITUDE);
    }

    @Test
    @Transactional
    public void getAllCheckinsByLatitudeIsInShouldWork() throws Exception {
        // Initialize the database
        checkinRepository.saveAndFlush(checkin);

        // Get all the checkinList where latitude in DEFAULT_LATITUDE or UPDATED_LATITUDE
        defaultCheckinShouldBeFound("latitude.in=" + DEFAULT_LATITUDE + "," + UPDATED_LATITUDE);

        // Get all the checkinList where latitude equals to UPDATED_LATITUDE
        defaultCheckinShouldNotBeFound("latitude.in=" + UPDATED_LATITUDE);
    }

    @Test
    @Transactional
    public void getAllCheckinsByLatitudeIsNullOrNotNull() throws Exception {
        // Initialize the database
        checkinRepository.saveAndFlush(checkin);

        // Get all the checkinList where latitude is not null
        defaultCheckinShouldBeFound("latitude.specified=true");

        // Get all the checkinList where latitude is null
        defaultCheckinShouldNotBeFound("latitude.specified=false");
    }

    @Test
    @Transactional
    public void getAllCheckinsByLongitudeIsEqualToSomething() throws Exception {
        // Initialize the database
        checkinRepository.saveAndFlush(checkin);

        // Get all the checkinList where longitude equals to DEFAULT_LONGITUDE
        defaultCheckinShouldBeFound("longitude.equals=" + DEFAULT_LONGITUDE);

        // Get all the checkinList where longitude equals to UPDATED_LONGITUDE
        defaultCheckinShouldNotBeFound("longitude.equals=" + UPDATED_LONGITUDE);
    }

    @Test
    @Transactional
    public void getAllCheckinsByLongitudeIsInShouldWork() throws Exception {
        // Initialize the database
        checkinRepository.saveAndFlush(checkin);

        // Get all the checkinList where longitude in DEFAULT_LONGITUDE or UPDATED_LONGITUDE
        defaultCheckinShouldBeFound("longitude.in=" + DEFAULT_LONGITUDE + "," + UPDATED_LONGITUDE);

        // Get all the checkinList where longitude equals to UPDATED_LONGITUDE
        defaultCheckinShouldNotBeFound("longitude.in=" + UPDATED_LONGITUDE);
    }

    @Test
    @Transactional
    public void getAllCheckinsByLongitudeIsNullOrNotNull() throws Exception {
        // Initialize the database
        checkinRepository.saveAndFlush(checkin);

        // Get all the checkinList where longitude is not null
        defaultCheckinShouldBeFound("longitude.specified=true");

        // Get all the checkinList where longitude is null
        defaultCheckinShouldNotBeFound("longitude.specified=false");
    }

    @Test
    @Transactional
    public void getAllCheckinsByLinhaIsEqualToSomething() throws Exception {
        // Initialize the database
        Linha linha = LinhaResourceIT.createEntity(em);
        em.persist(linha);
        em.flush();
        checkin.setLinha(linha);
        checkinRepository.saveAndFlush(checkin);
        Long linhaId = linha.getId();

        // Get all the checkinList where linha equals to linhaId
        defaultCheckinShouldBeFound("linhaId.equals=" + linhaId);

        // Get all the checkinList where linha equals to linhaId + 1
        defaultCheckinShouldNotBeFound("linhaId.equals=" + (linhaId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultCheckinShouldBeFound(String filter) throws Exception {
        restCheckinMockMvc.perform(get("/api/checkins?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(checkin.getId().intValue())))
            .andExpect(jsonPath("$.[*].latitude").value(hasItem(DEFAULT_LATITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].longitude").value(hasItem(DEFAULT_LONGITUDE.doubleValue())));

        // Check, that the count call also returns 1
        restCheckinMockMvc.perform(get("/api/checkins/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultCheckinShouldNotBeFound(String filter) throws Exception {
        restCheckinMockMvc.perform(get("/api/checkins?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restCheckinMockMvc.perform(get("/api/checkins/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingCheckin() throws Exception {
        // Get the checkin
        restCheckinMockMvc.perform(get("/api/checkins/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCheckin() throws Exception {
        // Initialize the database
        checkinRepository.saveAndFlush(checkin);

        int databaseSizeBeforeUpdate = checkinRepository.findAll().size();

        // Update the checkin
        Checkin updatedCheckin = checkinRepository.findById(checkin.getId()).get();
        // Disconnect from session so that the updates on updatedCheckin are not directly saved in db
        em.detach(updatedCheckin);
        updatedCheckin
            .latitude(UPDATED_LATITUDE)
            .longitude(UPDATED_LONGITUDE);
        CheckinDTO checkinDTO = checkinMapper.toDto(updatedCheckin);

        restCheckinMockMvc.perform(put("/api/checkins")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(checkinDTO)))
            .andExpect(status().isOk());

        // Validate the Checkin in the database
        List<Checkin> checkinList = checkinRepository.findAll();
        assertThat(checkinList).hasSize(databaseSizeBeforeUpdate);
        Checkin testCheckin = checkinList.get(checkinList.size() - 1);
        assertThat(testCheckin.getLatitude()).isEqualTo(UPDATED_LATITUDE);
        assertThat(testCheckin.getLongitude()).isEqualTo(UPDATED_LONGITUDE);
    }

    @Test
    @Transactional
    public void updateNonExistingCheckin() throws Exception {
        int databaseSizeBeforeUpdate = checkinRepository.findAll().size();

        // Create the Checkin
        CheckinDTO checkinDTO = checkinMapper.toDto(checkin);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCheckinMockMvc.perform(put("/api/checkins")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(checkinDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Checkin in the database
        List<Checkin> checkinList = checkinRepository.findAll();
        assertThat(checkinList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCheckin() throws Exception {
        // Initialize the database
        checkinRepository.saveAndFlush(checkin);

        int databaseSizeBeforeDelete = checkinRepository.findAll().size();

        // Delete the checkin
        restCheckinMockMvc.perform(delete("/api/checkins/{id}", checkin.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<Checkin> checkinList = checkinRepository.findAll();
        assertThat(checkinList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Checkin.class);
        Checkin checkin1 = new Checkin();
        checkin1.setId(1L);
        Checkin checkin2 = new Checkin();
        checkin2.setId(checkin1.getId());
        assertThat(checkin1).isEqualTo(checkin2);
        checkin2.setId(2L);
        assertThat(checkin1).isNotEqualTo(checkin2);
        checkin1.setId(null);
        assertThat(checkin1).isNotEqualTo(checkin2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CheckinDTO.class);
        CheckinDTO checkinDTO1 = new CheckinDTO();
        checkinDTO1.setId(1L);
        CheckinDTO checkinDTO2 = new CheckinDTO();
        assertThat(checkinDTO1).isNotEqualTo(checkinDTO2);
        checkinDTO2.setId(checkinDTO1.getId());
        assertThat(checkinDTO1).isEqualTo(checkinDTO2);
        checkinDTO2.setId(2L);
        assertThat(checkinDTO1).isNotEqualTo(checkinDTO2);
        checkinDTO1.setId(null);
        assertThat(checkinDTO1).isNotEqualTo(checkinDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(checkinMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(checkinMapper.fromId(null)).isNull();
    }
}
