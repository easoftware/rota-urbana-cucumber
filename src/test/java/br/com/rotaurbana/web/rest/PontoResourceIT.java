package br.com.rotaurbana.web.rest;

import br.com.rotaurbana.RotaurbanacucumberApp;
import br.com.rotaurbana.domain.Ponto;
import br.com.rotaurbana.repository.PontoRepository;
import br.com.rotaurbana.service.PontoService;
import br.com.rotaurbana.service.dto.PontoDTO;
import br.com.rotaurbana.service.mapper.PontoMapper;
import br.com.rotaurbana.web.rest.errors.ExceptionTranslator;
import br.com.rotaurbana.service.dto.PontoCriteria;
import br.com.rotaurbana.service.PontoQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static br.com.rotaurbana.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link PontoResource} REST controller.
 */
@SpringBootTest(classes = RotaurbanacucumberApp.class)
public class PontoResourceIT {

    private static final Double DEFAULT_LATITUDE = 1D;
    private static final Double UPDATED_LATITUDE = 2D;

    private static final Double DEFAULT_LONGITUDE = 1D;
    private static final Double UPDATED_LONGITUDE = 2D;

    private static final String DEFAULT_CODIGO_ANDROID = "AAAAAAAAAA";
    private static final String UPDATED_CODIGO_ANDROID = "BBBBBBBBBB";

    @Autowired
    private PontoRepository pontoRepository;

    @Autowired
    private PontoMapper pontoMapper;

    @Autowired
    private PontoService pontoService;

    @Autowired
    private PontoQueryService pontoQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restPontoMockMvc;

    private Ponto ponto;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PontoResource pontoResource = new PontoResource(pontoService, pontoQueryService);
        this.restPontoMockMvc = MockMvcBuilders.standaloneSetup(pontoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Ponto createEntity(EntityManager em) {
        Ponto ponto = new Ponto()
            .latitude(DEFAULT_LATITUDE)
            .longitude(DEFAULT_LONGITUDE)
            .codigoAndroid(DEFAULT_CODIGO_ANDROID);
        return ponto;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Ponto createUpdatedEntity(EntityManager em) {
        Ponto ponto = new Ponto()
            .latitude(UPDATED_LATITUDE)
            .longitude(UPDATED_LONGITUDE)
            .codigoAndroid(UPDATED_CODIGO_ANDROID);
        return ponto;
    }

    @BeforeEach
    public void initTest() {
        ponto = createEntity(em);
    }

    @Test
    @Transactional
    public void createPonto() throws Exception {
        int databaseSizeBeforeCreate = pontoRepository.findAll().size();

        // Create the Ponto
        PontoDTO pontoDTO = pontoMapper.toDto(ponto);
        restPontoMockMvc.perform(post("/api/pontos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pontoDTO)))
            .andExpect(status().isCreated());

        // Validate the Ponto in the database
        List<Ponto> pontoList = pontoRepository.findAll();
        assertThat(pontoList).hasSize(databaseSizeBeforeCreate + 1);
        Ponto testPonto = pontoList.get(pontoList.size() - 1);
        assertThat(testPonto.getLatitude()).isEqualTo(DEFAULT_LATITUDE);
        assertThat(testPonto.getLongitude()).isEqualTo(DEFAULT_LONGITUDE);
        assertThat(testPonto.getCodigoAndroid()).isEqualTo(DEFAULT_CODIGO_ANDROID);
    }

    @Test
    @Transactional
    public void createPontoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = pontoRepository.findAll().size();

        // Create the Ponto with an existing ID
        ponto.setId(1L);
        PontoDTO pontoDTO = pontoMapper.toDto(ponto);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPontoMockMvc.perform(post("/api/pontos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pontoDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Ponto in the database
        List<Ponto> pontoList = pontoRepository.findAll();
        assertThat(pontoList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllPontos() throws Exception {
        // Initialize the database
        pontoRepository.saveAndFlush(ponto);

        // Get all the pontoList
        restPontoMockMvc.perform(get("/api/pontos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(ponto.getId().intValue())))
            .andExpect(jsonPath("$.[*].latitude").value(hasItem(DEFAULT_LATITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].longitude").value(hasItem(DEFAULT_LONGITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].codigoAndroid").value(hasItem(DEFAULT_CODIGO_ANDROID.toString())));
    }
    
    @Test
    @Transactional
    public void getPonto() throws Exception {
        // Initialize the database
        pontoRepository.saveAndFlush(ponto);

        // Get the ponto
        restPontoMockMvc.perform(get("/api/pontos/{id}", ponto.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(ponto.getId().intValue()))
            .andExpect(jsonPath("$.latitude").value(DEFAULT_LATITUDE.doubleValue()))
            .andExpect(jsonPath("$.longitude").value(DEFAULT_LONGITUDE.doubleValue()))
            .andExpect(jsonPath("$.codigoAndroid").value(DEFAULT_CODIGO_ANDROID.toString()));
    }

    @Test
    @Transactional
    public void getAllPontosByLatitudeIsEqualToSomething() throws Exception {
        // Initialize the database
        pontoRepository.saveAndFlush(ponto);

        // Get all the pontoList where latitude equals to DEFAULT_LATITUDE
        defaultPontoShouldBeFound("latitude.equals=" + DEFAULT_LATITUDE);

        // Get all the pontoList where latitude equals to UPDATED_LATITUDE
        defaultPontoShouldNotBeFound("latitude.equals=" + UPDATED_LATITUDE);
    }

    @Test
    @Transactional
    public void getAllPontosByLatitudeIsInShouldWork() throws Exception {
        // Initialize the database
        pontoRepository.saveAndFlush(ponto);

        // Get all the pontoList where latitude in DEFAULT_LATITUDE or UPDATED_LATITUDE
        defaultPontoShouldBeFound("latitude.in=" + DEFAULT_LATITUDE + "," + UPDATED_LATITUDE);

        // Get all the pontoList where latitude equals to UPDATED_LATITUDE
        defaultPontoShouldNotBeFound("latitude.in=" + UPDATED_LATITUDE);
    }

    @Test
    @Transactional
    public void getAllPontosByLatitudeIsNullOrNotNull() throws Exception {
        // Initialize the database
        pontoRepository.saveAndFlush(ponto);

        // Get all the pontoList where latitude is not null
        defaultPontoShouldBeFound("latitude.specified=true");

        // Get all the pontoList where latitude is null
        defaultPontoShouldNotBeFound("latitude.specified=false");
    }

    @Test
    @Transactional
    public void getAllPontosByLongitudeIsEqualToSomething() throws Exception {
        // Initialize the database
        pontoRepository.saveAndFlush(ponto);

        // Get all the pontoList where longitude equals to DEFAULT_LONGITUDE
        defaultPontoShouldBeFound("longitude.equals=" + DEFAULT_LONGITUDE);

        // Get all the pontoList where longitude equals to UPDATED_LONGITUDE
        defaultPontoShouldNotBeFound("longitude.equals=" + UPDATED_LONGITUDE);
    }

    @Test
    @Transactional
    public void getAllPontosByLongitudeIsInShouldWork() throws Exception {
        // Initialize the database
        pontoRepository.saveAndFlush(ponto);

        // Get all the pontoList where longitude in DEFAULT_LONGITUDE or UPDATED_LONGITUDE
        defaultPontoShouldBeFound("longitude.in=" + DEFAULT_LONGITUDE + "," + UPDATED_LONGITUDE);

        // Get all the pontoList where longitude equals to UPDATED_LONGITUDE
        defaultPontoShouldNotBeFound("longitude.in=" + UPDATED_LONGITUDE);
    }

    @Test
    @Transactional
    public void getAllPontosByLongitudeIsNullOrNotNull() throws Exception {
        // Initialize the database
        pontoRepository.saveAndFlush(ponto);

        // Get all the pontoList where longitude is not null
        defaultPontoShouldBeFound("longitude.specified=true");

        // Get all the pontoList where longitude is null
        defaultPontoShouldNotBeFound("longitude.specified=false");
    }

    @Test
    @Transactional
    public void getAllPontosByCodigoAndroidIsEqualToSomething() throws Exception {
        // Initialize the database
        pontoRepository.saveAndFlush(ponto);

        // Get all the pontoList where codigoAndroid equals to DEFAULT_CODIGO_ANDROID
        defaultPontoShouldBeFound("codigoAndroid.equals=" + DEFAULT_CODIGO_ANDROID);

        // Get all the pontoList where codigoAndroid equals to UPDATED_CODIGO_ANDROID
        defaultPontoShouldNotBeFound("codigoAndroid.equals=" + UPDATED_CODIGO_ANDROID);
    }

    @Test
    @Transactional
    public void getAllPontosByCodigoAndroidIsInShouldWork() throws Exception {
        // Initialize the database
        pontoRepository.saveAndFlush(ponto);

        // Get all the pontoList where codigoAndroid in DEFAULT_CODIGO_ANDROID or UPDATED_CODIGO_ANDROID
        defaultPontoShouldBeFound("codigoAndroid.in=" + DEFAULT_CODIGO_ANDROID + "," + UPDATED_CODIGO_ANDROID);

        // Get all the pontoList where codigoAndroid equals to UPDATED_CODIGO_ANDROID
        defaultPontoShouldNotBeFound("codigoAndroid.in=" + UPDATED_CODIGO_ANDROID);
    }

    @Test
    @Transactional
    public void getAllPontosByCodigoAndroidIsNullOrNotNull() throws Exception {
        // Initialize the database
        pontoRepository.saveAndFlush(ponto);

        // Get all the pontoList where codigoAndroid is not null
        defaultPontoShouldBeFound("codigoAndroid.specified=true");

        // Get all the pontoList where codigoAndroid is null
        defaultPontoShouldNotBeFound("codigoAndroid.specified=false");
    }
    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultPontoShouldBeFound(String filter) throws Exception {
        restPontoMockMvc.perform(get("/api/pontos?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(ponto.getId().intValue())))
            .andExpect(jsonPath("$.[*].latitude").value(hasItem(DEFAULT_LATITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].longitude").value(hasItem(DEFAULT_LONGITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].codigoAndroid").value(hasItem(DEFAULT_CODIGO_ANDROID)));

        // Check, that the count call also returns 1
        restPontoMockMvc.perform(get("/api/pontos/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultPontoShouldNotBeFound(String filter) throws Exception {
        restPontoMockMvc.perform(get("/api/pontos?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restPontoMockMvc.perform(get("/api/pontos/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingPonto() throws Exception {
        // Get the ponto
        restPontoMockMvc.perform(get("/api/pontos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePonto() throws Exception {
        // Initialize the database
        pontoRepository.saveAndFlush(ponto);

        int databaseSizeBeforeUpdate = pontoRepository.findAll().size();

        // Update the ponto
        Ponto updatedPonto = pontoRepository.findById(ponto.getId()).get();
        // Disconnect from session so that the updates on updatedPonto are not directly saved in db
        em.detach(updatedPonto);
        updatedPonto
            .latitude(UPDATED_LATITUDE)
            .longitude(UPDATED_LONGITUDE)
            .codigoAndroid(UPDATED_CODIGO_ANDROID);
        PontoDTO pontoDTO = pontoMapper.toDto(updatedPonto);

        restPontoMockMvc.perform(put("/api/pontos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pontoDTO)))
            .andExpect(status().isOk());

        // Validate the Ponto in the database
        List<Ponto> pontoList = pontoRepository.findAll();
        assertThat(pontoList).hasSize(databaseSizeBeforeUpdate);
        Ponto testPonto = pontoList.get(pontoList.size() - 1);
        assertThat(testPonto.getLatitude()).isEqualTo(UPDATED_LATITUDE);
        assertThat(testPonto.getLongitude()).isEqualTo(UPDATED_LONGITUDE);
        assertThat(testPonto.getCodigoAndroid()).isEqualTo(UPDATED_CODIGO_ANDROID);
    }

    @Test
    @Transactional
    public void updateNonExistingPonto() throws Exception {
        int databaseSizeBeforeUpdate = pontoRepository.findAll().size();

        // Create the Ponto
        PontoDTO pontoDTO = pontoMapper.toDto(ponto);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPontoMockMvc.perform(put("/api/pontos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pontoDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Ponto in the database
        List<Ponto> pontoList = pontoRepository.findAll();
        assertThat(pontoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePonto() throws Exception {
        // Initialize the database
        pontoRepository.saveAndFlush(ponto);

        int databaseSizeBeforeDelete = pontoRepository.findAll().size();

        // Delete the ponto
        restPontoMockMvc.perform(delete("/api/pontos/{id}", ponto.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<Ponto> pontoList = pontoRepository.findAll();
        assertThat(pontoList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Ponto.class);
        Ponto ponto1 = new Ponto();
        ponto1.setId(1L);
        Ponto ponto2 = new Ponto();
        ponto2.setId(ponto1.getId());
        assertThat(ponto1).isEqualTo(ponto2);
        ponto2.setId(2L);
        assertThat(ponto1).isNotEqualTo(ponto2);
        ponto1.setId(null);
        assertThat(ponto1).isNotEqualTo(ponto2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PontoDTO.class);
        PontoDTO pontoDTO1 = new PontoDTO();
        pontoDTO1.setId(1L);
        PontoDTO pontoDTO2 = new PontoDTO();
        assertThat(pontoDTO1).isNotEqualTo(pontoDTO2);
        pontoDTO2.setId(pontoDTO1.getId());
        assertThat(pontoDTO1).isEqualTo(pontoDTO2);
        pontoDTO2.setId(2L);
        assertThat(pontoDTO1).isNotEqualTo(pontoDTO2);
        pontoDTO1.setId(null);
        assertThat(pontoDTO1).isNotEqualTo(pontoDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(pontoMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(pontoMapper.fromId(null)).isNull();
    }
}
