package br.com.rotaurbana.web.rest;

import br.com.rotaurbana.RotaurbanacucumberApp;
import br.com.rotaurbana.domain.PontoTracadoTrajeto;
import br.com.rotaurbana.domain.Linha;
import br.com.rotaurbana.repository.PontoTracadoTrajetoRepository;
import br.com.rotaurbana.service.PontoTracadoTrajetoService;
import br.com.rotaurbana.service.dto.PontoTracadoTrajetoDTO;
import br.com.rotaurbana.service.mapper.PontoTracadoTrajetoMapper;
import br.com.rotaurbana.web.rest.errors.ExceptionTranslator;
import br.com.rotaurbana.service.dto.PontoTracadoTrajetoCriteria;
import br.com.rotaurbana.service.PontoTracadoTrajetoQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static br.com.rotaurbana.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import br.com.rotaurbana.domain.enumeration.Tipo;
/**
 * Integration tests for the {@Link PontoTracadoTrajetoResource} REST controller.
 */
@SpringBootTest(classes = RotaurbanacucumberApp.class)
public class PontoTracadoTrajetoResourceIT {

    private static final Double DEFAULT_LATITUDE = 1D;
    private static final Double UPDATED_LATITUDE = 2D;

    private static final Double DEFAULT_LONGITUDE = 1D;
    private static final Double UPDATED_LONGITUDE = 2D;

    private static final Integer DEFAULT_POSICAO = 1;
    private static final Integer UPDATED_POSICAO = 2;

    private static final Tipo DEFAULT_TIPO = Tipo.IDA;
    private static final Tipo UPDATED_TIPO = Tipo.VOLTA;

    @Autowired
    private PontoTracadoTrajetoRepository pontoTracadoTrajetoRepository;

    @Autowired
    private PontoTracadoTrajetoMapper pontoTracadoTrajetoMapper;

    @Autowired
    private PontoTracadoTrajetoService pontoTracadoTrajetoService;

    @Autowired
    private PontoTracadoTrajetoQueryService pontoTracadoTrajetoQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restPontoTracadoTrajetoMockMvc;

    private PontoTracadoTrajeto pontoTracadoTrajeto;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PontoTracadoTrajetoResource pontoTracadoTrajetoResource = new PontoTracadoTrajetoResource(pontoTracadoTrajetoService, pontoTracadoTrajetoQueryService);
        this.restPontoTracadoTrajetoMockMvc = MockMvcBuilders.standaloneSetup(pontoTracadoTrajetoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PontoTracadoTrajeto createEntity(EntityManager em) {
        PontoTracadoTrajeto pontoTracadoTrajeto = new PontoTracadoTrajeto()
            .latitude(DEFAULT_LATITUDE)
            .longitude(DEFAULT_LONGITUDE)
            .posicao(DEFAULT_POSICAO)
            .tipo(DEFAULT_TIPO);
        return pontoTracadoTrajeto;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PontoTracadoTrajeto createUpdatedEntity(EntityManager em) {
        PontoTracadoTrajeto pontoTracadoTrajeto = new PontoTracadoTrajeto()
            .latitude(UPDATED_LATITUDE)
            .longitude(UPDATED_LONGITUDE)
            .posicao(UPDATED_POSICAO)
            .tipo(UPDATED_TIPO);
        return pontoTracadoTrajeto;
    }

    @BeforeEach
    public void initTest() {
        pontoTracadoTrajeto = createEntity(em);
    }

    @Test
    @Transactional
    public void createPontoTracadoTrajeto() throws Exception {
        int databaseSizeBeforeCreate = pontoTracadoTrajetoRepository.findAll().size();

        // Create the PontoTracadoTrajeto
        PontoTracadoTrajetoDTO pontoTracadoTrajetoDTO = pontoTracadoTrajetoMapper.toDto(pontoTracadoTrajeto);
        restPontoTracadoTrajetoMockMvc.perform(post("/api/ponto-tracado-trajetos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pontoTracadoTrajetoDTO)))
            .andExpect(status().isCreated());

        // Validate the PontoTracadoTrajeto in the database
        List<PontoTracadoTrajeto> pontoTracadoTrajetoList = pontoTracadoTrajetoRepository.findAll();
        assertThat(pontoTracadoTrajetoList).hasSize(databaseSizeBeforeCreate + 1);
        PontoTracadoTrajeto testPontoTracadoTrajeto = pontoTracadoTrajetoList.get(pontoTracadoTrajetoList.size() - 1);
        assertThat(testPontoTracadoTrajeto.getLatitude()).isEqualTo(DEFAULT_LATITUDE);
        assertThat(testPontoTracadoTrajeto.getLongitude()).isEqualTo(DEFAULT_LONGITUDE);
        assertThat(testPontoTracadoTrajeto.getPosicao()).isEqualTo(DEFAULT_POSICAO);
        assertThat(testPontoTracadoTrajeto.getTipo()).isEqualTo(DEFAULT_TIPO);
    }

    @Test
    @Transactional
    public void createPontoTracadoTrajetoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = pontoTracadoTrajetoRepository.findAll().size();

        // Create the PontoTracadoTrajeto with an existing ID
        pontoTracadoTrajeto.setId(1L);
        PontoTracadoTrajetoDTO pontoTracadoTrajetoDTO = pontoTracadoTrajetoMapper.toDto(pontoTracadoTrajeto);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPontoTracadoTrajetoMockMvc.perform(post("/api/ponto-tracado-trajetos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pontoTracadoTrajetoDTO)))
            .andExpect(status().isBadRequest());

        // Validate the PontoTracadoTrajeto in the database
        List<PontoTracadoTrajeto> pontoTracadoTrajetoList = pontoTracadoTrajetoRepository.findAll();
        assertThat(pontoTracadoTrajetoList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllPontoTracadoTrajetos() throws Exception {
        // Initialize the database
        pontoTracadoTrajetoRepository.saveAndFlush(pontoTracadoTrajeto);

        // Get all the pontoTracadoTrajetoList
        restPontoTracadoTrajetoMockMvc.perform(get("/api/ponto-tracado-trajetos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(pontoTracadoTrajeto.getId().intValue())))
            .andExpect(jsonPath("$.[*].latitude").value(hasItem(DEFAULT_LATITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].longitude").value(hasItem(DEFAULT_LONGITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].posicao").value(hasItem(DEFAULT_POSICAO)))
            .andExpect(jsonPath("$.[*].tipo").value(hasItem(DEFAULT_TIPO.toString())));
    }
    
    @Test
    @Transactional
    public void getPontoTracadoTrajeto() throws Exception {
        // Initialize the database
        pontoTracadoTrajetoRepository.saveAndFlush(pontoTracadoTrajeto);

        // Get the pontoTracadoTrajeto
        restPontoTracadoTrajetoMockMvc.perform(get("/api/ponto-tracado-trajetos/{id}", pontoTracadoTrajeto.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(pontoTracadoTrajeto.getId().intValue()))
            .andExpect(jsonPath("$.latitude").value(DEFAULT_LATITUDE.doubleValue()))
            .andExpect(jsonPath("$.longitude").value(DEFAULT_LONGITUDE.doubleValue()))
            .andExpect(jsonPath("$.posicao").value(DEFAULT_POSICAO))
            .andExpect(jsonPath("$.tipo").value(DEFAULT_TIPO.toString()));
    }

    @Test
    @Transactional
    public void getAllPontoTracadoTrajetosByLatitudeIsEqualToSomething() throws Exception {
        // Initialize the database
        pontoTracadoTrajetoRepository.saveAndFlush(pontoTracadoTrajeto);

        // Get all the pontoTracadoTrajetoList where latitude equals to DEFAULT_LATITUDE
        defaultPontoTracadoTrajetoShouldBeFound("latitude.equals=" + DEFAULT_LATITUDE);

        // Get all the pontoTracadoTrajetoList where latitude equals to UPDATED_LATITUDE
        defaultPontoTracadoTrajetoShouldNotBeFound("latitude.equals=" + UPDATED_LATITUDE);
    }

    @Test
    @Transactional
    public void getAllPontoTracadoTrajetosByLatitudeIsInShouldWork() throws Exception {
        // Initialize the database
        pontoTracadoTrajetoRepository.saveAndFlush(pontoTracadoTrajeto);

        // Get all the pontoTracadoTrajetoList where latitude in DEFAULT_LATITUDE or UPDATED_LATITUDE
        defaultPontoTracadoTrajetoShouldBeFound("latitude.in=" + DEFAULT_LATITUDE + "," + UPDATED_LATITUDE);

        // Get all the pontoTracadoTrajetoList where latitude equals to UPDATED_LATITUDE
        defaultPontoTracadoTrajetoShouldNotBeFound("latitude.in=" + UPDATED_LATITUDE);
    }

    @Test
    @Transactional
    public void getAllPontoTracadoTrajetosByLatitudeIsNullOrNotNull() throws Exception {
        // Initialize the database
        pontoTracadoTrajetoRepository.saveAndFlush(pontoTracadoTrajeto);

        // Get all the pontoTracadoTrajetoList where latitude is not null
        defaultPontoTracadoTrajetoShouldBeFound("latitude.specified=true");

        // Get all the pontoTracadoTrajetoList where latitude is null
        defaultPontoTracadoTrajetoShouldNotBeFound("latitude.specified=false");
    }

    @Test
    @Transactional
    public void getAllPontoTracadoTrajetosByLongitudeIsEqualToSomething() throws Exception {
        // Initialize the database
        pontoTracadoTrajetoRepository.saveAndFlush(pontoTracadoTrajeto);

        // Get all the pontoTracadoTrajetoList where longitude equals to DEFAULT_LONGITUDE
        defaultPontoTracadoTrajetoShouldBeFound("longitude.equals=" + DEFAULT_LONGITUDE);

        // Get all the pontoTracadoTrajetoList where longitude equals to UPDATED_LONGITUDE
        defaultPontoTracadoTrajetoShouldNotBeFound("longitude.equals=" + UPDATED_LONGITUDE);
    }

    @Test
    @Transactional
    public void getAllPontoTracadoTrajetosByLongitudeIsInShouldWork() throws Exception {
        // Initialize the database
        pontoTracadoTrajetoRepository.saveAndFlush(pontoTracadoTrajeto);

        // Get all the pontoTracadoTrajetoList where longitude in DEFAULT_LONGITUDE or UPDATED_LONGITUDE
        defaultPontoTracadoTrajetoShouldBeFound("longitude.in=" + DEFAULT_LONGITUDE + "," + UPDATED_LONGITUDE);

        // Get all the pontoTracadoTrajetoList where longitude equals to UPDATED_LONGITUDE
        defaultPontoTracadoTrajetoShouldNotBeFound("longitude.in=" + UPDATED_LONGITUDE);
    }

    @Test
    @Transactional
    public void getAllPontoTracadoTrajetosByLongitudeIsNullOrNotNull() throws Exception {
        // Initialize the database
        pontoTracadoTrajetoRepository.saveAndFlush(pontoTracadoTrajeto);

        // Get all the pontoTracadoTrajetoList where longitude is not null
        defaultPontoTracadoTrajetoShouldBeFound("longitude.specified=true");

        // Get all the pontoTracadoTrajetoList where longitude is null
        defaultPontoTracadoTrajetoShouldNotBeFound("longitude.specified=false");
    }

    @Test
    @Transactional
    public void getAllPontoTracadoTrajetosByPosicaoIsEqualToSomething() throws Exception {
        // Initialize the database
        pontoTracadoTrajetoRepository.saveAndFlush(pontoTracadoTrajeto);

        // Get all the pontoTracadoTrajetoList where posicao equals to DEFAULT_POSICAO
        defaultPontoTracadoTrajetoShouldBeFound("posicao.equals=" + DEFAULT_POSICAO);

        // Get all the pontoTracadoTrajetoList where posicao equals to UPDATED_POSICAO
        defaultPontoTracadoTrajetoShouldNotBeFound("posicao.equals=" + UPDATED_POSICAO);
    }

    @Test
    @Transactional
    public void getAllPontoTracadoTrajetosByPosicaoIsInShouldWork() throws Exception {
        // Initialize the database
        pontoTracadoTrajetoRepository.saveAndFlush(pontoTracadoTrajeto);

        // Get all the pontoTracadoTrajetoList where posicao in DEFAULT_POSICAO or UPDATED_POSICAO
        defaultPontoTracadoTrajetoShouldBeFound("posicao.in=" + DEFAULT_POSICAO + "," + UPDATED_POSICAO);

        // Get all the pontoTracadoTrajetoList where posicao equals to UPDATED_POSICAO
        defaultPontoTracadoTrajetoShouldNotBeFound("posicao.in=" + UPDATED_POSICAO);
    }

    @Test
    @Transactional
    public void getAllPontoTracadoTrajetosByPosicaoIsNullOrNotNull() throws Exception {
        // Initialize the database
        pontoTracadoTrajetoRepository.saveAndFlush(pontoTracadoTrajeto);

        // Get all the pontoTracadoTrajetoList where posicao is not null
        defaultPontoTracadoTrajetoShouldBeFound("posicao.specified=true");

        // Get all the pontoTracadoTrajetoList where posicao is null
        defaultPontoTracadoTrajetoShouldNotBeFound("posicao.specified=false");
    }

    @Test
    @Transactional
    public void getAllPontoTracadoTrajetosByPosicaoIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        pontoTracadoTrajetoRepository.saveAndFlush(pontoTracadoTrajeto);

        // Get all the pontoTracadoTrajetoList where posicao greater than or equals to DEFAULT_POSICAO
        defaultPontoTracadoTrajetoShouldBeFound("posicao.greaterOrEqualThan=" + DEFAULT_POSICAO);

        // Get all the pontoTracadoTrajetoList where posicao greater than or equals to UPDATED_POSICAO
        defaultPontoTracadoTrajetoShouldNotBeFound("posicao.greaterOrEqualThan=" + UPDATED_POSICAO);
    }

    @Test
    @Transactional
    public void getAllPontoTracadoTrajetosByPosicaoIsLessThanSomething() throws Exception {
        // Initialize the database
        pontoTracadoTrajetoRepository.saveAndFlush(pontoTracadoTrajeto);

        // Get all the pontoTracadoTrajetoList where posicao less than or equals to DEFAULT_POSICAO
        defaultPontoTracadoTrajetoShouldNotBeFound("posicao.lessThan=" + DEFAULT_POSICAO);

        // Get all the pontoTracadoTrajetoList where posicao less than or equals to UPDATED_POSICAO
        defaultPontoTracadoTrajetoShouldBeFound("posicao.lessThan=" + UPDATED_POSICAO);
    }


    @Test
    @Transactional
    public void getAllPontoTracadoTrajetosByTipoIsEqualToSomething() throws Exception {
        // Initialize the database
        pontoTracadoTrajetoRepository.saveAndFlush(pontoTracadoTrajeto);

        // Get all the pontoTracadoTrajetoList where tipo equals to DEFAULT_TIPO
        defaultPontoTracadoTrajetoShouldBeFound("tipo.equals=" + DEFAULT_TIPO);

        // Get all the pontoTracadoTrajetoList where tipo equals to UPDATED_TIPO
        defaultPontoTracadoTrajetoShouldNotBeFound("tipo.equals=" + UPDATED_TIPO);
    }

    @Test
    @Transactional
    public void getAllPontoTracadoTrajetosByTipoIsInShouldWork() throws Exception {
        // Initialize the database
        pontoTracadoTrajetoRepository.saveAndFlush(pontoTracadoTrajeto);

        // Get all the pontoTracadoTrajetoList where tipo in DEFAULT_TIPO or UPDATED_TIPO
        defaultPontoTracadoTrajetoShouldBeFound("tipo.in=" + DEFAULT_TIPO + "," + UPDATED_TIPO);

        // Get all the pontoTracadoTrajetoList where tipo equals to UPDATED_TIPO
        defaultPontoTracadoTrajetoShouldNotBeFound("tipo.in=" + UPDATED_TIPO);
    }

    @Test
    @Transactional
    public void getAllPontoTracadoTrajetosByTipoIsNullOrNotNull() throws Exception {
        // Initialize the database
        pontoTracadoTrajetoRepository.saveAndFlush(pontoTracadoTrajeto);

        // Get all the pontoTracadoTrajetoList where tipo is not null
        defaultPontoTracadoTrajetoShouldBeFound("tipo.specified=true");

        // Get all the pontoTracadoTrajetoList where tipo is null
        defaultPontoTracadoTrajetoShouldNotBeFound("tipo.specified=false");
    }

    @Test
    @Transactional
    public void getAllPontoTracadoTrajetosByLinhaIsEqualToSomething() throws Exception {
        // Initialize the database
        Linha linha = LinhaResourceIT.createEntity(em);
        em.persist(linha);
        em.flush();
        pontoTracadoTrajeto.setLinha(linha);
        pontoTracadoTrajetoRepository.saveAndFlush(pontoTracadoTrajeto);
        Long linhaId = linha.getId();

        // Get all the pontoTracadoTrajetoList where linha equals to linhaId
        defaultPontoTracadoTrajetoShouldBeFound("linhaId.equals=" + linhaId);

        // Get all the pontoTracadoTrajetoList where linha equals to linhaId + 1
        defaultPontoTracadoTrajetoShouldNotBeFound("linhaId.equals=" + (linhaId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultPontoTracadoTrajetoShouldBeFound(String filter) throws Exception {
        restPontoTracadoTrajetoMockMvc.perform(get("/api/ponto-tracado-trajetos?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(pontoTracadoTrajeto.getId().intValue())))
            .andExpect(jsonPath("$.[*].latitude").value(hasItem(DEFAULT_LATITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].longitude").value(hasItem(DEFAULT_LONGITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].posicao").value(hasItem(DEFAULT_POSICAO)))
            .andExpect(jsonPath("$.[*].tipo").value(hasItem(DEFAULT_TIPO.toString())));

        // Check, that the count call also returns 1
        restPontoTracadoTrajetoMockMvc.perform(get("/api/ponto-tracado-trajetos/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultPontoTracadoTrajetoShouldNotBeFound(String filter) throws Exception {
        restPontoTracadoTrajetoMockMvc.perform(get("/api/ponto-tracado-trajetos?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restPontoTracadoTrajetoMockMvc.perform(get("/api/ponto-tracado-trajetos/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingPontoTracadoTrajeto() throws Exception {
        // Get the pontoTracadoTrajeto
        restPontoTracadoTrajetoMockMvc.perform(get("/api/ponto-tracado-trajetos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePontoTracadoTrajeto() throws Exception {
        // Initialize the database
        pontoTracadoTrajetoRepository.saveAndFlush(pontoTracadoTrajeto);

        int databaseSizeBeforeUpdate = pontoTracadoTrajetoRepository.findAll().size();

        // Update the pontoTracadoTrajeto
        PontoTracadoTrajeto updatedPontoTracadoTrajeto = pontoTracadoTrajetoRepository.findById(pontoTracadoTrajeto.getId()).get();
        // Disconnect from session so that the updates on updatedPontoTracadoTrajeto are not directly saved in db
        em.detach(updatedPontoTracadoTrajeto);
        updatedPontoTracadoTrajeto
            .latitude(UPDATED_LATITUDE)
            .longitude(UPDATED_LONGITUDE)
            .posicao(UPDATED_POSICAO)
            .tipo(UPDATED_TIPO);
        PontoTracadoTrajetoDTO pontoTracadoTrajetoDTO = pontoTracadoTrajetoMapper.toDto(updatedPontoTracadoTrajeto);

        restPontoTracadoTrajetoMockMvc.perform(put("/api/ponto-tracado-trajetos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pontoTracadoTrajetoDTO)))
            .andExpect(status().isOk());

        // Validate the PontoTracadoTrajeto in the database
        List<PontoTracadoTrajeto> pontoTracadoTrajetoList = pontoTracadoTrajetoRepository.findAll();
        assertThat(pontoTracadoTrajetoList).hasSize(databaseSizeBeforeUpdate);
        PontoTracadoTrajeto testPontoTracadoTrajeto = pontoTracadoTrajetoList.get(pontoTracadoTrajetoList.size() - 1);
        assertThat(testPontoTracadoTrajeto.getLatitude()).isEqualTo(UPDATED_LATITUDE);
        assertThat(testPontoTracadoTrajeto.getLongitude()).isEqualTo(UPDATED_LONGITUDE);
        assertThat(testPontoTracadoTrajeto.getPosicao()).isEqualTo(UPDATED_POSICAO);
        assertThat(testPontoTracadoTrajeto.getTipo()).isEqualTo(UPDATED_TIPO);
    }

    @Test
    @Transactional
    public void updateNonExistingPontoTracadoTrajeto() throws Exception {
        int databaseSizeBeforeUpdate = pontoTracadoTrajetoRepository.findAll().size();

        // Create the PontoTracadoTrajeto
        PontoTracadoTrajetoDTO pontoTracadoTrajetoDTO = pontoTracadoTrajetoMapper.toDto(pontoTracadoTrajeto);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPontoTracadoTrajetoMockMvc.perform(put("/api/ponto-tracado-trajetos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pontoTracadoTrajetoDTO)))
            .andExpect(status().isBadRequest());

        // Validate the PontoTracadoTrajeto in the database
        List<PontoTracadoTrajeto> pontoTracadoTrajetoList = pontoTracadoTrajetoRepository.findAll();
        assertThat(pontoTracadoTrajetoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePontoTracadoTrajeto() throws Exception {
        // Initialize the database
        pontoTracadoTrajetoRepository.saveAndFlush(pontoTracadoTrajeto);

        int databaseSizeBeforeDelete = pontoTracadoTrajetoRepository.findAll().size();

        // Delete the pontoTracadoTrajeto
        restPontoTracadoTrajetoMockMvc.perform(delete("/api/ponto-tracado-trajetos/{id}", pontoTracadoTrajeto.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<PontoTracadoTrajeto> pontoTracadoTrajetoList = pontoTracadoTrajetoRepository.findAll();
        assertThat(pontoTracadoTrajetoList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PontoTracadoTrajeto.class);
        PontoTracadoTrajeto pontoTracadoTrajeto1 = new PontoTracadoTrajeto();
        pontoTracadoTrajeto1.setId(1L);
        PontoTracadoTrajeto pontoTracadoTrajeto2 = new PontoTracadoTrajeto();
        pontoTracadoTrajeto2.setId(pontoTracadoTrajeto1.getId());
        assertThat(pontoTracadoTrajeto1).isEqualTo(pontoTracadoTrajeto2);
        pontoTracadoTrajeto2.setId(2L);
        assertThat(pontoTracadoTrajeto1).isNotEqualTo(pontoTracadoTrajeto2);
        pontoTracadoTrajeto1.setId(null);
        assertThat(pontoTracadoTrajeto1).isNotEqualTo(pontoTracadoTrajeto2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PontoTracadoTrajetoDTO.class);
        PontoTracadoTrajetoDTO pontoTracadoTrajetoDTO1 = new PontoTracadoTrajetoDTO();
        pontoTracadoTrajetoDTO1.setId(1L);
        PontoTracadoTrajetoDTO pontoTracadoTrajetoDTO2 = new PontoTracadoTrajetoDTO();
        assertThat(pontoTracadoTrajetoDTO1).isNotEqualTo(pontoTracadoTrajetoDTO2);
        pontoTracadoTrajetoDTO2.setId(pontoTracadoTrajetoDTO1.getId());
        assertThat(pontoTracadoTrajetoDTO1).isEqualTo(pontoTracadoTrajetoDTO2);
        pontoTracadoTrajetoDTO2.setId(2L);
        assertThat(pontoTracadoTrajetoDTO1).isNotEqualTo(pontoTracadoTrajetoDTO2);
        pontoTracadoTrajetoDTO1.setId(null);
        assertThat(pontoTracadoTrajetoDTO1).isNotEqualTo(pontoTracadoTrajetoDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(pontoTracadoTrajetoMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(pontoTracadoTrajetoMapper.fromId(null)).isNull();
    }
}
