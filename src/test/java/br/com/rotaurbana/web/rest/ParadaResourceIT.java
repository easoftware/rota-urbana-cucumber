package br.com.rotaurbana.web.rest;

import br.com.rotaurbana.RotaurbanacucumberApp;
import br.com.rotaurbana.domain.Parada;
import br.com.rotaurbana.domain.Linha;
import br.com.rotaurbana.repository.ParadaRepository;
import br.com.rotaurbana.service.ParadaService;
import br.com.rotaurbana.service.dto.ParadaDTO;
import br.com.rotaurbana.service.mapper.ParadaMapper;
import br.com.rotaurbana.web.rest.errors.ExceptionTranslator;
import br.com.rotaurbana.service.dto.ParadaCriteria;
import br.com.rotaurbana.service.ParadaQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static br.com.rotaurbana.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link ParadaResource} REST controller.
 */
@SpringBootTest(classes = RotaurbanacucumberApp.class)
public class ParadaResourceIT {

    private static final Double DEFAULT_LATITUDE = 1D;
    private static final Double UPDATED_LATITUDE = 2D;

    private static final Double DEFAULT_LONGITUDE = 1D;
    private static final Double UPDATED_LONGITUDE = 2D;

    private static final Integer DEFAULT_STATUS = 1;
    private static final Integer UPDATED_STATUS = 2;

    private static final String DEFAULT_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_TITLE = "BBBBBBBBBB";

    private static final String DEFAULT_COMMENTS = "AAAAAAAAAA";
    private static final String UPDATED_COMMENTS = "BBBBBBBBBB";

    private static final String DEFAULT_TIPO_DE_ROTA_DA_PARADA = "AAAAAAAAAA";
    private static final String UPDATED_TIPO_DE_ROTA_DA_PARADA = "BBBBBBBBBB";

    @Autowired
    private ParadaRepository paradaRepository;

    @Autowired
    private ParadaMapper paradaMapper;

    @Autowired
    private ParadaService paradaService;

    @Autowired
    private ParadaQueryService paradaQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restParadaMockMvc;

    private Parada parada;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ParadaResource paradaResource = new ParadaResource(paradaService, paradaQueryService);
        this.restParadaMockMvc = MockMvcBuilders.standaloneSetup(paradaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Parada createEntity(EntityManager em) {
        Parada parada = new Parada()
            .latitude(DEFAULT_LATITUDE)
            .longitude(DEFAULT_LONGITUDE)
            .status(DEFAULT_STATUS)
            .title(DEFAULT_TITLE)
            .comments(DEFAULT_COMMENTS)
            .tipoDeRotaDaParada(DEFAULT_TIPO_DE_ROTA_DA_PARADA);
        return parada;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Parada createUpdatedEntity(EntityManager em) {
        Parada parada = new Parada()
            .latitude(UPDATED_LATITUDE)
            .longitude(UPDATED_LONGITUDE)
            .status(UPDATED_STATUS)
            .title(UPDATED_TITLE)
            .comments(UPDATED_COMMENTS)
            .tipoDeRotaDaParada(UPDATED_TIPO_DE_ROTA_DA_PARADA);
        return parada;
    }

    @BeforeEach
    public void initTest() {
        parada = createEntity(em);
    }

    @Test
    @Transactional
    public void createParada() throws Exception {
        int databaseSizeBeforeCreate = paradaRepository.findAll().size();

        // Create the Parada
        ParadaDTO paradaDTO = paradaMapper.toDto(parada);
        restParadaMockMvc.perform(post("/api/paradas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(paradaDTO)))
            .andExpect(status().isCreated());

        // Validate the Parada in the database
        List<Parada> paradaList = paradaRepository.findAll();
        assertThat(paradaList).hasSize(databaseSizeBeforeCreate + 1);
        Parada testParada = paradaList.get(paradaList.size() - 1);
        assertThat(testParada.getLatitude()).isEqualTo(DEFAULT_LATITUDE);
        assertThat(testParada.getLongitude()).isEqualTo(DEFAULT_LONGITUDE);
        assertThat(testParada.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testParada.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testParada.getComments()).isEqualTo(DEFAULT_COMMENTS);
        assertThat(testParada.getTipoDeRotaDaParada()).isEqualTo(DEFAULT_TIPO_DE_ROTA_DA_PARADA);
    }

    @Test
    @Transactional
    public void createParadaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = paradaRepository.findAll().size();

        // Create the Parada with an existing ID
        parada.setId(1L);
        ParadaDTO paradaDTO = paradaMapper.toDto(parada);

        // An entity with an existing ID cannot be created, so this API call must fail
        restParadaMockMvc.perform(post("/api/paradas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(paradaDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Parada in the database
        List<Parada> paradaList = paradaRepository.findAll();
        assertThat(paradaList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllParadas() throws Exception {
        // Initialize the database
        paradaRepository.saveAndFlush(parada);

        // Get all the paradaList
        restParadaMockMvc.perform(get("/api/paradas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(parada.getId().intValue())))
            .andExpect(jsonPath("$.[*].latitude").value(hasItem(DEFAULT_LATITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].longitude").value(hasItem(DEFAULT_LONGITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE.toString())))
            .andExpect(jsonPath("$.[*].comments").value(hasItem(DEFAULT_COMMENTS.toString())))
            .andExpect(jsonPath("$.[*].tipoDeRotaDaParada").value(hasItem(DEFAULT_TIPO_DE_ROTA_DA_PARADA.toString())));
    }
    
    @Test
    @Transactional
    public void getParada() throws Exception {
        // Initialize the database
        paradaRepository.saveAndFlush(parada);

        // Get the parada
        restParadaMockMvc.perform(get("/api/paradas/{id}", parada.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(parada.getId().intValue()))
            .andExpect(jsonPath("$.latitude").value(DEFAULT_LATITUDE.doubleValue()))
            .andExpect(jsonPath("$.longitude").value(DEFAULT_LONGITUDE.doubleValue()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE.toString()))
            .andExpect(jsonPath("$.comments").value(DEFAULT_COMMENTS.toString()))
            .andExpect(jsonPath("$.tipoDeRotaDaParada").value(DEFAULT_TIPO_DE_ROTA_DA_PARADA.toString()));
    }

    @Test
    @Transactional
    public void getAllParadasByLatitudeIsEqualToSomething() throws Exception {
        // Initialize the database
        paradaRepository.saveAndFlush(parada);

        // Get all the paradaList where latitude equals to DEFAULT_LATITUDE
        defaultParadaShouldBeFound("latitude.equals=" + DEFAULT_LATITUDE);

        // Get all the paradaList where latitude equals to UPDATED_LATITUDE
        defaultParadaShouldNotBeFound("latitude.equals=" + UPDATED_LATITUDE);
    }

    @Test
    @Transactional
    public void getAllParadasByLatitudeIsInShouldWork() throws Exception {
        // Initialize the database
        paradaRepository.saveAndFlush(parada);

        // Get all the paradaList where latitude in DEFAULT_LATITUDE or UPDATED_LATITUDE
        defaultParadaShouldBeFound("latitude.in=" + DEFAULT_LATITUDE + "," + UPDATED_LATITUDE);

        // Get all the paradaList where latitude equals to UPDATED_LATITUDE
        defaultParadaShouldNotBeFound("latitude.in=" + UPDATED_LATITUDE);
    }

    @Test
    @Transactional
    public void getAllParadasByLatitudeIsNullOrNotNull() throws Exception {
        // Initialize the database
        paradaRepository.saveAndFlush(parada);

        // Get all the paradaList where latitude is not null
        defaultParadaShouldBeFound("latitude.specified=true");

        // Get all the paradaList where latitude is null
        defaultParadaShouldNotBeFound("latitude.specified=false");
    }

    @Test
    @Transactional
    public void getAllParadasByLongitudeIsEqualToSomething() throws Exception {
        // Initialize the database
        paradaRepository.saveAndFlush(parada);

        // Get all the paradaList where longitude equals to DEFAULT_LONGITUDE
        defaultParadaShouldBeFound("longitude.equals=" + DEFAULT_LONGITUDE);

        // Get all the paradaList where longitude equals to UPDATED_LONGITUDE
        defaultParadaShouldNotBeFound("longitude.equals=" + UPDATED_LONGITUDE);
    }

    @Test
    @Transactional
    public void getAllParadasByLongitudeIsInShouldWork() throws Exception {
        // Initialize the database
        paradaRepository.saveAndFlush(parada);

        // Get all the paradaList where longitude in DEFAULT_LONGITUDE or UPDATED_LONGITUDE
        defaultParadaShouldBeFound("longitude.in=" + DEFAULT_LONGITUDE + "," + UPDATED_LONGITUDE);

        // Get all the paradaList where longitude equals to UPDATED_LONGITUDE
        defaultParadaShouldNotBeFound("longitude.in=" + UPDATED_LONGITUDE);
    }

    @Test
    @Transactional
    public void getAllParadasByLongitudeIsNullOrNotNull() throws Exception {
        // Initialize the database
        paradaRepository.saveAndFlush(parada);

        // Get all the paradaList where longitude is not null
        defaultParadaShouldBeFound("longitude.specified=true");

        // Get all the paradaList where longitude is null
        defaultParadaShouldNotBeFound("longitude.specified=false");
    }

    @Test
    @Transactional
    public void getAllParadasByStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        paradaRepository.saveAndFlush(parada);

        // Get all the paradaList where status equals to DEFAULT_STATUS
        defaultParadaShouldBeFound("status.equals=" + DEFAULT_STATUS);

        // Get all the paradaList where status equals to UPDATED_STATUS
        defaultParadaShouldNotBeFound("status.equals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllParadasByStatusIsInShouldWork() throws Exception {
        // Initialize the database
        paradaRepository.saveAndFlush(parada);

        // Get all the paradaList where status in DEFAULT_STATUS or UPDATED_STATUS
        defaultParadaShouldBeFound("status.in=" + DEFAULT_STATUS + "," + UPDATED_STATUS);

        // Get all the paradaList where status equals to UPDATED_STATUS
        defaultParadaShouldNotBeFound("status.in=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllParadasByStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        paradaRepository.saveAndFlush(parada);

        // Get all the paradaList where status is not null
        defaultParadaShouldBeFound("status.specified=true");

        // Get all the paradaList where status is null
        defaultParadaShouldNotBeFound("status.specified=false");
    }

    @Test
    @Transactional
    public void getAllParadasByStatusIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        paradaRepository.saveAndFlush(parada);

        // Get all the paradaList where status greater than or equals to DEFAULT_STATUS
        defaultParadaShouldBeFound("status.greaterOrEqualThan=" + DEFAULT_STATUS);

        // Get all the paradaList where status greater than or equals to UPDATED_STATUS
        defaultParadaShouldNotBeFound("status.greaterOrEqualThan=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllParadasByStatusIsLessThanSomething() throws Exception {
        // Initialize the database
        paradaRepository.saveAndFlush(parada);

        // Get all the paradaList where status less than or equals to DEFAULT_STATUS
        defaultParadaShouldNotBeFound("status.lessThan=" + DEFAULT_STATUS);

        // Get all the paradaList where status less than or equals to UPDATED_STATUS
        defaultParadaShouldBeFound("status.lessThan=" + UPDATED_STATUS);
    }


    @Test
    @Transactional
    public void getAllParadasByTitleIsEqualToSomething() throws Exception {
        // Initialize the database
        paradaRepository.saveAndFlush(parada);

        // Get all the paradaList where title equals to DEFAULT_TITLE
        defaultParadaShouldBeFound("title.equals=" + DEFAULT_TITLE);

        // Get all the paradaList where title equals to UPDATED_TITLE
        defaultParadaShouldNotBeFound("title.equals=" + UPDATED_TITLE);
    }

    @Test
    @Transactional
    public void getAllParadasByTitleIsInShouldWork() throws Exception {
        // Initialize the database
        paradaRepository.saveAndFlush(parada);

        // Get all the paradaList where title in DEFAULT_TITLE or UPDATED_TITLE
        defaultParadaShouldBeFound("title.in=" + DEFAULT_TITLE + "," + UPDATED_TITLE);

        // Get all the paradaList where title equals to UPDATED_TITLE
        defaultParadaShouldNotBeFound("title.in=" + UPDATED_TITLE);
    }

    @Test
    @Transactional
    public void getAllParadasByTitleIsNullOrNotNull() throws Exception {
        // Initialize the database
        paradaRepository.saveAndFlush(parada);

        // Get all the paradaList where title is not null
        defaultParadaShouldBeFound("title.specified=true");

        // Get all the paradaList where title is null
        defaultParadaShouldNotBeFound("title.specified=false");
    }

    @Test
    @Transactional
    public void getAllParadasByCommentsIsEqualToSomething() throws Exception {
        // Initialize the database
        paradaRepository.saveAndFlush(parada);

        // Get all the paradaList where comments equals to DEFAULT_COMMENTS
        defaultParadaShouldBeFound("comments.equals=" + DEFAULT_COMMENTS);

        // Get all the paradaList where comments equals to UPDATED_COMMENTS
        defaultParadaShouldNotBeFound("comments.equals=" + UPDATED_COMMENTS);
    }

    @Test
    @Transactional
    public void getAllParadasByCommentsIsInShouldWork() throws Exception {
        // Initialize the database
        paradaRepository.saveAndFlush(parada);

        // Get all the paradaList where comments in DEFAULT_COMMENTS or UPDATED_COMMENTS
        defaultParadaShouldBeFound("comments.in=" + DEFAULT_COMMENTS + "," + UPDATED_COMMENTS);

        // Get all the paradaList where comments equals to UPDATED_COMMENTS
        defaultParadaShouldNotBeFound("comments.in=" + UPDATED_COMMENTS);
    }

    @Test
    @Transactional
    public void getAllParadasByCommentsIsNullOrNotNull() throws Exception {
        // Initialize the database
        paradaRepository.saveAndFlush(parada);

        // Get all the paradaList where comments is not null
        defaultParadaShouldBeFound("comments.specified=true");

        // Get all the paradaList where comments is null
        defaultParadaShouldNotBeFound("comments.specified=false");
    }

    @Test
    @Transactional
    public void getAllParadasByTipoDeRotaDaParadaIsEqualToSomething() throws Exception {
        // Initialize the database
        paradaRepository.saveAndFlush(parada);

        // Get all the paradaList where tipoDeRotaDaParada equals to DEFAULT_TIPO_DE_ROTA_DA_PARADA
        defaultParadaShouldBeFound("tipoDeRotaDaParada.equals=" + DEFAULT_TIPO_DE_ROTA_DA_PARADA);

        // Get all the paradaList where tipoDeRotaDaParada equals to UPDATED_TIPO_DE_ROTA_DA_PARADA
        defaultParadaShouldNotBeFound("tipoDeRotaDaParada.equals=" + UPDATED_TIPO_DE_ROTA_DA_PARADA);
    }

    @Test
    @Transactional
    public void getAllParadasByTipoDeRotaDaParadaIsInShouldWork() throws Exception {
        // Initialize the database
        paradaRepository.saveAndFlush(parada);

        // Get all the paradaList where tipoDeRotaDaParada in DEFAULT_TIPO_DE_ROTA_DA_PARADA or UPDATED_TIPO_DE_ROTA_DA_PARADA
        defaultParadaShouldBeFound("tipoDeRotaDaParada.in=" + DEFAULT_TIPO_DE_ROTA_DA_PARADA + "," + UPDATED_TIPO_DE_ROTA_DA_PARADA);

        // Get all the paradaList where tipoDeRotaDaParada equals to UPDATED_TIPO_DE_ROTA_DA_PARADA
        defaultParadaShouldNotBeFound("tipoDeRotaDaParada.in=" + UPDATED_TIPO_DE_ROTA_DA_PARADA);
    }

    @Test
    @Transactional
    public void getAllParadasByTipoDeRotaDaParadaIsNullOrNotNull() throws Exception {
        // Initialize the database
        paradaRepository.saveAndFlush(parada);

        // Get all the paradaList where tipoDeRotaDaParada is not null
        defaultParadaShouldBeFound("tipoDeRotaDaParada.specified=true");

        // Get all the paradaList where tipoDeRotaDaParada is null
        defaultParadaShouldNotBeFound("tipoDeRotaDaParada.specified=false");
    }

    @Test
    @Transactional
    public void getAllParadasByLinhasIsEqualToSomething() throws Exception {
        // Initialize the database
        Linha linhas = LinhaResourceIT.createEntity(em);
        em.persist(linhas);
        em.flush();
        parada.addLinhas(linhas);
        paradaRepository.saveAndFlush(parada);
        Long linhasId = linhas.getId();

        // Get all the paradaList where linhas equals to linhasId
        defaultParadaShouldBeFound("linhasId.equals=" + linhasId);

        // Get all the paradaList where linhas equals to linhasId + 1
        defaultParadaShouldNotBeFound("linhasId.equals=" + (linhasId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultParadaShouldBeFound(String filter) throws Exception {
        restParadaMockMvc.perform(get("/api/paradas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(parada.getId().intValue())))
            .andExpect(jsonPath("$.[*].latitude").value(hasItem(DEFAULT_LATITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].longitude").value(hasItem(DEFAULT_LONGITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE)))
            .andExpect(jsonPath("$.[*].comments").value(hasItem(DEFAULT_COMMENTS)))
            .andExpect(jsonPath("$.[*].tipoDeRotaDaParada").value(hasItem(DEFAULT_TIPO_DE_ROTA_DA_PARADA)));

        // Check, that the count call also returns 1
        restParadaMockMvc.perform(get("/api/paradas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultParadaShouldNotBeFound(String filter) throws Exception {
        restParadaMockMvc.perform(get("/api/paradas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restParadaMockMvc.perform(get("/api/paradas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingParada() throws Exception {
        // Get the parada
        restParadaMockMvc.perform(get("/api/paradas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateParada() throws Exception {
        // Initialize the database
        paradaRepository.saveAndFlush(parada);

        int databaseSizeBeforeUpdate = paradaRepository.findAll().size();

        // Update the parada
        Parada updatedParada = paradaRepository.findById(parada.getId()).get();
        // Disconnect from session so that the updates on updatedParada are not directly saved in db
        em.detach(updatedParada);
        updatedParada
            .latitude(UPDATED_LATITUDE)
            .longitude(UPDATED_LONGITUDE)
            .status(UPDATED_STATUS)
            .title(UPDATED_TITLE)
            .comments(UPDATED_COMMENTS)
            .tipoDeRotaDaParada(UPDATED_TIPO_DE_ROTA_DA_PARADA);
        ParadaDTO paradaDTO = paradaMapper.toDto(updatedParada);

        restParadaMockMvc.perform(put("/api/paradas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(paradaDTO)))
            .andExpect(status().isOk());

        // Validate the Parada in the database
        List<Parada> paradaList = paradaRepository.findAll();
        assertThat(paradaList).hasSize(databaseSizeBeforeUpdate);
        Parada testParada = paradaList.get(paradaList.size() - 1);
        assertThat(testParada.getLatitude()).isEqualTo(UPDATED_LATITUDE);
        assertThat(testParada.getLongitude()).isEqualTo(UPDATED_LONGITUDE);
        assertThat(testParada.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testParada.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testParada.getComments()).isEqualTo(UPDATED_COMMENTS);
        assertThat(testParada.getTipoDeRotaDaParada()).isEqualTo(UPDATED_TIPO_DE_ROTA_DA_PARADA);
    }

    @Test
    @Transactional
    public void updateNonExistingParada() throws Exception {
        int databaseSizeBeforeUpdate = paradaRepository.findAll().size();

        // Create the Parada
        ParadaDTO paradaDTO = paradaMapper.toDto(parada);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restParadaMockMvc.perform(put("/api/paradas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(paradaDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Parada in the database
        List<Parada> paradaList = paradaRepository.findAll();
        assertThat(paradaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteParada() throws Exception {
        // Initialize the database
        paradaRepository.saveAndFlush(parada);

        int databaseSizeBeforeDelete = paradaRepository.findAll().size();

        // Delete the parada
        restParadaMockMvc.perform(delete("/api/paradas/{id}", parada.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<Parada> paradaList = paradaRepository.findAll();
        assertThat(paradaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Parada.class);
        Parada parada1 = new Parada();
        parada1.setId(1L);
        Parada parada2 = new Parada();
        parada2.setId(parada1.getId());
        assertThat(parada1).isEqualTo(parada2);
        parada2.setId(2L);
        assertThat(parada1).isNotEqualTo(parada2);
        parada1.setId(null);
        assertThat(parada1).isNotEqualTo(parada2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ParadaDTO.class);
        ParadaDTO paradaDTO1 = new ParadaDTO();
        paradaDTO1.setId(1L);
        ParadaDTO paradaDTO2 = new ParadaDTO();
        assertThat(paradaDTO1).isNotEqualTo(paradaDTO2);
        paradaDTO2.setId(paradaDTO1.getId());
        assertThat(paradaDTO1).isEqualTo(paradaDTO2);
        paradaDTO2.setId(2L);
        assertThat(paradaDTO1).isNotEqualTo(paradaDTO2);
        paradaDTO1.setId(null);
        assertThat(paradaDTO1).isNotEqualTo(paradaDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(paradaMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(paradaMapper.fromId(null)).isNull();
    }
}
