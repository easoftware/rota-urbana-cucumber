package br.com.rotaurbana.web.rest;

import br.com.rotaurbana.RotaurbanacucumberApp;
import br.com.rotaurbana.domain.PontoPesquisa;
import br.com.rotaurbana.domain.Linha;
import br.com.rotaurbana.repository.PontoPesquisaRepository;
import br.com.rotaurbana.service.PontoPesquisaService;
import br.com.rotaurbana.service.dto.PontoPesquisaDTO;
import br.com.rotaurbana.service.mapper.PontoPesquisaMapper;
import br.com.rotaurbana.web.rest.errors.ExceptionTranslator;
import br.com.rotaurbana.service.dto.PontoPesquisaCriteria;
import br.com.rotaurbana.service.PontoPesquisaQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static br.com.rotaurbana.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import br.com.rotaurbana.domain.enumeration.Tipo;
/**
 * Integration tests for the {@Link PontoPesquisaResource} REST controller.
 */
@SpringBootTest(classes = RotaurbanacucumberApp.class)
public class PontoPesquisaResourceIT {

    private static final Double DEFAULT_LATITUDE = 1D;
    private static final Double UPDATED_LATITUDE = 2D;

    private static final Double DEFAULT_LONGITUDE = 1D;
    private static final Double UPDATED_LONGITUDE = 2D;

    private static final Integer DEFAULT_POSICAO = 1;
    private static final Integer UPDATED_POSICAO = 2;

    private static final Tipo DEFAULT_TIPO = Tipo.IDA;
    private static final Tipo UPDATED_TIPO = Tipo.VOLTA;

    @Autowired
    private PontoPesquisaRepository pontoPesquisaRepository;

    @Autowired
    private PontoPesquisaMapper pontoPesquisaMapper;

    @Autowired
    private PontoPesquisaService pontoPesquisaService;

    @Autowired
    private PontoPesquisaQueryService pontoPesquisaQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restPontoPesquisaMockMvc;

    private PontoPesquisa pontoPesquisa;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PontoPesquisaResource pontoPesquisaResource = new PontoPesquisaResource(pontoPesquisaService, pontoPesquisaQueryService);
        this.restPontoPesquisaMockMvc = MockMvcBuilders.standaloneSetup(pontoPesquisaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PontoPesquisa createEntity(EntityManager em) {
        PontoPesquisa pontoPesquisa = new PontoPesquisa()
            .latitude(DEFAULT_LATITUDE)
            .longitude(DEFAULT_LONGITUDE)
            .posicao(DEFAULT_POSICAO)
            .tipo(DEFAULT_TIPO);
        return pontoPesquisa;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PontoPesquisa createUpdatedEntity(EntityManager em) {
        PontoPesquisa pontoPesquisa = new PontoPesquisa()
            .latitude(UPDATED_LATITUDE)
            .longitude(UPDATED_LONGITUDE)
            .posicao(UPDATED_POSICAO)
            .tipo(UPDATED_TIPO);
        return pontoPesquisa;
    }

    @BeforeEach
    public void initTest() {
        pontoPesquisa = createEntity(em);
    }

    @Test
    @Transactional
    public void createPontoPesquisa() throws Exception {
        int databaseSizeBeforeCreate = pontoPesquisaRepository.findAll().size();

        // Create the PontoPesquisa
        PontoPesquisaDTO pontoPesquisaDTO = pontoPesquisaMapper.toDto(pontoPesquisa);
        restPontoPesquisaMockMvc.perform(post("/api/ponto-pesquisas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pontoPesquisaDTO)))
            .andExpect(status().isCreated());

        // Validate the PontoPesquisa in the database
        List<PontoPesquisa> pontoPesquisaList = pontoPesquisaRepository.findAll();
        assertThat(pontoPesquisaList).hasSize(databaseSizeBeforeCreate + 1);
        PontoPesquisa testPontoPesquisa = pontoPesquisaList.get(pontoPesquisaList.size() - 1);
        assertThat(testPontoPesquisa.getLatitude()).isEqualTo(DEFAULT_LATITUDE);
        assertThat(testPontoPesquisa.getLongitude()).isEqualTo(DEFAULT_LONGITUDE);
        assertThat(testPontoPesquisa.getPosicao()).isEqualTo(DEFAULT_POSICAO);
        assertThat(testPontoPesquisa.getTipo()).isEqualTo(DEFAULT_TIPO);
    }

    @Test
    @Transactional
    public void createPontoPesquisaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = pontoPesquisaRepository.findAll().size();

        // Create the PontoPesquisa with an existing ID
        pontoPesquisa.setId(1L);
        PontoPesquisaDTO pontoPesquisaDTO = pontoPesquisaMapper.toDto(pontoPesquisa);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPontoPesquisaMockMvc.perform(post("/api/ponto-pesquisas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pontoPesquisaDTO)))
            .andExpect(status().isBadRequest());

        // Validate the PontoPesquisa in the database
        List<PontoPesquisa> pontoPesquisaList = pontoPesquisaRepository.findAll();
        assertThat(pontoPesquisaList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllPontoPesquisas() throws Exception {
        // Initialize the database
        pontoPesquisaRepository.saveAndFlush(pontoPesquisa);

        // Get all the pontoPesquisaList
        restPontoPesquisaMockMvc.perform(get("/api/ponto-pesquisas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(pontoPesquisa.getId().intValue())))
            .andExpect(jsonPath("$.[*].latitude").value(hasItem(DEFAULT_LATITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].longitude").value(hasItem(DEFAULT_LONGITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].posicao").value(hasItem(DEFAULT_POSICAO)))
            .andExpect(jsonPath("$.[*].tipo").value(hasItem(DEFAULT_TIPO.toString())));
    }
    
    @Test
    @Transactional
    public void getPontoPesquisa() throws Exception {
        // Initialize the database
        pontoPesquisaRepository.saveAndFlush(pontoPesquisa);

        // Get the pontoPesquisa
        restPontoPesquisaMockMvc.perform(get("/api/ponto-pesquisas/{id}", pontoPesquisa.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(pontoPesquisa.getId().intValue()))
            .andExpect(jsonPath("$.latitude").value(DEFAULT_LATITUDE.doubleValue()))
            .andExpect(jsonPath("$.longitude").value(DEFAULT_LONGITUDE.doubleValue()))
            .andExpect(jsonPath("$.posicao").value(DEFAULT_POSICAO))
            .andExpect(jsonPath("$.tipo").value(DEFAULT_TIPO.toString()));
    }

    @Test
    @Transactional
    public void getAllPontoPesquisasByLatitudeIsEqualToSomething() throws Exception {
        // Initialize the database
        pontoPesquisaRepository.saveAndFlush(pontoPesquisa);

        // Get all the pontoPesquisaList where latitude equals to DEFAULT_LATITUDE
        defaultPontoPesquisaShouldBeFound("latitude.equals=" + DEFAULT_LATITUDE);

        // Get all the pontoPesquisaList where latitude equals to UPDATED_LATITUDE
        defaultPontoPesquisaShouldNotBeFound("latitude.equals=" + UPDATED_LATITUDE);
    }

    @Test
    @Transactional
    public void getAllPontoPesquisasByLatitudeIsInShouldWork() throws Exception {
        // Initialize the database
        pontoPesquisaRepository.saveAndFlush(pontoPesquisa);

        // Get all the pontoPesquisaList where latitude in DEFAULT_LATITUDE or UPDATED_LATITUDE
        defaultPontoPesquisaShouldBeFound("latitude.in=" + DEFAULT_LATITUDE + "," + UPDATED_LATITUDE);

        // Get all the pontoPesquisaList where latitude equals to UPDATED_LATITUDE
        defaultPontoPesquisaShouldNotBeFound("latitude.in=" + UPDATED_LATITUDE);
    }

    @Test
    @Transactional
    public void getAllPontoPesquisasByLatitudeIsNullOrNotNull() throws Exception {
        // Initialize the database
        pontoPesquisaRepository.saveAndFlush(pontoPesquisa);

        // Get all the pontoPesquisaList where latitude is not null
        defaultPontoPesquisaShouldBeFound("latitude.specified=true");

        // Get all the pontoPesquisaList where latitude is null
        defaultPontoPesquisaShouldNotBeFound("latitude.specified=false");
    }

    @Test
    @Transactional
    public void getAllPontoPesquisasByLongitudeIsEqualToSomething() throws Exception {
        // Initialize the database
        pontoPesquisaRepository.saveAndFlush(pontoPesquisa);

        // Get all the pontoPesquisaList where longitude equals to DEFAULT_LONGITUDE
        defaultPontoPesquisaShouldBeFound("longitude.equals=" + DEFAULT_LONGITUDE);

        // Get all the pontoPesquisaList where longitude equals to UPDATED_LONGITUDE
        defaultPontoPesquisaShouldNotBeFound("longitude.equals=" + UPDATED_LONGITUDE);
    }

    @Test
    @Transactional
    public void getAllPontoPesquisasByLongitudeIsInShouldWork() throws Exception {
        // Initialize the database
        pontoPesquisaRepository.saveAndFlush(pontoPesquisa);

        // Get all the pontoPesquisaList where longitude in DEFAULT_LONGITUDE or UPDATED_LONGITUDE
        defaultPontoPesquisaShouldBeFound("longitude.in=" + DEFAULT_LONGITUDE + "," + UPDATED_LONGITUDE);

        // Get all the pontoPesquisaList where longitude equals to UPDATED_LONGITUDE
        defaultPontoPesquisaShouldNotBeFound("longitude.in=" + UPDATED_LONGITUDE);
    }

    @Test
    @Transactional
    public void getAllPontoPesquisasByLongitudeIsNullOrNotNull() throws Exception {
        // Initialize the database
        pontoPesquisaRepository.saveAndFlush(pontoPesquisa);

        // Get all the pontoPesquisaList where longitude is not null
        defaultPontoPesquisaShouldBeFound("longitude.specified=true");

        // Get all the pontoPesquisaList where longitude is null
        defaultPontoPesquisaShouldNotBeFound("longitude.specified=false");
    }

    @Test
    @Transactional
    public void getAllPontoPesquisasByPosicaoIsEqualToSomething() throws Exception {
        // Initialize the database
        pontoPesquisaRepository.saveAndFlush(pontoPesquisa);

        // Get all the pontoPesquisaList where posicao equals to DEFAULT_POSICAO
        defaultPontoPesquisaShouldBeFound("posicao.equals=" + DEFAULT_POSICAO);

        // Get all the pontoPesquisaList where posicao equals to UPDATED_POSICAO
        defaultPontoPesquisaShouldNotBeFound("posicao.equals=" + UPDATED_POSICAO);
    }

    @Test
    @Transactional
    public void getAllPontoPesquisasByPosicaoIsInShouldWork() throws Exception {
        // Initialize the database
        pontoPesquisaRepository.saveAndFlush(pontoPesquisa);

        // Get all the pontoPesquisaList where posicao in DEFAULT_POSICAO or UPDATED_POSICAO
        defaultPontoPesquisaShouldBeFound("posicao.in=" + DEFAULT_POSICAO + "," + UPDATED_POSICAO);

        // Get all the pontoPesquisaList where posicao equals to UPDATED_POSICAO
        defaultPontoPesquisaShouldNotBeFound("posicao.in=" + UPDATED_POSICAO);
    }

    @Test
    @Transactional
    public void getAllPontoPesquisasByPosicaoIsNullOrNotNull() throws Exception {
        // Initialize the database
        pontoPesquisaRepository.saveAndFlush(pontoPesquisa);

        // Get all the pontoPesquisaList where posicao is not null
        defaultPontoPesquisaShouldBeFound("posicao.specified=true");

        // Get all the pontoPesquisaList where posicao is null
        defaultPontoPesquisaShouldNotBeFound("posicao.specified=false");
    }

    @Test
    @Transactional
    public void getAllPontoPesquisasByPosicaoIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        pontoPesquisaRepository.saveAndFlush(pontoPesquisa);

        // Get all the pontoPesquisaList where posicao greater than or equals to DEFAULT_POSICAO
        defaultPontoPesquisaShouldBeFound("posicao.greaterOrEqualThan=" + DEFAULT_POSICAO);

        // Get all the pontoPesquisaList where posicao greater than or equals to UPDATED_POSICAO
        defaultPontoPesquisaShouldNotBeFound("posicao.greaterOrEqualThan=" + UPDATED_POSICAO);
    }

    @Test
    @Transactional
    public void getAllPontoPesquisasByPosicaoIsLessThanSomething() throws Exception {
        // Initialize the database
        pontoPesquisaRepository.saveAndFlush(pontoPesquisa);

        // Get all the pontoPesquisaList where posicao less than or equals to DEFAULT_POSICAO
        defaultPontoPesquisaShouldNotBeFound("posicao.lessThan=" + DEFAULT_POSICAO);

        // Get all the pontoPesquisaList where posicao less than or equals to UPDATED_POSICAO
        defaultPontoPesquisaShouldBeFound("posicao.lessThan=" + UPDATED_POSICAO);
    }


    @Test
    @Transactional
    public void getAllPontoPesquisasByTipoIsEqualToSomething() throws Exception {
        // Initialize the database
        pontoPesquisaRepository.saveAndFlush(pontoPesquisa);

        // Get all the pontoPesquisaList where tipo equals to DEFAULT_TIPO
        defaultPontoPesquisaShouldBeFound("tipo.equals=" + DEFAULT_TIPO);

        // Get all the pontoPesquisaList where tipo equals to UPDATED_TIPO
        defaultPontoPesquisaShouldNotBeFound("tipo.equals=" + UPDATED_TIPO);
    }

    @Test
    @Transactional
    public void getAllPontoPesquisasByTipoIsInShouldWork() throws Exception {
        // Initialize the database
        pontoPesquisaRepository.saveAndFlush(pontoPesquisa);

        // Get all the pontoPesquisaList where tipo in DEFAULT_TIPO or UPDATED_TIPO
        defaultPontoPesquisaShouldBeFound("tipo.in=" + DEFAULT_TIPO + "," + UPDATED_TIPO);

        // Get all the pontoPesquisaList where tipo equals to UPDATED_TIPO
        defaultPontoPesquisaShouldNotBeFound("tipo.in=" + UPDATED_TIPO);
    }

    @Test
    @Transactional
    public void getAllPontoPesquisasByTipoIsNullOrNotNull() throws Exception {
        // Initialize the database
        pontoPesquisaRepository.saveAndFlush(pontoPesquisa);

        // Get all the pontoPesquisaList where tipo is not null
        defaultPontoPesquisaShouldBeFound("tipo.specified=true");

        // Get all the pontoPesquisaList where tipo is null
        defaultPontoPesquisaShouldNotBeFound("tipo.specified=false");
    }

    @Test
    @Transactional
    public void getAllPontoPesquisasByLinhaIsEqualToSomething() throws Exception {
        // Initialize the database
        Linha linha = LinhaResourceIT.createEntity(em);
        em.persist(linha);
        em.flush();
        pontoPesquisa.setLinha(linha);
        pontoPesquisaRepository.saveAndFlush(pontoPesquisa);
        Long linhaId = linha.getId();

        // Get all the pontoPesquisaList where linha equals to linhaId
        defaultPontoPesquisaShouldBeFound("linhaId.equals=" + linhaId);

        // Get all the pontoPesquisaList where linha equals to linhaId + 1
        defaultPontoPesquisaShouldNotBeFound("linhaId.equals=" + (linhaId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultPontoPesquisaShouldBeFound(String filter) throws Exception {
        restPontoPesquisaMockMvc.perform(get("/api/ponto-pesquisas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(pontoPesquisa.getId().intValue())))
            .andExpect(jsonPath("$.[*].latitude").value(hasItem(DEFAULT_LATITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].longitude").value(hasItem(DEFAULT_LONGITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].posicao").value(hasItem(DEFAULT_POSICAO)))
            .andExpect(jsonPath("$.[*].tipo").value(hasItem(DEFAULT_TIPO.toString())));

        // Check, that the count call also returns 1
        restPontoPesquisaMockMvc.perform(get("/api/ponto-pesquisas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultPontoPesquisaShouldNotBeFound(String filter) throws Exception {
        restPontoPesquisaMockMvc.perform(get("/api/ponto-pesquisas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restPontoPesquisaMockMvc.perform(get("/api/ponto-pesquisas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingPontoPesquisa() throws Exception {
        // Get the pontoPesquisa
        restPontoPesquisaMockMvc.perform(get("/api/ponto-pesquisas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePontoPesquisa() throws Exception {
        // Initialize the database
        pontoPesquisaRepository.saveAndFlush(pontoPesquisa);

        int databaseSizeBeforeUpdate = pontoPesquisaRepository.findAll().size();

        // Update the pontoPesquisa
        PontoPesquisa updatedPontoPesquisa = pontoPesquisaRepository.findById(pontoPesquisa.getId()).get();
        // Disconnect from session so that the updates on updatedPontoPesquisa are not directly saved in db
        em.detach(updatedPontoPesquisa);
        updatedPontoPesquisa
            .latitude(UPDATED_LATITUDE)
            .longitude(UPDATED_LONGITUDE)
            .posicao(UPDATED_POSICAO)
            .tipo(UPDATED_TIPO);
        PontoPesquisaDTO pontoPesquisaDTO = pontoPesquisaMapper.toDto(updatedPontoPesquisa);

        restPontoPesquisaMockMvc.perform(put("/api/ponto-pesquisas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pontoPesquisaDTO)))
            .andExpect(status().isOk());

        // Validate the PontoPesquisa in the database
        List<PontoPesquisa> pontoPesquisaList = pontoPesquisaRepository.findAll();
        assertThat(pontoPesquisaList).hasSize(databaseSizeBeforeUpdate);
        PontoPesquisa testPontoPesquisa = pontoPesquisaList.get(pontoPesquisaList.size() - 1);
        assertThat(testPontoPesquisa.getLatitude()).isEqualTo(UPDATED_LATITUDE);
        assertThat(testPontoPesquisa.getLongitude()).isEqualTo(UPDATED_LONGITUDE);
        assertThat(testPontoPesquisa.getPosicao()).isEqualTo(UPDATED_POSICAO);
        assertThat(testPontoPesquisa.getTipo()).isEqualTo(UPDATED_TIPO);
    }

    @Test
    @Transactional
    public void updateNonExistingPontoPesquisa() throws Exception {
        int databaseSizeBeforeUpdate = pontoPesquisaRepository.findAll().size();

        // Create the PontoPesquisa
        PontoPesquisaDTO pontoPesquisaDTO = pontoPesquisaMapper.toDto(pontoPesquisa);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPontoPesquisaMockMvc.perform(put("/api/ponto-pesquisas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pontoPesquisaDTO)))
            .andExpect(status().isBadRequest());

        // Validate the PontoPesquisa in the database
        List<PontoPesquisa> pontoPesquisaList = pontoPesquisaRepository.findAll();
        assertThat(pontoPesquisaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePontoPesquisa() throws Exception {
        // Initialize the database
        pontoPesquisaRepository.saveAndFlush(pontoPesquisa);

        int databaseSizeBeforeDelete = pontoPesquisaRepository.findAll().size();

        // Delete the pontoPesquisa
        restPontoPesquisaMockMvc.perform(delete("/api/ponto-pesquisas/{id}", pontoPesquisa.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<PontoPesquisa> pontoPesquisaList = pontoPesquisaRepository.findAll();
        assertThat(pontoPesquisaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PontoPesquisa.class);
        PontoPesquisa pontoPesquisa1 = new PontoPesquisa();
        pontoPesquisa1.setId(1L);
        PontoPesquisa pontoPesquisa2 = new PontoPesquisa();
        pontoPesquisa2.setId(pontoPesquisa1.getId());
        assertThat(pontoPesquisa1).isEqualTo(pontoPesquisa2);
        pontoPesquisa2.setId(2L);
        assertThat(pontoPesquisa1).isNotEqualTo(pontoPesquisa2);
        pontoPesquisa1.setId(null);
        assertThat(pontoPesquisa1).isNotEqualTo(pontoPesquisa2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PontoPesquisaDTO.class);
        PontoPesquisaDTO pontoPesquisaDTO1 = new PontoPesquisaDTO();
        pontoPesquisaDTO1.setId(1L);
        PontoPesquisaDTO pontoPesquisaDTO2 = new PontoPesquisaDTO();
        assertThat(pontoPesquisaDTO1).isNotEqualTo(pontoPesquisaDTO2);
        pontoPesquisaDTO2.setId(pontoPesquisaDTO1.getId());
        assertThat(pontoPesquisaDTO1).isEqualTo(pontoPesquisaDTO2);
        pontoPesquisaDTO2.setId(2L);
        assertThat(pontoPesquisaDTO1).isNotEqualTo(pontoPesquisaDTO2);
        pontoPesquisaDTO1.setId(null);
        assertThat(pontoPesquisaDTO1).isNotEqualTo(pontoPesquisaDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(pontoPesquisaMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(pontoPesquisaMapper.fromId(null)).isNull();
    }
}
