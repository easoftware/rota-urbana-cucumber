package br.com.rotaurbana.web.rest;

import br.com.rotaurbana.RotaurbanacucumberApp;
import br.com.rotaurbana.domain.Cidade;
import br.com.rotaurbana.domain.Cidade;
import br.com.rotaurbana.domain.Estado;
import br.com.rotaurbana.repository.CidadeRepository;
import br.com.rotaurbana.service.CidadeService;
import br.com.rotaurbana.service.dto.CidadeDTO;
import br.com.rotaurbana.service.mapper.CidadeMapper;
import br.com.rotaurbana.web.rest.errors.ExceptionTranslator;
import br.com.rotaurbana.service.dto.CidadeCriteria;
import br.com.rotaurbana.service.CidadeQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static br.com.rotaurbana.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link CidadeResource} REST controller.
 */
@SpringBootTest(classes = RotaurbanacucumberApp.class)
public class CidadeResourceIT {

    private static final String DEFAULT_NOME = "AAAAAAAAAA";
    private static final String UPDATED_NOME = "BBBBBBBBBB";

    private static final String DEFAULT_LATITUDE = "AAAAAAAAAA";
    private static final String UPDATED_LATITUDE = "BBBBBBBBBB";

    private static final String DEFAULT_LONGITUDE = "AAAAAAAAAA";
    private static final String UPDATED_LONGITUDE = "BBBBBBBBBB";

    private static final Double DEFAULT_LATITUDE_DOUBLE = 1D;
    private static final Double UPDATED_LATITUDE_DOUBLE = 2D;

    private static final Double DEFAULT_LONGITUDE_DOUBLE = 1D;
    private static final Double UPDATED_LONGITUDE_DOUBLE = 2D;

    private static final String DEFAULT_SAME_AS = "AAAAAAAAAA";
    private static final String UPDATED_SAME_AS = "BBBBBBBBBB";

    @Autowired
    private CidadeRepository cidadeRepository;

    @Autowired
    private CidadeMapper cidadeMapper;

    @Autowired
    private CidadeService cidadeService;

    @Autowired
    private CidadeQueryService cidadeQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restCidadeMockMvc;

    private Cidade cidade;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final CidadeResource cidadeResource = new CidadeResource(cidadeService, cidadeQueryService);
        this.restCidadeMockMvc = MockMvcBuilders.standaloneSetup(cidadeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Cidade createEntity(EntityManager em) {
        Cidade cidade = new Cidade()
            .nome(DEFAULT_NOME)
            .latitude(DEFAULT_LATITUDE)
            .longitude(DEFAULT_LONGITUDE)
            .latitudeDouble(DEFAULT_LATITUDE_DOUBLE)
            .longitudeDouble(DEFAULT_LONGITUDE_DOUBLE)
            .sameAs(DEFAULT_SAME_AS);
        return cidade;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Cidade createUpdatedEntity(EntityManager em) {
        Cidade cidade = new Cidade()
            .nome(UPDATED_NOME)
            .latitude(UPDATED_LATITUDE)
            .longitude(UPDATED_LONGITUDE)
            .latitudeDouble(UPDATED_LATITUDE_DOUBLE)
            .longitudeDouble(UPDATED_LONGITUDE_DOUBLE)
            .sameAs(UPDATED_SAME_AS);
        return cidade;
    }

    @BeforeEach
    public void initTest() {
        cidade = createEntity(em);
    }

    @Test
    @Transactional
    public void createCidade() throws Exception {
        int databaseSizeBeforeCreate = cidadeRepository.findAll().size();

        // Create the Cidade
        CidadeDTO cidadeDTO = cidadeMapper.toDto(cidade);
        restCidadeMockMvc.perform(post("/api/cidades")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cidadeDTO)))
            .andExpect(status().isCreated());

        // Validate the Cidade in the database
        List<Cidade> cidadeList = cidadeRepository.findAll();
        assertThat(cidadeList).hasSize(databaseSizeBeforeCreate + 1);
        Cidade testCidade = cidadeList.get(cidadeList.size() - 1);
        assertThat(testCidade.getNome()).isEqualTo(DEFAULT_NOME);
        assertThat(testCidade.getLatitude()).isEqualTo(DEFAULT_LATITUDE);
        assertThat(testCidade.getLongitude()).isEqualTo(DEFAULT_LONGITUDE);
        assertThat(testCidade.getLatitudeDouble()).isEqualTo(DEFAULT_LATITUDE_DOUBLE);
        assertThat(testCidade.getLongitudeDouble()).isEqualTo(DEFAULT_LONGITUDE_DOUBLE);
        assertThat(testCidade.getSameAs()).isEqualTo(DEFAULT_SAME_AS);
    }

    @Test
    @Transactional
    public void createCidadeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = cidadeRepository.findAll().size();

        // Create the Cidade with an existing ID
        cidade.setId(1L);
        CidadeDTO cidadeDTO = cidadeMapper.toDto(cidade);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCidadeMockMvc.perform(post("/api/cidades")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cidadeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Cidade in the database
        List<Cidade> cidadeList = cidadeRepository.findAll();
        assertThat(cidadeList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllCidades() throws Exception {
        // Initialize the database
        cidadeRepository.saveAndFlush(cidade);

        // Get all the cidadeList
        restCidadeMockMvc.perform(get("/api/cidades?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(cidade.getId().intValue())))
            .andExpect(jsonPath("$.[*].nome").value(hasItem(DEFAULT_NOME.toString())))
            .andExpect(jsonPath("$.[*].latitude").value(hasItem(DEFAULT_LATITUDE.toString())))
            .andExpect(jsonPath("$.[*].longitude").value(hasItem(DEFAULT_LONGITUDE.toString())))
            .andExpect(jsonPath("$.[*].latitudeDouble").value(hasItem(DEFAULT_LATITUDE_DOUBLE.doubleValue())))
            .andExpect(jsonPath("$.[*].longitudeDouble").value(hasItem(DEFAULT_LONGITUDE_DOUBLE.doubleValue())))
            .andExpect(jsonPath("$.[*].sameAs").value(hasItem(DEFAULT_SAME_AS.toString())));
    }
    
    @Test
    @Transactional
    public void getCidade() throws Exception {
        // Initialize the database
        cidadeRepository.saveAndFlush(cidade);

        // Get the cidade
        restCidadeMockMvc.perform(get("/api/cidades/{id}", cidade.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(cidade.getId().intValue()))
            .andExpect(jsonPath("$.nome").value(DEFAULT_NOME.toString()))
            .andExpect(jsonPath("$.latitude").value(DEFAULT_LATITUDE.toString()))
            .andExpect(jsonPath("$.longitude").value(DEFAULT_LONGITUDE.toString()))
            .andExpect(jsonPath("$.latitudeDouble").value(DEFAULT_LATITUDE_DOUBLE.doubleValue()))
            .andExpect(jsonPath("$.longitudeDouble").value(DEFAULT_LONGITUDE_DOUBLE.doubleValue()))
            .andExpect(jsonPath("$.sameAs").value(DEFAULT_SAME_AS.toString()));
    }

    @Test
    @Transactional
    public void getAllCidadesByNomeIsEqualToSomething() throws Exception {
        // Initialize the database
        cidadeRepository.saveAndFlush(cidade);

        // Get all the cidadeList where nome equals to DEFAULT_NOME
        defaultCidadeShouldBeFound("nome.equals=" + DEFAULT_NOME);

        // Get all the cidadeList where nome equals to UPDATED_NOME
        defaultCidadeShouldNotBeFound("nome.equals=" + UPDATED_NOME);
    }

    @Test
    @Transactional
    public void getAllCidadesByNomeIsInShouldWork() throws Exception {
        // Initialize the database
        cidadeRepository.saveAndFlush(cidade);

        // Get all the cidadeList where nome in DEFAULT_NOME or UPDATED_NOME
        defaultCidadeShouldBeFound("nome.in=" + DEFAULT_NOME + "," + UPDATED_NOME);

        // Get all the cidadeList where nome equals to UPDATED_NOME
        defaultCidadeShouldNotBeFound("nome.in=" + UPDATED_NOME);
    }

    @Test
    @Transactional
    public void getAllCidadesByNomeIsNullOrNotNull() throws Exception {
        // Initialize the database
        cidadeRepository.saveAndFlush(cidade);

        // Get all the cidadeList where nome is not null
        defaultCidadeShouldBeFound("nome.specified=true");

        // Get all the cidadeList where nome is null
        defaultCidadeShouldNotBeFound("nome.specified=false");
    }

    @Test
    @Transactional
    public void getAllCidadesByLatitudeIsEqualToSomething() throws Exception {
        // Initialize the database
        cidadeRepository.saveAndFlush(cidade);

        // Get all the cidadeList where latitude equals to DEFAULT_LATITUDE
        defaultCidadeShouldBeFound("latitude.equals=" + DEFAULT_LATITUDE);

        // Get all the cidadeList where latitude equals to UPDATED_LATITUDE
        defaultCidadeShouldNotBeFound("latitude.equals=" + UPDATED_LATITUDE);
    }

    @Test
    @Transactional
    public void getAllCidadesByLatitudeIsInShouldWork() throws Exception {
        // Initialize the database
        cidadeRepository.saveAndFlush(cidade);

        // Get all the cidadeList where latitude in DEFAULT_LATITUDE or UPDATED_LATITUDE
        defaultCidadeShouldBeFound("latitude.in=" + DEFAULT_LATITUDE + "," + UPDATED_LATITUDE);

        // Get all the cidadeList where latitude equals to UPDATED_LATITUDE
        defaultCidadeShouldNotBeFound("latitude.in=" + UPDATED_LATITUDE);
    }

    @Test
    @Transactional
    public void getAllCidadesByLatitudeIsNullOrNotNull() throws Exception {
        // Initialize the database
        cidadeRepository.saveAndFlush(cidade);

        // Get all the cidadeList where latitude is not null
        defaultCidadeShouldBeFound("latitude.specified=true");

        // Get all the cidadeList where latitude is null
        defaultCidadeShouldNotBeFound("latitude.specified=false");
    }

    @Test
    @Transactional
    public void getAllCidadesByLongitudeIsEqualToSomething() throws Exception {
        // Initialize the database
        cidadeRepository.saveAndFlush(cidade);

        // Get all the cidadeList where longitude equals to DEFAULT_LONGITUDE
        defaultCidadeShouldBeFound("longitude.equals=" + DEFAULT_LONGITUDE);

        // Get all the cidadeList where longitude equals to UPDATED_LONGITUDE
        defaultCidadeShouldNotBeFound("longitude.equals=" + UPDATED_LONGITUDE);
    }

    @Test
    @Transactional
    public void getAllCidadesByLongitudeIsInShouldWork() throws Exception {
        // Initialize the database
        cidadeRepository.saveAndFlush(cidade);

        // Get all the cidadeList where longitude in DEFAULT_LONGITUDE or UPDATED_LONGITUDE
        defaultCidadeShouldBeFound("longitude.in=" + DEFAULT_LONGITUDE + "," + UPDATED_LONGITUDE);

        // Get all the cidadeList where longitude equals to UPDATED_LONGITUDE
        defaultCidadeShouldNotBeFound("longitude.in=" + UPDATED_LONGITUDE);
    }

    @Test
    @Transactional
    public void getAllCidadesByLongitudeIsNullOrNotNull() throws Exception {
        // Initialize the database
        cidadeRepository.saveAndFlush(cidade);

        // Get all the cidadeList where longitude is not null
        defaultCidadeShouldBeFound("longitude.specified=true");

        // Get all the cidadeList where longitude is null
        defaultCidadeShouldNotBeFound("longitude.specified=false");
    }

    @Test
    @Transactional
    public void getAllCidadesByLatitudeDoubleIsEqualToSomething() throws Exception {
        // Initialize the database
        cidadeRepository.saveAndFlush(cidade);

        // Get all the cidadeList where latitudeDouble equals to DEFAULT_LATITUDE_DOUBLE
        defaultCidadeShouldBeFound("latitudeDouble.equals=" + DEFAULT_LATITUDE_DOUBLE);

        // Get all the cidadeList where latitudeDouble equals to UPDATED_LATITUDE_DOUBLE
        defaultCidadeShouldNotBeFound("latitudeDouble.equals=" + UPDATED_LATITUDE_DOUBLE);
    }

    @Test
    @Transactional
    public void getAllCidadesByLatitudeDoubleIsInShouldWork() throws Exception {
        // Initialize the database
        cidadeRepository.saveAndFlush(cidade);

        // Get all the cidadeList where latitudeDouble in DEFAULT_LATITUDE_DOUBLE or UPDATED_LATITUDE_DOUBLE
        defaultCidadeShouldBeFound("latitudeDouble.in=" + DEFAULT_LATITUDE_DOUBLE + "," + UPDATED_LATITUDE_DOUBLE);

        // Get all the cidadeList where latitudeDouble equals to UPDATED_LATITUDE_DOUBLE
        defaultCidadeShouldNotBeFound("latitudeDouble.in=" + UPDATED_LATITUDE_DOUBLE);
    }

    @Test
    @Transactional
    public void getAllCidadesByLatitudeDoubleIsNullOrNotNull() throws Exception {
        // Initialize the database
        cidadeRepository.saveAndFlush(cidade);

        // Get all the cidadeList where latitudeDouble is not null
        defaultCidadeShouldBeFound("latitudeDouble.specified=true");

        // Get all the cidadeList where latitudeDouble is null
        defaultCidadeShouldNotBeFound("latitudeDouble.specified=false");
    }

    @Test
    @Transactional
    public void getAllCidadesByLongitudeDoubleIsEqualToSomething() throws Exception {
        // Initialize the database
        cidadeRepository.saveAndFlush(cidade);

        // Get all the cidadeList where longitudeDouble equals to DEFAULT_LONGITUDE_DOUBLE
        defaultCidadeShouldBeFound("longitudeDouble.equals=" + DEFAULT_LONGITUDE_DOUBLE);

        // Get all the cidadeList where longitudeDouble equals to UPDATED_LONGITUDE_DOUBLE
        defaultCidadeShouldNotBeFound("longitudeDouble.equals=" + UPDATED_LONGITUDE_DOUBLE);
    }

    @Test
    @Transactional
    public void getAllCidadesByLongitudeDoubleIsInShouldWork() throws Exception {
        // Initialize the database
        cidadeRepository.saveAndFlush(cidade);

        // Get all the cidadeList where longitudeDouble in DEFAULT_LONGITUDE_DOUBLE or UPDATED_LONGITUDE_DOUBLE
        defaultCidadeShouldBeFound("longitudeDouble.in=" + DEFAULT_LONGITUDE_DOUBLE + "," + UPDATED_LONGITUDE_DOUBLE);

        // Get all the cidadeList where longitudeDouble equals to UPDATED_LONGITUDE_DOUBLE
        defaultCidadeShouldNotBeFound("longitudeDouble.in=" + UPDATED_LONGITUDE_DOUBLE);
    }

    @Test
    @Transactional
    public void getAllCidadesByLongitudeDoubleIsNullOrNotNull() throws Exception {
        // Initialize the database
        cidadeRepository.saveAndFlush(cidade);

        // Get all the cidadeList where longitudeDouble is not null
        defaultCidadeShouldBeFound("longitudeDouble.specified=true");

        // Get all the cidadeList where longitudeDouble is null
        defaultCidadeShouldNotBeFound("longitudeDouble.specified=false");
    }

    @Test
    @Transactional
    public void getAllCidadesBySameAsIsEqualToSomething() throws Exception {
        // Initialize the database
        cidadeRepository.saveAndFlush(cidade);

        // Get all the cidadeList where sameAs equals to DEFAULT_SAME_AS
        defaultCidadeShouldBeFound("sameAs.equals=" + DEFAULT_SAME_AS);

        // Get all the cidadeList where sameAs equals to UPDATED_SAME_AS
        defaultCidadeShouldNotBeFound("sameAs.equals=" + UPDATED_SAME_AS);
    }

    @Test
    @Transactional
    public void getAllCidadesBySameAsIsInShouldWork() throws Exception {
        // Initialize the database
        cidadeRepository.saveAndFlush(cidade);

        // Get all the cidadeList where sameAs in DEFAULT_SAME_AS or UPDATED_SAME_AS
        defaultCidadeShouldBeFound("sameAs.in=" + DEFAULT_SAME_AS + "," + UPDATED_SAME_AS);

        // Get all the cidadeList where sameAs equals to UPDATED_SAME_AS
        defaultCidadeShouldNotBeFound("sameAs.in=" + UPDATED_SAME_AS);
    }

    @Test
    @Transactional
    public void getAllCidadesBySameAsIsNullOrNotNull() throws Exception {
        // Initialize the database
        cidadeRepository.saveAndFlush(cidade);

        // Get all the cidadeList where sameAs is not null
        defaultCidadeShouldBeFound("sameAs.specified=true");

        // Get all the cidadeList where sameAs is null
        defaultCidadeShouldNotBeFound("sameAs.specified=false");
    }

    @Test
    @Transactional
    public void getAllCidadesByBelongsToIsEqualToSomething() throws Exception {
        // Initialize the database
        Cidade belongsTo = CidadeResourceIT.createEntity(em);
        em.persist(belongsTo);
        em.flush();
        cidade.setBelongsTo(belongsTo);
        cidadeRepository.saveAndFlush(cidade);
        Long belongsToId = belongsTo.getId();

        // Get all the cidadeList where belongsTo equals to belongsToId
        defaultCidadeShouldBeFound("belongsToId.equals=" + belongsToId);

        // Get all the cidadeList where belongsTo equals to belongsToId + 1
        defaultCidadeShouldNotBeFound("belongsToId.equals=" + (belongsToId + 1));
    }


    @Test
    @Transactional
    public void getAllCidadesByEstadoIsEqualToSomething() throws Exception {
        // Initialize the database
        Estado estado = EstadoResourceIT.createEntity(em);
        em.persist(estado);
        em.flush();
        cidade.setEstado(estado);
        cidadeRepository.saveAndFlush(cidade);
        Long estadoId = estado.getId();

        // Get all the cidadeList where estado equals to estadoId
        defaultCidadeShouldBeFound("estadoId.equals=" + estadoId);

        // Get all the cidadeList where estado equals to estadoId + 1
        defaultCidadeShouldNotBeFound("estadoId.equals=" + (estadoId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultCidadeShouldBeFound(String filter) throws Exception {
        restCidadeMockMvc.perform(get("/api/cidades?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(cidade.getId().intValue())))
            .andExpect(jsonPath("$.[*].nome").value(hasItem(DEFAULT_NOME)))
            .andExpect(jsonPath("$.[*].latitude").value(hasItem(DEFAULT_LATITUDE)))
            .andExpect(jsonPath("$.[*].longitude").value(hasItem(DEFAULT_LONGITUDE)))
            .andExpect(jsonPath("$.[*].latitudeDouble").value(hasItem(DEFAULT_LATITUDE_DOUBLE.doubleValue())))
            .andExpect(jsonPath("$.[*].longitudeDouble").value(hasItem(DEFAULT_LONGITUDE_DOUBLE.doubleValue())))
            .andExpect(jsonPath("$.[*].sameAs").value(hasItem(DEFAULT_SAME_AS)));

        // Check, that the count call also returns 1
        restCidadeMockMvc.perform(get("/api/cidades/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultCidadeShouldNotBeFound(String filter) throws Exception {
        restCidadeMockMvc.perform(get("/api/cidades?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restCidadeMockMvc.perform(get("/api/cidades/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingCidade() throws Exception {
        // Get the cidade
        restCidadeMockMvc.perform(get("/api/cidades/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCidade() throws Exception {
        // Initialize the database
        cidadeRepository.saveAndFlush(cidade);

        int databaseSizeBeforeUpdate = cidadeRepository.findAll().size();

        // Update the cidade
        Cidade updatedCidade = cidadeRepository.findById(cidade.getId()).get();
        // Disconnect from session so that the updates on updatedCidade are not directly saved in db
        em.detach(updatedCidade);
        updatedCidade
            .nome(UPDATED_NOME)
            .latitude(UPDATED_LATITUDE)
            .longitude(UPDATED_LONGITUDE)
            .latitudeDouble(UPDATED_LATITUDE_DOUBLE)
            .longitudeDouble(UPDATED_LONGITUDE_DOUBLE)
            .sameAs(UPDATED_SAME_AS);
        CidadeDTO cidadeDTO = cidadeMapper.toDto(updatedCidade);

        restCidadeMockMvc.perform(put("/api/cidades")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cidadeDTO)))
            .andExpect(status().isOk());

        // Validate the Cidade in the database
        List<Cidade> cidadeList = cidadeRepository.findAll();
        assertThat(cidadeList).hasSize(databaseSizeBeforeUpdate);
        Cidade testCidade = cidadeList.get(cidadeList.size() - 1);
        assertThat(testCidade.getNome()).isEqualTo(UPDATED_NOME);
        assertThat(testCidade.getLatitude()).isEqualTo(UPDATED_LATITUDE);
        assertThat(testCidade.getLongitude()).isEqualTo(UPDATED_LONGITUDE);
        assertThat(testCidade.getLatitudeDouble()).isEqualTo(UPDATED_LATITUDE_DOUBLE);
        assertThat(testCidade.getLongitudeDouble()).isEqualTo(UPDATED_LONGITUDE_DOUBLE);
        assertThat(testCidade.getSameAs()).isEqualTo(UPDATED_SAME_AS);
    }

    @Test
    @Transactional
    public void updateNonExistingCidade() throws Exception {
        int databaseSizeBeforeUpdate = cidadeRepository.findAll().size();

        // Create the Cidade
        CidadeDTO cidadeDTO = cidadeMapper.toDto(cidade);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCidadeMockMvc.perform(put("/api/cidades")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cidadeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Cidade in the database
        List<Cidade> cidadeList = cidadeRepository.findAll();
        assertThat(cidadeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCidade() throws Exception {
        // Initialize the database
        cidadeRepository.saveAndFlush(cidade);

        int databaseSizeBeforeDelete = cidadeRepository.findAll().size();

        // Delete the cidade
        restCidadeMockMvc.perform(delete("/api/cidades/{id}", cidade.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<Cidade> cidadeList = cidadeRepository.findAll();
        assertThat(cidadeList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Cidade.class);
        Cidade cidade1 = new Cidade();
        cidade1.setId(1L);
        Cidade cidade2 = new Cidade();
        cidade2.setId(cidade1.getId());
        assertThat(cidade1).isEqualTo(cidade2);
        cidade2.setId(2L);
        assertThat(cidade1).isNotEqualTo(cidade2);
        cidade1.setId(null);
        assertThat(cidade1).isNotEqualTo(cidade2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CidadeDTO.class);
        CidadeDTO cidadeDTO1 = new CidadeDTO();
        cidadeDTO1.setId(1L);
        CidadeDTO cidadeDTO2 = new CidadeDTO();
        assertThat(cidadeDTO1).isNotEqualTo(cidadeDTO2);
        cidadeDTO2.setId(cidadeDTO1.getId());
        assertThat(cidadeDTO1).isEqualTo(cidadeDTO2);
        cidadeDTO2.setId(2L);
        assertThat(cidadeDTO1).isNotEqualTo(cidadeDTO2);
        cidadeDTO1.setId(null);
        assertThat(cidadeDTO1).isNotEqualTo(cidadeDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(cidadeMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(cidadeMapper.fromId(null)).isNull();
    }
}
