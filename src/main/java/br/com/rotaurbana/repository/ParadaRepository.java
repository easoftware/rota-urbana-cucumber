package br.com.rotaurbana.repository;

import br.com.rotaurbana.domain.Parada;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Parada entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ParadaRepository extends JpaRepository<Parada, Long>, JpaSpecificationExecutor<Parada> {

}
