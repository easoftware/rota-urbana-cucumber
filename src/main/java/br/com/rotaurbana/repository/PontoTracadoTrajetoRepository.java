package br.com.rotaurbana.repository;

import br.com.rotaurbana.domain.PontoTracadoTrajeto;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the PontoTracadoTrajeto entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PontoTracadoTrajetoRepository extends JpaRepository<PontoTracadoTrajeto, Long>, JpaSpecificationExecutor<PontoTracadoTrajeto> {

}
