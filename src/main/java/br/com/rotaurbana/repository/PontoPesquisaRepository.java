package br.com.rotaurbana.repository;

import br.com.rotaurbana.domain.PontoPesquisa;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the PontoPesquisa entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PontoPesquisaRepository extends JpaRepository<PontoPesquisa, Long>, JpaSpecificationExecutor<PontoPesquisa> {

}
