package br.com.rotaurbana.repository;

import br.com.rotaurbana.domain.Linha;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Linha entity.
 */
@Repository
public interface LinhaRepository extends JpaRepository<Linha, Long>, JpaSpecificationExecutor<Linha> {

    @Query(value = "select distinct linha from Linha linha left join fetch linha.paradas",
        countQuery = "select count(distinct linha) from Linha linha")
    Page<Linha> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct linha from Linha linha left join fetch linha.paradas")
    List<Linha> findAllWithEagerRelationships();

    @Query("select linha from Linha linha left join fetch linha.paradas where linha.id =:id")
    Optional<Linha> findOneWithEagerRelationships(@Param("id") Long id);

}
