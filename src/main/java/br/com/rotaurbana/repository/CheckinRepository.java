package br.com.rotaurbana.repository;

import br.com.rotaurbana.domain.Checkin;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Checkin entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CheckinRepository extends JpaRepository<Checkin, Long>, JpaSpecificationExecutor<Checkin> {

}
