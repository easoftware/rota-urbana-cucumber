package br.com.rotaurbana.domain.enumeration;

/**
 * The Tipo enumeration.
 */
public enum Tipo {
    IDA, VOLTA, TRACADO
}
