package br.com.rotaurbana.domain.enumeration;

/**
 * The TipoDeRota enumeration.
 */
public enum TipoDeRota {
    ONIBUS, CIRCUITO, PATINS, BICICLETA, FLUVIAL, INTERMUNICIPAL
}
