package br.com.rotaurbana.domain;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Parada.
 */
@Entity
@Table(name = "parada")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Parada implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "latitude")
    private Double latitude;

    @Column(name = "longitude")
    private Double longitude;

    @Column(name = "status")
    private Integer status;

    @Column(name = "title")
    private String title;

    @Column(name = "comments")
    private String comments;

    @Column(name = "tipo_de_rota_da_parada")
    private String tipoDeRotaDaParada;

    @ManyToMany(mappedBy = "paradas")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JsonIgnore
    private Set<Linha> linhas = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Parada latitude(Double latitude) {
        this.latitude = latitude;
        return this;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public Parada longitude(Double longitude) {
        this.longitude = longitude;
        return this;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Integer getStatus() {
        return status;
    }

    public Parada status(Integer status) {
        this.status = status;
        return this;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public Parada title(String title) {
        this.title = title;
        return this;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getComments() {
        return comments;
    }

    public Parada comments(String comments) {
        this.comments = comments;
        return this;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getTipoDeRotaDaParada() {
        return tipoDeRotaDaParada;
    }

    public Parada tipoDeRotaDaParada(String tipoDeRotaDaParada) {
        this.tipoDeRotaDaParada = tipoDeRotaDaParada;
        return this;
    }

    public void setTipoDeRotaDaParada(String tipoDeRotaDaParada) {
        this.tipoDeRotaDaParada = tipoDeRotaDaParada;
    }

    public Set<Linha> getLinhas() {
        return linhas;
    }

    public Parada linhas(Set<Linha> linhas) {
        this.linhas = linhas;
        return this;
    }

    public Parada addLinhas(Linha linha) {
        this.linhas.add(linha);
        linha.getParadas().add(this);
        return this;
    }

    public Parada removeLinhas(Linha linha) {
        this.linhas.remove(linha);
        linha.getParadas().remove(this);
        return this;
    }

    public void setLinhas(Set<Linha> linhas) {
        this.linhas = linhas;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Parada)) {
            return false;
        }
        return id != null && id.equals(((Parada) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Parada{" +
            "id=" + getId() +
            ", latitude=" + getLatitude() +
            ", longitude=" + getLongitude() +
            ", status=" + getStatus() +
            ", title='" + getTitle() + "'" +
            ", comments='" + getComments() + "'" +
            ", tipoDeRotaDaParada='" + getTipoDeRotaDaParada() + "'" +
            "}";
    }
}
