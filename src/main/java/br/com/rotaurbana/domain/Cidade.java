package br.com.rotaurbana.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A Cidade.
 */
@Entity
@Table(name = "cidade")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Cidade implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nome")
    private String nome;

    @Column(name = "latitude")
    private String latitude;

    @Column(name = "longitude")
    private String longitude;

    @Column(name = "latitude_double")
    private Double latitudeDouble;

    @Column(name = "longitude_double")
    private Double longitudeDouble;

    @Column(name = "same_as")
    private String sameAs;

    @ManyToOne
    @JsonIgnoreProperties("cidades")
    private Cidade belongsTo;

    @ManyToOne
    @JsonIgnoreProperties("cidades")
    private Estado estado;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public Cidade nome(String nome) {
        this.nome = nome;
        return this;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getLatitude() {
        return latitude;
    }

    public Cidade latitude(String latitude) {
        this.latitude = latitude;
        return this;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public Cidade longitude(String longitude) {
        this.longitude = longitude;
        return this;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public Double getLatitudeDouble() {
        return latitudeDouble;
    }

    public Cidade latitudeDouble(Double latitudeDouble) {
        this.latitudeDouble = latitudeDouble;
        return this;
    }

    public void setLatitudeDouble(Double latitudeDouble) {
        this.latitudeDouble = latitudeDouble;
    }

    public Double getLongitudeDouble() {
        return longitudeDouble;
    }

    public Cidade longitudeDouble(Double longitudeDouble) {
        this.longitudeDouble = longitudeDouble;
        return this;
    }

    public void setLongitudeDouble(Double longitudeDouble) {
        this.longitudeDouble = longitudeDouble;
    }

    public String getSameAs() {
        return sameAs;
    }

    public Cidade sameAs(String sameAs) {
        this.sameAs = sameAs;
        return this;
    }

    public void setSameAs(String sameAs) {
        this.sameAs = sameAs;
    }

    public Cidade getBelongsTo() {
        return belongsTo;
    }

    public Cidade belongsTo(Cidade cidade) {
        this.belongsTo = cidade;
        return this;
    }

    public void setBelongsTo(Cidade cidade) {
        this.belongsTo = cidade;
    }

    public Estado getEstado() {
        return estado;
    }

    public Cidade estado(Estado estado) {
        this.estado = estado;
        return this;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Cidade)) {
            return false;
        }
        return id != null && id.equals(((Cidade) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Cidade{" +
            "id=" + getId() +
            ", nome='" + getNome() + "'" +
            ", latitude='" + getLatitude() + "'" +
            ", longitude='" + getLongitude() + "'" +
            ", latitudeDouble=" + getLatitudeDouble() +
            ", longitudeDouble=" + getLongitudeDouble() +
            ", sameAs='" + getSameAs() + "'" +
            "}";
    }
}
