package br.com.rotaurbana.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;

import br.com.rotaurbana.domain.enumeration.Tipo;

/**
 * A PontoTracadoTrajeto.
 */
@Entity
@Table(name = "ponto_tracado_trajeto")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class PontoTracadoTrajeto implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "latitude")
    private Double latitude;

    @Column(name = "longitude")
    private Double longitude;

    @Column(name = "posicao")
    private Integer posicao;

    @Enumerated(EnumType.STRING)
    @Column(name = "tipo")
    private Tipo tipo;

    @ManyToOne
    @JsonIgnoreProperties("itinerarios")
    private Linha linha;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getLatitude() {
        return latitude;
    }

    public PontoTracadoTrajeto latitude(Double latitude) {
        this.latitude = latitude;
        return this;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public PontoTracadoTrajeto longitude(Double longitude) {
        this.longitude = longitude;
        return this;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Integer getPosicao() {
        return posicao;
    }

    public PontoTracadoTrajeto posicao(Integer posicao) {
        this.posicao = posicao;
        return this;
    }

    public void setPosicao(Integer posicao) {
        this.posicao = posicao;
    }

    public Tipo getTipo() {
        return tipo;
    }

    public PontoTracadoTrajeto tipo(Tipo tipo) {
        this.tipo = tipo;
        return this;
    }

    public void setTipo(Tipo tipo) {
        this.tipo = tipo;
    }

    public Linha getLinha() {
        return linha;
    }

    public PontoTracadoTrajeto linha(Linha linha) {
        this.linha = linha;
        return this;
    }

    public void setLinha(Linha linha) {
        this.linha = linha;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PontoTracadoTrajeto)) {
            return false;
        }
        return id != null && id.equals(((PontoTracadoTrajeto) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "PontoTracadoTrajeto{" +
            "id=" + getId() +
            ", latitude=" + getLatitude() +
            ", longitude=" + getLongitude() +
            ", posicao=" + getPosicao() +
            ", tipo='" + getTipo() + "'" +
            "}";
    }
}
