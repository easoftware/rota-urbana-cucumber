package br.com.rotaurbana.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

import br.com.rotaurbana.domain.enumeration.TipoDeRota;

/**
 * A Linha.
 */
@Entity
@Table(name = "linha")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Linha implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nome")
    private String nome;

    @Column(name = "codigo")
    private String codigo;

    @Column(name = "url")
    private String url;

    @Column(name = "completa")
    private Boolean completa;

    @Column(name = "last_update")
    private ZonedDateTime lastUpdate;

    @Column(name = "comentario")
    private String comentario;

    @Size(max = 2000)
    @Column(name = "itinerario_total_encoding", length = 2000)
    private String itinerarioTotalEncoding;

    @Column(name = "em_avaliacao")
    private Boolean emAvaliacao;

    @Column(name = "falta_cadastrar_pontos_pesquisa")
    private Boolean faltaCadastrarPontosPesquisa;

    @Column(name = "semob")
    private Boolean semob;

    @Enumerated(EnumType.STRING)
    @Column(name = "tipo_de_rota")
    private TipoDeRota tipoDeRota;

    @OneToMany(mappedBy = "linha")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<PontoTracadoTrajeto> itinerarios = new HashSet<>();

    @OneToMany(mappedBy = "linha")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<PontoPesquisa> pontosPesquisas = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("linhas")
    private Cidade cidade;

    @ManyToOne
    @JsonIgnoreProperties("children")
    private Linha root;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "linha_paradas",
               joinColumns = @JoinColumn(name = "linha_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "paradas_id", referencedColumnName = "id"))
    private Set<Parada> paradas = new HashSet<>();

    @OneToMany(mappedBy = "root")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Linha> children = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public Linha nome(String nome) {
        this.nome = nome;
        return this;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCodigo() {
        return codigo;
    }

    public Linha codigo(String codigo) {
        this.codigo = codigo;
        return this;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getUrl() {
        return url;
    }

    public Linha url(String url) {
        this.url = url;
        return this;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Boolean isCompleta() {
        return completa;
    }

    public Linha completa(Boolean completa) {
        this.completa = completa;
        return this;
    }

    public void setCompleta(Boolean completa) {
        this.completa = completa;
    }

    public ZonedDateTime getLastUpdate() {
        return lastUpdate;
    }

    public Linha lastUpdate(ZonedDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
        return this;
    }

    public void setLastUpdate(ZonedDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getComentario() {
        return comentario;
    }

    public Linha comentario(String comentario) {
        this.comentario = comentario;
        return this;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public String getItinerarioTotalEncoding() {
        return itinerarioTotalEncoding;
    }

    public Linha itinerarioTotalEncoding(String itinerarioTotalEncoding) {
        this.itinerarioTotalEncoding = itinerarioTotalEncoding;
        return this;
    }

    public void setItinerarioTotalEncoding(String itinerarioTotalEncoding) {
        this.itinerarioTotalEncoding = itinerarioTotalEncoding;
    }

    public Boolean isEmAvaliacao() {
        return emAvaliacao;
    }

    public Linha emAvaliacao(Boolean emAvaliacao) {
        this.emAvaliacao = emAvaliacao;
        return this;
    }

    public void setEmAvaliacao(Boolean emAvaliacao) {
        this.emAvaliacao = emAvaliacao;
    }

    public Boolean isFaltaCadastrarPontosPesquisa() {
        return faltaCadastrarPontosPesquisa;
    }

    public Linha faltaCadastrarPontosPesquisa(Boolean faltaCadastrarPontosPesquisa) {
        this.faltaCadastrarPontosPesquisa = faltaCadastrarPontosPesquisa;
        return this;
    }

    public void setFaltaCadastrarPontosPesquisa(Boolean faltaCadastrarPontosPesquisa) {
        this.faltaCadastrarPontosPesquisa = faltaCadastrarPontosPesquisa;
    }

    public Boolean isSemob() {
        return semob;
    }

    public Linha semob(Boolean semob) {
        this.semob = semob;
        return this;
    }

    public void setSemob(Boolean semob) {
        this.semob = semob;
    }

    public TipoDeRota getTipoDeRota() {
        return tipoDeRota;
    }

    public Linha tipoDeRota(TipoDeRota tipoDeRota) {
        this.tipoDeRota = tipoDeRota;
        return this;
    }

    public void setTipoDeRota(TipoDeRota tipoDeRota) {
        this.tipoDeRota = tipoDeRota;
    }

    public Set<PontoTracadoTrajeto> getItinerarios() {
        return itinerarios;
    }

    public Linha itinerarios(Set<PontoTracadoTrajeto> pontoTracadoTrajetos) {
        this.itinerarios = pontoTracadoTrajetos;
        return this;
    }

    public Linha addItinerario(PontoTracadoTrajeto pontoTracadoTrajeto) {
        this.itinerarios.add(pontoTracadoTrajeto);
        pontoTracadoTrajeto.setLinha(this);
        return this;
    }

    public Linha removeItinerario(PontoTracadoTrajeto pontoTracadoTrajeto) {
        this.itinerarios.remove(pontoTracadoTrajeto);
        pontoTracadoTrajeto.setLinha(null);
        return this;
    }

    public void setItinerarios(Set<PontoTracadoTrajeto> pontoTracadoTrajetos) {
        this.itinerarios = pontoTracadoTrajetos;
    }

    public Set<PontoPesquisa> getPontosPesquisas() {
        return pontosPesquisas;
    }

    public Linha pontosPesquisas(Set<PontoPesquisa> pontoPesquisas) {
        this.pontosPesquisas = pontoPesquisas;
        return this;
    }

    public Linha addPontosPesquisa(PontoPesquisa pontoPesquisa) {
        this.pontosPesquisas.add(pontoPesquisa);
        pontoPesquisa.setLinha(this);
        return this;
    }

    public Linha removePontosPesquisa(PontoPesquisa pontoPesquisa) {
        this.pontosPesquisas.remove(pontoPesquisa);
        pontoPesquisa.setLinha(null);
        return this;
    }

    public void setPontosPesquisas(Set<PontoPesquisa> pontoPesquisas) {
        this.pontosPesquisas = pontoPesquisas;
    }

    public Cidade getCidade() {
        return cidade;
    }

    public Linha cidade(Cidade cidade) {
        this.cidade = cidade;
        return this;
    }

    public void setCidade(Cidade cidade) {
        this.cidade = cidade;
    }

    public Linha getRoot() {
        return root;
    }

    public Linha root(Linha linha) {
        this.root = linha;
        return this;
    }

    public void setRoot(Linha linha) {
        this.root = linha;
    }

    public Set<Parada> getParadas() {
        return paradas;
    }

    public Linha paradas(Set<Parada> paradas) {
        this.paradas = paradas;
        return this;
    }

    public Linha addParadas(Parada parada) {
        this.paradas.add(parada);
        parada.getLinhas().add(this);
        return this;
    }

    public Linha removeParadas(Parada parada) {
        this.paradas.remove(parada);
        parada.getLinhas().remove(this);
        return this;
    }

    public void setParadas(Set<Parada> paradas) {
        this.paradas = paradas;
    }

    public Set<Linha> getChildren() {
        return children;
    }

    public Linha children(Set<Linha> linhas) {
        this.children = linhas;
        return this;
    }

    public Linha addChildren(Linha linha) {
        this.children.add(linha);
        linha.setRoot(this);
        return this;
    }

    public Linha removeChildren(Linha linha) {
        this.children.remove(linha);
        linha.setRoot(null);
        return this;
    }

    public void setChildren(Set<Linha> linhas) {
        this.children = linhas;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Linha)) {
            return false;
        }
        return id != null && id.equals(((Linha) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Linha{" +
            "id=" + getId() +
            ", nome='" + getNome() + "'" +
            ", codigo='" + getCodigo() + "'" +
            ", url='" + getUrl() + "'" +
            ", completa='" + isCompleta() + "'" +
            ", lastUpdate='" + getLastUpdate() + "'" +
            ", comentario='" + getComentario() + "'" +
            ", itinerarioTotalEncoding='" + getItinerarioTotalEncoding() + "'" +
            ", emAvaliacao='" + isEmAvaliacao() + "'" +
            ", faltaCadastrarPontosPesquisa='" + isFaltaCadastrarPontosPesquisa() + "'" +
            ", semob='" + isSemob() + "'" +
            ", tipoDeRota='" + getTipoDeRota() + "'" +
            "}";
    }
}
