package br.com.rotaurbana.web.rest;

import br.com.rotaurbana.service.PontoService;
import br.com.rotaurbana.web.rest.errors.BadRequestAlertException;
import br.com.rotaurbana.service.dto.PontoDTO;
import br.com.rotaurbana.service.dto.PontoCriteria;
import br.com.rotaurbana.service.PontoQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link br.com.rotaurbana.domain.Ponto}.
 */
@RestController
@RequestMapping("/api")
public class PontoResource {

    private final Logger log = LoggerFactory.getLogger(PontoResource.class);

    private static final String ENTITY_NAME = "ponto";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PontoService pontoService;

    private final PontoQueryService pontoQueryService;

    public PontoResource(PontoService pontoService, PontoQueryService pontoQueryService) {
        this.pontoService = pontoService;
        this.pontoQueryService = pontoQueryService;
    }

    /**
     * {@code POST  /pontos} : Create a new ponto.
     *
     * @param pontoDTO the pontoDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new pontoDTO, or with status {@code 400 (Bad Request)} if the ponto has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/pontos")
    public ResponseEntity<PontoDTO> createPonto(@RequestBody PontoDTO pontoDTO) throws URISyntaxException {
        log.debug("REST request to save Ponto : {}", pontoDTO);
        if (pontoDTO.getId() != null) {
            throw new BadRequestAlertException("A new ponto cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PontoDTO result = pontoService.save(pontoDTO);
        return ResponseEntity.created(new URI("/api/pontos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /pontos} : Updates an existing ponto.
     *
     * @param pontoDTO the pontoDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated pontoDTO,
     * or with status {@code 400 (Bad Request)} if the pontoDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the pontoDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/pontos")
    public ResponseEntity<PontoDTO> updatePonto(@RequestBody PontoDTO pontoDTO) throws URISyntaxException {
        log.debug("REST request to update Ponto : {}", pontoDTO);
        if (pontoDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        PontoDTO result = pontoService.save(pontoDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, pontoDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /pontos} : get all the pontos.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of pontos in body.
     */
    @GetMapping("/pontos")
    public ResponseEntity<List<PontoDTO>> getAllPontos(PontoCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get Pontos by criteria: {}", criteria);
        Page<PontoDTO> page = pontoQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /pontos/count} : count all the pontos.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/pontos/count")
    public ResponseEntity<Long> countPontos(PontoCriteria criteria) {
        log.debug("REST request to count Pontos by criteria: {}", criteria);
        return ResponseEntity.ok().body(pontoQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /pontos/:id} : get the "id" ponto.
     *
     * @param id the id of the pontoDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the pontoDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/pontos/{id}")
    public ResponseEntity<PontoDTO> getPonto(@PathVariable Long id) {
        log.debug("REST request to get Ponto : {}", id);
        Optional<PontoDTO> pontoDTO = pontoService.findOne(id);
        return ResponseUtil.wrapOrNotFound(pontoDTO);
    }

    /**
     * {@code DELETE  /pontos/:id} : delete the "id" ponto.
     *
     * @param id the id of the pontoDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/pontos/{id}")
    public ResponseEntity<Void> deletePonto(@PathVariable Long id) {
        log.debug("REST request to delete Ponto : {}", id);
        pontoService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
