package br.com.rotaurbana.web.rest;

import br.com.rotaurbana.service.LinhaService;
import br.com.rotaurbana.web.rest.errors.BadRequestAlertException;
import br.com.rotaurbana.service.dto.LinhaDTO;
import br.com.rotaurbana.service.dto.LinhaCriteria;
import br.com.rotaurbana.service.LinhaQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link br.com.rotaurbana.domain.Linha}.
 */
@RestController
@RequestMapping("/api")
public class LinhaResource {

    private final Logger log = LoggerFactory.getLogger(LinhaResource.class);

    private static final String ENTITY_NAME = "linha";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final LinhaService linhaService;

    private final LinhaQueryService linhaQueryService;

    public LinhaResource(LinhaService linhaService, LinhaQueryService linhaQueryService) {
        this.linhaService = linhaService;
        this.linhaQueryService = linhaQueryService;
    }

    /**
     * {@code POST  /linhas} : Create a new linha.
     *
     * @param linhaDTO the linhaDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new linhaDTO, or with status {@code 400 (Bad Request)} if the linha has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/linhas")
    public ResponseEntity<LinhaDTO> createLinha(@Valid @RequestBody LinhaDTO linhaDTO) throws URISyntaxException {
        log.debug("REST request to save Linha : {}", linhaDTO);
        if (linhaDTO.getId() != null) {
            throw new BadRequestAlertException("A new linha cannot already have an ID", ENTITY_NAME, "idexists");
        }
        LinhaDTO result = linhaService.save(linhaDTO);
        return ResponseEntity.created(new URI("/api/linhas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /linhas} : Updates an existing linha.
     *
     * @param linhaDTO the linhaDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated linhaDTO,
     * or with status {@code 400 (Bad Request)} if the linhaDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the linhaDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/linhas")
    public ResponseEntity<LinhaDTO> updateLinha(@Valid @RequestBody LinhaDTO linhaDTO) throws URISyntaxException {
        log.debug("REST request to update Linha : {}", linhaDTO);
        if (linhaDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        LinhaDTO result = linhaService.save(linhaDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, linhaDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /linhas} : get all the linhas.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of linhas in body.
     */
    @GetMapping("/linhas")
    public ResponseEntity<List<LinhaDTO>> getAllLinhas(LinhaCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get Linhas by criteria: {}", criteria);
        Page<LinhaDTO> page = linhaQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /linhas/count} : count all the linhas.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/linhas/count")
    public ResponseEntity<Long> countLinhas(LinhaCriteria criteria) {
        log.debug("REST request to count Linhas by criteria: {}", criteria);
        return ResponseEntity.ok().body(linhaQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /linhas/:id} : get the "id" linha.
     *
     * @param id the id of the linhaDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the linhaDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/linhas/{id}")
    public ResponseEntity<LinhaDTO> getLinha(@PathVariable Long id) {
        log.debug("REST request to get Linha : {}", id);
        Optional<LinhaDTO> linhaDTO = linhaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(linhaDTO);
    }

    /**
     * {@code DELETE  /linhas/:id} : delete the "id" linha.
     *
     * @param id the id of the linhaDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/linhas/{id}")
    public ResponseEntity<Void> deleteLinha(@PathVariable Long id) {
        log.debug("REST request to delete Linha : {}", id);
        linhaService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
