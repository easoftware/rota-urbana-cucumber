package br.com.rotaurbana.web.rest;

import br.com.rotaurbana.service.PontoPesquisaService;
import br.com.rotaurbana.web.rest.errors.BadRequestAlertException;
import br.com.rotaurbana.service.dto.PontoPesquisaDTO;
import br.com.rotaurbana.service.dto.PontoPesquisaCriteria;
import br.com.rotaurbana.service.PontoPesquisaQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link br.com.rotaurbana.domain.PontoPesquisa}.
 */
@RestController
@RequestMapping("/api")
public class PontoPesquisaResource {

    private final Logger log = LoggerFactory.getLogger(PontoPesquisaResource.class);

    private static final String ENTITY_NAME = "pontoPesquisa";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PontoPesquisaService pontoPesquisaService;

    private final PontoPesquisaQueryService pontoPesquisaQueryService;

    public PontoPesquisaResource(PontoPesquisaService pontoPesquisaService, PontoPesquisaQueryService pontoPesquisaQueryService) {
        this.pontoPesquisaService = pontoPesquisaService;
        this.pontoPesquisaQueryService = pontoPesquisaQueryService;
    }

    /**
     * {@code POST  /ponto-pesquisas} : Create a new pontoPesquisa.
     *
     * @param pontoPesquisaDTO the pontoPesquisaDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new pontoPesquisaDTO, or with status {@code 400 (Bad Request)} if the pontoPesquisa has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/ponto-pesquisas")
    public ResponseEntity<PontoPesquisaDTO> createPontoPesquisa(@RequestBody PontoPesquisaDTO pontoPesquisaDTO) throws URISyntaxException {
        log.debug("REST request to save PontoPesquisa : {}", pontoPesquisaDTO);
        if (pontoPesquisaDTO.getId() != null) {
            throw new BadRequestAlertException("A new pontoPesquisa cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PontoPesquisaDTO result = pontoPesquisaService.save(pontoPesquisaDTO);
        return ResponseEntity.created(new URI("/api/ponto-pesquisas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /ponto-pesquisas} : Updates an existing pontoPesquisa.
     *
     * @param pontoPesquisaDTO the pontoPesquisaDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated pontoPesquisaDTO,
     * or with status {@code 400 (Bad Request)} if the pontoPesquisaDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the pontoPesquisaDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/ponto-pesquisas")
    public ResponseEntity<PontoPesquisaDTO> updatePontoPesquisa(@RequestBody PontoPesquisaDTO pontoPesquisaDTO) throws URISyntaxException {
        log.debug("REST request to update PontoPesquisa : {}", pontoPesquisaDTO);
        if (pontoPesquisaDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        PontoPesquisaDTO result = pontoPesquisaService.save(pontoPesquisaDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, pontoPesquisaDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /ponto-pesquisas} : get all the pontoPesquisas.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of pontoPesquisas in body.
     */
    @GetMapping("/ponto-pesquisas")
    public ResponseEntity<List<PontoPesquisaDTO>> getAllPontoPesquisas(PontoPesquisaCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get PontoPesquisas by criteria: {}", criteria);
        Page<PontoPesquisaDTO> page = pontoPesquisaQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /ponto-pesquisas/count} : count all the pontoPesquisas.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/ponto-pesquisas/count")
    public ResponseEntity<Long> countPontoPesquisas(PontoPesquisaCriteria criteria) {
        log.debug("REST request to count PontoPesquisas by criteria: {}", criteria);
        return ResponseEntity.ok().body(pontoPesquisaQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /ponto-pesquisas/:id} : get the "id" pontoPesquisa.
     *
     * @param id the id of the pontoPesquisaDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the pontoPesquisaDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/ponto-pesquisas/{id}")
    public ResponseEntity<PontoPesquisaDTO> getPontoPesquisa(@PathVariable Long id) {
        log.debug("REST request to get PontoPesquisa : {}", id);
        Optional<PontoPesquisaDTO> pontoPesquisaDTO = pontoPesquisaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(pontoPesquisaDTO);
    }

    /**
     * {@code DELETE  /ponto-pesquisas/:id} : delete the "id" pontoPesquisa.
     *
     * @param id the id of the pontoPesquisaDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/ponto-pesquisas/{id}")
    public ResponseEntity<Void> deletePontoPesquisa(@PathVariable Long id) {
        log.debug("REST request to delete PontoPesquisa : {}", id);
        pontoPesquisaService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
