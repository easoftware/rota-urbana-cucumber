package br.com.rotaurbana.web.rest;

import br.com.rotaurbana.service.ParadaService;
import br.com.rotaurbana.web.rest.errors.BadRequestAlertException;
import br.com.rotaurbana.service.dto.ParadaDTO;
import br.com.rotaurbana.service.dto.ParadaCriteria;
import br.com.rotaurbana.service.ParadaQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link br.com.rotaurbana.domain.Parada}.
 */
@RestController
@RequestMapping("/api")
public class ParadaResource {

    private final Logger log = LoggerFactory.getLogger(ParadaResource.class);

    private static final String ENTITY_NAME = "parada";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ParadaService paradaService;

    private final ParadaQueryService paradaQueryService;

    public ParadaResource(ParadaService paradaService, ParadaQueryService paradaQueryService) {
        this.paradaService = paradaService;
        this.paradaQueryService = paradaQueryService;
    }

    /**
     * {@code POST  /paradas} : Create a new parada.
     *
     * @param paradaDTO the paradaDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new paradaDTO, or with status {@code 400 (Bad Request)} if the parada has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/paradas")
    public ResponseEntity<ParadaDTO> createParada(@RequestBody ParadaDTO paradaDTO) throws URISyntaxException {
        log.debug("REST request to save Parada : {}", paradaDTO);
        if (paradaDTO.getId() != null) {
            throw new BadRequestAlertException("A new parada cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ParadaDTO result = paradaService.save(paradaDTO);
        return ResponseEntity.created(new URI("/api/paradas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /paradas} : Updates an existing parada.
     *
     * @param paradaDTO the paradaDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated paradaDTO,
     * or with status {@code 400 (Bad Request)} if the paradaDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the paradaDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/paradas")
    public ResponseEntity<ParadaDTO> updateParada(@RequestBody ParadaDTO paradaDTO) throws URISyntaxException {
        log.debug("REST request to update Parada : {}", paradaDTO);
        if (paradaDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ParadaDTO result = paradaService.save(paradaDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, paradaDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /paradas} : get all the paradas.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of paradas in body.
     */
    @GetMapping("/paradas")
    public ResponseEntity<List<ParadaDTO>> getAllParadas(ParadaCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get Paradas by criteria: {}", criteria);
        Page<ParadaDTO> page = paradaQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /paradas/count} : count all the paradas.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/paradas/count")
    public ResponseEntity<Long> countParadas(ParadaCriteria criteria) {
        log.debug("REST request to count Paradas by criteria: {}", criteria);
        return ResponseEntity.ok().body(paradaQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /paradas/:id} : get the "id" parada.
     *
     * @param id the id of the paradaDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the paradaDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/paradas/{id}")
    public ResponseEntity<ParadaDTO> getParada(@PathVariable Long id) {
        log.debug("REST request to get Parada : {}", id);
        Optional<ParadaDTO> paradaDTO = paradaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(paradaDTO);
    }

    /**
     * {@code DELETE  /paradas/:id} : delete the "id" parada.
     *
     * @param id the id of the paradaDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/paradas/{id}")
    public ResponseEntity<Void> deleteParada(@PathVariable Long id) {
        log.debug("REST request to delete Parada : {}", id);
        paradaService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
