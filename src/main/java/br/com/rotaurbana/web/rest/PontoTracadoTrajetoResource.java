package br.com.rotaurbana.web.rest;

import br.com.rotaurbana.service.PontoTracadoTrajetoService;
import br.com.rotaurbana.web.rest.errors.BadRequestAlertException;
import br.com.rotaurbana.service.dto.PontoTracadoTrajetoDTO;
import br.com.rotaurbana.service.dto.PontoTracadoTrajetoCriteria;
import br.com.rotaurbana.service.PontoTracadoTrajetoQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link br.com.rotaurbana.domain.PontoTracadoTrajeto}.
 */
@RestController
@RequestMapping("/api")
public class PontoTracadoTrajetoResource {

    private final Logger log = LoggerFactory.getLogger(PontoTracadoTrajetoResource.class);

    private static final String ENTITY_NAME = "pontoTracadoTrajeto";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PontoTracadoTrajetoService pontoTracadoTrajetoService;

    private final PontoTracadoTrajetoQueryService pontoTracadoTrajetoQueryService;

    public PontoTracadoTrajetoResource(PontoTracadoTrajetoService pontoTracadoTrajetoService, PontoTracadoTrajetoQueryService pontoTracadoTrajetoQueryService) {
        this.pontoTracadoTrajetoService = pontoTracadoTrajetoService;
        this.pontoTracadoTrajetoQueryService = pontoTracadoTrajetoQueryService;
    }

    /**
     * {@code POST  /ponto-tracado-trajetos} : Create a new pontoTracadoTrajeto.
     *
     * @param pontoTracadoTrajetoDTO the pontoTracadoTrajetoDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new pontoTracadoTrajetoDTO, or with status {@code 400 (Bad Request)} if the pontoTracadoTrajeto has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/ponto-tracado-trajetos")
    public ResponseEntity<PontoTracadoTrajetoDTO> createPontoTracadoTrajeto(@RequestBody PontoTracadoTrajetoDTO pontoTracadoTrajetoDTO) throws URISyntaxException {
        log.debug("REST request to save PontoTracadoTrajeto : {}", pontoTracadoTrajetoDTO);
        if (pontoTracadoTrajetoDTO.getId() != null) {
            throw new BadRequestAlertException("A new pontoTracadoTrajeto cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PontoTracadoTrajetoDTO result = pontoTracadoTrajetoService.save(pontoTracadoTrajetoDTO);
        return ResponseEntity.created(new URI("/api/ponto-tracado-trajetos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /ponto-tracado-trajetos} : Updates an existing pontoTracadoTrajeto.
     *
     * @param pontoTracadoTrajetoDTO the pontoTracadoTrajetoDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated pontoTracadoTrajetoDTO,
     * or with status {@code 400 (Bad Request)} if the pontoTracadoTrajetoDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the pontoTracadoTrajetoDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/ponto-tracado-trajetos")
    public ResponseEntity<PontoTracadoTrajetoDTO> updatePontoTracadoTrajeto(@RequestBody PontoTracadoTrajetoDTO pontoTracadoTrajetoDTO) throws URISyntaxException {
        log.debug("REST request to update PontoTracadoTrajeto : {}", pontoTracadoTrajetoDTO);
        if (pontoTracadoTrajetoDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        PontoTracadoTrajetoDTO result = pontoTracadoTrajetoService.save(pontoTracadoTrajetoDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, pontoTracadoTrajetoDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /ponto-tracado-trajetos} : get all the pontoTracadoTrajetos.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of pontoTracadoTrajetos in body.
     */
    @GetMapping("/ponto-tracado-trajetos")
    public ResponseEntity<List<PontoTracadoTrajetoDTO>> getAllPontoTracadoTrajetos(PontoTracadoTrajetoCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get PontoTracadoTrajetos by criteria: {}", criteria);
        Page<PontoTracadoTrajetoDTO> page = pontoTracadoTrajetoQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /ponto-tracado-trajetos/count} : count all the pontoTracadoTrajetos.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/ponto-tracado-trajetos/count")
    public ResponseEntity<Long> countPontoTracadoTrajetos(PontoTracadoTrajetoCriteria criteria) {
        log.debug("REST request to count PontoTracadoTrajetos by criteria: {}", criteria);
        return ResponseEntity.ok().body(pontoTracadoTrajetoQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /ponto-tracado-trajetos/:id} : get the "id" pontoTracadoTrajeto.
     *
     * @param id the id of the pontoTracadoTrajetoDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the pontoTracadoTrajetoDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/ponto-tracado-trajetos/{id}")
    public ResponseEntity<PontoTracadoTrajetoDTO> getPontoTracadoTrajeto(@PathVariable Long id) {
        log.debug("REST request to get PontoTracadoTrajeto : {}", id);
        Optional<PontoTracadoTrajetoDTO> pontoTracadoTrajetoDTO = pontoTracadoTrajetoService.findOne(id);
        return ResponseUtil.wrapOrNotFound(pontoTracadoTrajetoDTO);
    }

    /**
     * {@code DELETE  /ponto-tracado-trajetos/:id} : delete the "id" pontoTracadoTrajeto.
     *
     * @param id the id of the pontoTracadoTrajetoDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/ponto-tracado-trajetos/{id}")
    public ResponseEntity<Void> deletePontoTracadoTrajeto(@PathVariable Long id) {
        log.debug("REST request to delete PontoTracadoTrajeto : {}", id);
        pontoTracadoTrajetoService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
