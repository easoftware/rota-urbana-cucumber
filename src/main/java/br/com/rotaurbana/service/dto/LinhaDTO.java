package br.com.rotaurbana.service.dto;
import java.time.ZonedDateTime;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import br.com.rotaurbana.domain.enumeration.TipoDeRota;

/**
 * A DTO for the {@link br.com.rotaurbana.domain.Linha} entity.
 */
public class LinhaDTO implements Serializable {

    private Long id;

    private String nome;

    private String codigo;

    private String url;

    private Boolean completa;

    private ZonedDateTime lastUpdate;

    private String comentario;

    @Size(max = 2000)
    private String itinerarioTotalEncoding;

    private Boolean emAvaliacao;

    private Boolean faltaCadastrarPontosPesquisa;

    private Boolean semob;

    private TipoDeRota tipoDeRota;


    private Long cidadeId;

    private String cidadeNome;

    private Long rootId;

    private Set<ParadaDTO> paradas = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Boolean isCompleta() {
        return completa;
    }

    public void setCompleta(Boolean completa) {
        this.completa = completa;
    }

    public ZonedDateTime getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(ZonedDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public String getItinerarioTotalEncoding() {
        return itinerarioTotalEncoding;
    }

    public void setItinerarioTotalEncoding(String itinerarioTotalEncoding) {
        this.itinerarioTotalEncoding = itinerarioTotalEncoding;
    }

    public Boolean isEmAvaliacao() {
        return emAvaliacao;
    }

    public void setEmAvaliacao(Boolean emAvaliacao) {
        this.emAvaliacao = emAvaliacao;
    }

    public Boolean isFaltaCadastrarPontosPesquisa() {
        return faltaCadastrarPontosPesquisa;
    }

    public void setFaltaCadastrarPontosPesquisa(Boolean faltaCadastrarPontosPesquisa) {
        this.faltaCadastrarPontosPesquisa = faltaCadastrarPontosPesquisa;
    }

    public Boolean isSemob() {
        return semob;
    }

    public void setSemob(Boolean semob) {
        this.semob = semob;
    }

    public TipoDeRota getTipoDeRota() {
        return tipoDeRota;
    }

    public void setTipoDeRota(TipoDeRota tipoDeRota) {
        this.tipoDeRota = tipoDeRota;
    }

    public Long getCidadeId() {
        return cidadeId;
    }

    public void setCidadeId(Long cidadeId) {
        this.cidadeId = cidadeId;
    }

    public String getCidadeNome() {
        return cidadeNome;
    }

    public void setCidadeNome(String cidadeNome) {
        this.cidadeNome = cidadeNome;
    }

    public Long getRootId() {
        return rootId;
    }

    public void setRootId(Long linhaId) {
        this.rootId = linhaId;
    }

    public Set<ParadaDTO> getParadas() {
        return paradas;
    }

    public void setParadas(Set<ParadaDTO> paradas) {
        this.paradas = paradas;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        LinhaDTO linhaDTO = (LinhaDTO) o;
        if (linhaDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), linhaDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "LinhaDTO{" +
            "id=" + getId() +
            ", nome='" + getNome() + "'" +
            ", codigo='" + getCodigo() + "'" +
            ", url='" + getUrl() + "'" +
            ", completa='" + isCompleta() + "'" +
            ", lastUpdate='" + getLastUpdate() + "'" +
            ", comentario='" + getComentario() + "'" +
            ", itinerarioTotalEncoding='" + getItinerarioTotalEncoding() + "'" +
            ", emAvaliacao='" + isEmAvaliacao() + "'" +
            ", faltaCadastrarPontosPesquisa='" + isFaltaCadastrarPontosPesquisa() + "'" +
            ", semob='" + isSemob() + "'" +
            ", tipoDeRota='" + getTipoDeRota() + "'" +
            ", cidade=" + getCidadeId() +
            ", cidade='" + getCidadeNome() + "'" +
            ", root=" + getRootId() +
            "}";
    }
}
