package br.com.rotaurbana.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import br.com.rotaurbana.domain.enumeration.Tipo;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link br.com.rotaurbana.domain.PontoTracadoTrajeto} entity. This class is used
 * in {@link br.com.rotaurbana.web.rest.PontoTracadoTrajetoResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /ponto-tracado-trajetos?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class PontoTracadoTrajetoCriteria implements Serializable, Criteria {
    /**
     * Class for filtering Tipo
     */
    public static class TipoFilter extends Filter<Tipo> {

        public TipoFilter() {
        }

        public TipoFilter(TipoFilter filter) {
            super(filter);
        }

        @Override
        public TipoFilter copy() {
            return new TipoFilter(this);
        }

    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private DoubleFilter latitude;

    private DoubleFilter longitude;

    private IntegerFilter posicao;

    private TipoFilter tipo;

    private LongFilter linhaId;

    public PontoTracadoTrajetoCriteria(){
    }

    public PontoTracadoTrajetoCriteria(PontoTracadoTrajetoCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.latitude = other.latitude == null ? null : other.latitude.copy();
        this.longitude = other.longitude == null ? null : other.longitude.copy();
        this.posicao = other.posicao == null ? null : other.posicao.copy();
        this.tipo = other.tipo == null ? null : other.tipo.copy();
        this.linhaId = other.linhaId == null ? null : other.linhaId.copy();
    }

    @Override
    public PontoTracadoTrajetoCriteria copy() {
        return new PontoTracadoTrajetoCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public DoubleFilter getLatitude() {
        return latitude;
    }

    public void setLatitude(DoubleFilter latitude) {
        this.latitude = latitude;
    }

    public DoubleFilter getLongitude() {
        return longitude;
    }

    public void setLongitude(DoubleFilter longitude) {
        this.longitude = longitude;
    }

    public IntegerFilter getPosicao() {
        return posicao;
    }

    public void setPosicao(IntegerFilter posicao) {
        this.posicao = posicao;
    }

    public TipoFilter getTipo() {
        return tipo;
    }

    public void setTipo(TipoFilter tipo) {
        this.tipo = tipo;
    }

    public LongFilter getLinhaId() {
        return linhaId;
    }

    public void setLinhaId(LongFilter linhaId) {
        this.linhaId = linhaId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final PontoTracadoTrajetoCriteria that = (PontoTracadoTrajetoCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(latitude, that.latitude) &&
            Objects.equals(longitude, that.longitude) &&
            Objects.equals(posicao, that.posicao) &&
            Objects.equals(tipo, that.tipo) &&
            Objects.equals(linhaId, that.linhaId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        latitude,
        longitude,
        posicao,
        tipo,
        linhaId
        );
    }

    @Override
    public String toString() {
        return "PontoTracadoTrajetoCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (latitude != null ? "latitude=" + latitude + ", " : "") +
                (longitude != null ? "longitude=" + longitude + ", " : "") +
                (posicao != null ? "posicao=" + posicao + ", " : "") +
                (tipo != null ? "tipo=" + tipo + ", " : "") +
                (linhaId != null ? "linhaId=" + linhaId + ", " : "") +
            "}";
    }

}
