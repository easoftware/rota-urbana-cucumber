package br.com.rotaurbana.service.dto;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link br.com.rotaurbana.domain.Cidade} entity.
 */
public class CidadeDTO implements Serializable {

    private Long id;

    private String nome;

    private String latitude;

    private String longitude;

    private Double latitudeDouble;

    private Double longitudeDouble;

    private String sameAs;


    private Long belongsToId;

    private String belongsToNome;

    private Long estadoId;

    private String estadoNome;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public Double getLatitudeDouble() {
        return latitudeDouble;
    }

    public void setLatitudeDouble(Double latitudeDouble) {
        this.latitudeDouble = latitudeDouble;
    }

    public Double getLongitudeDouble() {
        return longitudeDouble;
    }

    public void setLongitudeDouble(Double longitudeDouble) {
        this.longitudeDouble = longitudeDouble;
    }

    public String getSameAs() {
        return sameAs;
    }

    public void setSameAs(String sameAs) {
        this.sameAs = sameAs;
    }

    public Long getBelongsToId() {
        return belongsToId;
    }

    public void setBelongsToId(Long cidadeId) {
        this.belongsToId = cidadeId;
    }

    public String getBelongsToNome() {
        return belongsToNome;
    }

    public void setBelongsToNome(String cidadeNome) {
        this.belongsToNome = cidadeNome;
    }

    public Long getEstadoId() {
        return estadoId;
    }

    public void setEstadoId(Long estadoId) {
        this.estadoId = estadoId;
    }

    public String getEstadoNome() {
        return estadoNome;
    }

    public void setEstadoNome(String estadoNome) {
        this.estadoNome = estadoNome;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CidadeDTO cidadeDTO = (CidadeDTO) o;
        if (cidadeDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), cidadeDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CidadeDTO{" +
            "id=" + getId() +
            ", nome='" + getNome() + "'" +
            ", latitude='" + getLatitude() + "'" +
            ", longitude='" + getLongitude() + "'" +
            ", latitudeDouble=" + getLatitudeDouble() +
            ", longitudeDouble=" + getLongitudeDouble() +
            ", sameAs='" + getSameAs() + "'" +
            ", belongsTo=" + getBelongsToId() +
            ", belongsTo='" + getBelongsToNome() + "'" +
            ", estado=" + getEstadoId() +
            ", estado='" + getEstadoNome() + "'" +
            "}";
    }
}
