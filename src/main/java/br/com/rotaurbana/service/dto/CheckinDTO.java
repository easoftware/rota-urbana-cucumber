package br.com.rotaurbana.service.dto;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link br.com.rotaurbana.domain.Checkin} entity.
 */
public class CheckinDTO implements Serializable {

    private Long id;

    private Double latitude;

    private Double longitude;


    private Long linhaId;

    private String linhaNome;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Long getLinhaId() {
        return linhaId;
    }

    public void setLinhaId(Long linhaId) {
        this.linhaId = linhaId;
    }

    public String getLinhaNome() {
        return linhaNome;
    }

    public void setLinhaNome(String linhaNome) {
        this.linhaNome = linhaNome;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CheckinDTO checkinDTO = (CheckinDTO) o;
        if (checkinDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), checkinDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CheckinDTO{" +
            "id=" + getId() +
            ", latitude=" + getLatitude() +
            ", longitude=" + getLongitude() +
            ", linha=" + getLinhaId() +
            ", linha='" + getLinhaNome() + "'" +
            "}";
    }
}
