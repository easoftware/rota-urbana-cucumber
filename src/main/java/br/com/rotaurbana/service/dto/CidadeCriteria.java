package br.com.rotaurbana.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link br.com.rotaurbana.domain.Cidade} entity. This class is used
 * in {@link br.com.rotaurbana.web.rest.CidadeResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /cidades?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class CidadeCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter nome;

    private StringFilter latitude;

    private StringFilter longitude;

    private DoubleFilter latitudeDouble;

    private DoubleFilter longitudeDouble;

    private StringFilter sameAs;

    private LongFilter belongsToId;

    private LongFilter estadoId;

    public CidadeCriteria(){
    }

    public CidadeCriteria(CidadeCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.nome = other.nome == null ? null : other.nome.copy();
        this.latitude = other.latitude == null ? null : other.latitude.copy();
        this.longitude = other.longitude == null ? null : other.longitude.copy();
        this.latitudeDouble = other.latitudeDouble == null ? null : other.latitudeDouble.copy();
        this.longitudeDouble = other.longitudeDouble == null ? null : other.longitudeDouble.copy();
        this.sameAs = other.sameAs == null ? null : other.sameAs.copy();
        this.belongsToId = other.belongsToId == null ? null : other.belongsToId.copy();
        this.estadoId = other.estadoId == null ? null : other.estadoId.copy();
    }

    @Override
    public CidadeCriteria copy() {
        return new CidadeCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getNome() {
        return nome;
    }

    public void setNome(StringFilter nome) {
        this.nome = nome;
    }

    public StringFilter getLatitude() {
        return latitude;
    }

    public void setLatitude(StringFilter latitude) {
        this.latitude = latitude;
    }

    public StringFilter getLongitude() {
        return longitude;
    }

    public void setLongitude(StringFilter longitude) {
        this.longitude = longitude;
    }

    public DoubleFilter getLatitudeDouble() {
        return latitudeDouble;
    }

    public void setLatitudeDouble(DoubleFilter latitudeDouble) {
        this.latitudeDouble = latitudeDouble;
    }

    public DoubleFilter getLongitudeDouble() {
        return longitudeDouble;
    }

    public void setLongitudeDouble(DoubleFilter longitudeDouble) {
        this.longitudeDouble = longitudeDouble;
    }

    public StringFilter getSameAs() {
        return sameAs;
    }

    public void setSameAs(StringFilter sameAs) {
        this.sameAs = sameAs;
    }

    public LongFilter getBelongsToId() {
        return belongsToId;
    }

    public void setBelongsToId(LongFilter belongsToId) {
        this.belongsToId = belongsToId;
    }

    public LongFilter getEstadoId() {
        return estadoId;
    }

    public void setEstadoId(LongFilter estadoId) {
        this.estadoId = estadoId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final CidadeCriteria that = (CidadeCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(nome, that.nome) &&
            Objects.equals(latitude, that.latitude) &&
            Objects.equals(longitude, that.longitude) &&
            Objects.equals(latitudeDouble, that.latitudeDouble) &&
            Objects.equals(longitudeDouble, that.longitudeDouble) &&
            Objects.equals(sameAs, that.sameAs) &&
            Objects.equals(belongsToId, that.belongsToId) &&
            Objects.equals(estadoId, that.estadoId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        nome,
        latitude,
        longitude,
        latitudeDouble,
        longitudeDouble,
        sameAs,
        belongsToId,
        estadoId
        );
    }

    @Override
    public String toString() {
        return "CidadeCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (nome != null ? "nome=" + nome + ", " : "") +
                (latitude != null ? "latitude=" + latitude + ", " : "") +
                (longitude != null ? "longitude=" + longitude + ", " : "") +
                (latitudeDouble != null ? "latitudeDouble=" + latitudeDouble + ", " : "") +
                (longitudeDouble != null ? "longitudeDouble=" + longitudeDouble + ", " : "") +
                (sameAs != null ? "sameAs=" + sameAs + ", " : "") +
                (belongsToId != null ? "belongsToId=" + belongsToId + ", " : "") +
                (estadoId != null ? "estadoId=" + estadoId + ", " : "") +
            "}";
    }

}
