package br.com.rotaurbana.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import br.com.rotaurbana.domain.enumeration.TipoDeRota;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.ZonedDateTimeFilter;

/**
 * Criteria class for the {@link br.com.rotaurbana.domain.Linha} entity. This class is used
 * in {@link br.com.rotaurbana.web.rest.LinhaResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /linhas?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class LinhaCriteria implements Serializable, Criteria {
    /**
     * Class for filtering TipoDeRota
     */
    public static class TipoDeRotaFilter extends Filter<TipoDeRota> {

        public TipoDeRotaFilter() {
        }

        public TipoDeRotaFilter(TipoDeRotaFilter filter) {
            super(filter);
        }

        @Override
        public TipoDeRotaFilter copy() {
            return new TipoDeRotaFilter(this);
        }

    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter nome;

    private StringFilter codigo;

    private StringFilter url;

    private BooleanFilter completa;

    private ZonedDateTimeFilter lastUpdate;

    private StringFilter comentario;

    private StringFilter itinerarioTotalEncoding;

    private BooleanFilter emAvaliacao;

    private BooleanFilter faltaCadastrarPontosPesquisa;

    private BooleanFilter semob;

    private TipoDeRotaFilter tipoDeRota;

    private LongFilter itinerarioId;

    private LongFilter pontosPesquisaId;

    private LongFilter cidadeId;

    private LongFilter rootId;

    private LongFilter paradasId;

    private LongFilter childrenId;

    public LinhaCriteria(){
    }

    public LinhaCriteria(LinhaCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.nome = other.nome == null ? null : other.nome.copy();
        this.codigo = other.codigo == null ? null : other.codigo.copy();
        this.url = other.url == null ? null : other.url.copy();
        this.completa = other.completa == null ? null : other.completa.copy();
        this.lastUpdate = other.lastUpdate == null ? null : other.lastUpdate.copy();
        this.comentario = other.comentario == null ? null : other.comentario.copy();
        this.itinerarioTotalEncoding = other.itinerarioTotalEncoding == null ? null : other.itinerarioTotalEncoding.copy();
        this.emAvaliacao = other.emAvaliacao == null ? null : other.emAvaliacao.copy();
        this.faltaCadastrarPontosPesquisa = other.faltaCadastrarPontosPesquisa == null ? null : other.faltaCadastrarPontosPesquisa.copy();
        this.semob = other.semob == null ? null : other.semob.copy();
        this.tipoDeRota = other.tipoDeRota == null ? null : other.tipoDeRota.copy();
        this.itinerarioId = other.itinerarioId == null ? null : other.itinerarioId.copy();
        this.pontosPesquisaId = other.pontosPesquisaId == null ? null : other.pontosPesquisaId.copy();
        this.cidadeId = other.cidadeId == null ? null : other.cidadeId.copy();
        this.rootId = other.rootId == null ? null : other.rootId.copy();
        this.paradasId = other.paradasId == null ? null : other.paradasId.copy();
        this.childrenId = other.childrenId == null ? null : other.childrenId.copy();
    }

    @Override
    public LinhaCriteria copy() {
        return new LinhaCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getNome() {
        return nome;
    }

    public void setNome(StringFilter nome) {
        this.nome = nome;
    }

    public StringFilter getCodigo() {
        return codigo;
    }

    public void setCodigo(StringFilter codigo) {
        this.codigo = codigo;
    }

    public StringFilter getUrl() {
        return url;
    }

    public void setUrl(StringFilter url) {
        this.url = url;
    }

    public BooleanFilter getCompleta() {
        return completa;
    }

    public void setCompleta(BooleanFilter completa) {
        this.completa = completa;
    }

    public ZonedDateTimeFilter getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(ZonedDateTimeFilter lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public StringFilter getComentario() {
        return comentario;
    }

    public void setComentario(StringFilter comentario) {
        this.comentario = comentario;
    }

    public StringFilter getItinerarioTotalEncoding() {
        return itinerarioTotalEncoding;
    }

    public void setItinerarioTotalEncoding(StringFilter itinerarioTotalEncoding) {
        this.itinerarioTotalEncoding = itinerarioTotalEncoding;
    }

    public BooleanFilter getEmAvaliacao() {
        return emAvaliacao;
    }

    public void setEmAvaliacao(BooleanFilter emAvaliacao) {
        this.emAvaliacao = emAvaliacao;
    }

    public BooleanFilter getFaltaCadastrarPontosPesquisa() {
        return faltaCadastrarPontosPesquisa;
    }

    public void setFaltaCadastrarPontosPesquisa(BooleanFilter faltaCadastrarPontosPesquisa) {
        this.faltaCadastrarPontosPesquisa = faltaCadastrarPontosPesquisa;
    }

    public BooleanFilter getSemob() {
        return semob;
    }

    public void setSemob(BooleanFilter semob) {
        this.semob = semob;
    }

    public TipoDeRotaFilter getTipoDeRota() {
        return tipoDeRota;
    }

    public void setTipoDeRota(TipoDeRotaFilter tipoDeRota) {
        this.tipoDeRota = tipoDeRota;
    }

    public LongFilter getItinerarioId() {
        return itinerarioId;
    }

    public void setItinerarioId(LongFilter itinerarioId) {
        this.itinerarioId = itinerarioId;
    }

    public LongFilter getPontosPesquisaId() {
        return pontosPesquisaId;
    }

    public void setPontosPesquisaId(LongFilter pontosPesquisaId) {
        this.pontosPesquisaId = pontosPesquisaId;
    }

    public LongFilter getCidadeId() {
        return cidadeId;
    }

    public void setCidadeId(LongFilter cidadeId) {
        this.cidadeId = cidadeId;
    }

    public LongFilter getRootId() {
        return rootId;
    }

    public void setRootId(LongFilter rootId) {
        this.rootId = rootId;
    }

    public LongFilter getParadasId() {
        return paradasId;
    }

    public void setParadasId(LongFilter paradasId) {
        this.paradasId = paradasId;
    }

    public LongFilter getChildrenId() {
        return childrenId;
    }

    public void setChildrenId(LongFilter childrenId) {
        this.childrenId = childrenId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final LinhaCriteria that = (LinhaCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(nome, that.nome) &&
            Objects.equals(codigo, that.codigo) &&
            Objects.equals(url, that.url) &&
            Objects.equals(completa, that.completa) &&
            Objects.equals(lastUpdate, that.lastUpdate) &&
            Objects.equals(comentario, that.comentario) &&
            Objects.equals(itinerarioTotalEncoding, that.itinerarioTotalEncoding) &&
            Objects.equals(emAvaliacao, that.emAvaliacao) &&
            Objects.equals(faltaCadastrarPontosPesquisa, that.faltaCadastrarPontosPesquisa) &&
            Objects.equals(semob, that.semob) &&
            Objects.equals(tipoDeRota, that.tipoDeRota) &&
            Objects.equals(itinerarioId, that.itinerarioId) &&
            Objects.equals(pontosPesquisaId, that.pontosPesquisaId) &&
            Objects.equals(cidadeId, that.cidadeId) &&
            Objects.equals(rootId, that.rootId) &&
            Objects.equals(paradasId, that.paradasId) &&
            Objects.equals(childrenId, that.childrenId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        nome,
        codigo,
        url,
        completa,
        lastUpdate,
        comentario,
        itinerarioTotalEncoding,
        emAvaliacao,
        faltaCadastrarPontosPesquisa,
        semob,
        tipoDeRota,
        itinerarioId,
        pontosPesquisaId,
        cidadeId,
        rootId,
        paradasId,
        childrenId
        );
    }

    @Override
    public String toString() {
        return "LinhaCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (nome != null ? "nome=" + nome + ", " : "") +
                (codigo != null ? "codigo=" + codigo + ", " : "") +
                (url != null ? "url=" + url + ", " : "") +
                (completa != null ? "completa=" + completa + ", " : "") +
                (lastUpdate != null ? "lastUpdate=" + lastUpdate + ", " : "") +
                (comentario != null ? "comentario=" + comentario + ", " : "") +
                (itinerarioTotalEncoding != null ? "itinerarioTotalEncoding=" + itinerarioTotalEncoding + ", " : "") +
                (emAvaliacao != null ? "emAvaliacao=" + emAvaliacao + ", " : "") +
                (faltaCadastrarPontosPesquisa != null ? "faltaCadastrarPontosPesquisa=" + faltaCadastrarPontosPesquisa + ", " : "") +
                (semob != null ? "semob=" + semob + ", " : "") +
                (tipoDeRota != null ? "tipoDeRota=" + tipoDeRota + ", " : "") +
                (itinerarioId != null ? "itinerarioId=" + itinerarioId + ", " : "") +
                (pontosPesquisaId != null ? "pontosPesquisaId=" + pontosPesquisaId + ", " : "") +
                (cidadeId != null ? "cidadeId=" + cidadeId + ", " : "") +
                (rootId != null ? "rootId=" + rootId + ", " : "") +
                (paradasId != null ? "paradasId=" + paradasId + ", " : "") +
                (childrenId != null ? "childrenId=" + childrenId + ", " : "") +
            "}";
    }

}
