package br.com.rotaurbana.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link br.com.rotaurbana.domain.Parada} entity. This class is used
 * in {@link br.com.rotaurbana.web.rest.ParadaResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /paradas?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ParadaCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private DoubleFilter latitude;

    private DoubleFilter longitude;

    private IntegerFilter status;

    private StringFilter title;

    private StringFilter comments;

    private StringFilter tipoDeRotaDaParada;

    private LongFilter linhasId;

    public ParadaCriteria(){
    }

    public ParadaCriteria(ParadaCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.latitude = other.latitude == null ? null : other.latitude.copy();
        this.longitude = other.longitude == null ? null : other.longitude.copy();
        this.status = other.status == null ? null : other.status.copy();
        this.title = other.title == null ? null : other.title.copy();
        this.comments = other.comments == null ? null : other.comments.copy();
        this.tipoDeRotaDaParada = other.tipoDeRotaDaParada == null ? null : other.tipoDeRotaDaParada.copy();
        this.linhasId = other.linhasId == null ? null : other.linhasId.copy();
    }

    @Override
    public ParadaCriteria copy() {
        return new ParadaCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public DoubleFilter getLatitude() {
        return latitude;
    }

    public void setLatitude(DoubleFilter latitude) {
        this.latitude = latitude;
    }

    public DoubleFilter getLongitude() {
        return longitude;
    }

    public void setLongitude(DoubleFilter longitude) {
        this.longitude = longitude;
    }

    public IntegerFilter getStatus() {
        return status;
    }

    public void setStatus(IntegerFilter status) {
        this.status = status;
    }

    public StringFilter getTitle() {
        return title;
    }

    public void setTitle(StringFilter title) {
        this.title = title;
    }

    public StringFilter getComments() {
        return comments;
    }

    public void setComments(StringFilter comments) {
        this.comments = comments;
    }

    public StringFilter getTipoDeRotaDaParada() {
        return tipoDeRotaDaParada;
    }

    public void setTipoDeRotaDaParada(StringFilter tipoDeRotaDaParada) {
        this.tipoDeRotaDaParada = tipoDeRotaDaParada;
    }

    public LongFilter getLinhasId() {
        return linhasId;
    }

    public void setLinhasId(LongFilter linhasId) {
        this.linhasId = linhasId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ParadaCriteria that = (ParadaCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(latitude, that.latitude) &&
            Objects.equals(longitude, that.longitude) &&
            Objects.equals(status, that.status) &&
            Objects.equals(title, that.title) &&
            Objects.equals(comments, that.comments) &&
            Objects.equals(tipoDeRotaDaParada, that.tipoDeRotaDaParada) &&
            Objects.equals(linhasId, that.linhasId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        latitude,
        longitude,
        status,
        title,
        comments,
        tipoDeRotaDaParada,
        linhasId
        );
    }

    @Override
    public String toString() {
        return "ParadaCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (latitude != null ? "latitude=" + latitude + ", " : "") +
                (longitude != null ? "longitude=" + longitude + ", " : "") +
                (status != null ? "status=" + status + ", " : "") +
                (title != null ? "title=" + title + ", " : "") +
                (comments != null ? "comments=" + comments + ", " : "") +
                (tipoDeRotaDaParada != null ? "tipoDeRotaDaParada=" + tipoDeRotaDaParada + ", " : "") +
                (linhasId != null ? "linhasId=" + linhasId + ", " : "") +
            "}";
    }

}
