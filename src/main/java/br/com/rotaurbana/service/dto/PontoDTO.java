package br.com.rotaurbana.service.dto;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link br.com.rotaurbana.domain.Ponto} entity.
 */
public class PontoDTO implements Serializable {

    private Long id;

    private Double latitude;

    private Double longitude;

    private String codigoAndroid;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getCodigoAndroid() {
        return codigoAndroid;
    }

    public void setCodigoAndroid(String codigoAndroid) {
        this.codigoAndroid = codigoAndroid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PontoDTO pontoDTO = (PontoDTO) o;
        if (pontoDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), pontoDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PontoDTO{" +
            "id=" + getId() +
            ", latitude=" + getLatitude() +
            ", longitude=" + getLongitude() +
            ", codigoAndroid='" + getCodigoAndroid() + "'" +
            "}";
    }
}
