package br.com.rotaurbana.service.dto;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link br.com.rotaurbana.domain.Parada} entity.
 */
public class ParadaDTO implements Serializable {

    private Long id;

    private Double latitude;

    private Double longitude;

    private Integer status;

    private String title;

    private String comments;

    private String tipoDeRotaDaParada;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getTipoDeRotaDaParada() {
        return tipoDeRotaDaParada;
    }

    public void setTipoDeRotaDaParada(String tipoDeRotaDaParada) {
        this.tipoDeRotaDaParada = tipoDeRotaDaParada;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ParadaDTO paradaDTO = (ParadaDTO) o;
        if (paradaDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), paradaDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ParadaDTO{" +
            "id=" + getId() +
            ", latitude=" + getLatitude() +
            ", longitude=" + getLongitude() +
            ", status=" + getStatus() +
            ", title='" + getTitle() + "'" +
            ", comments='" + getComments() + "'" +
            ", tipoDeRotaDaParada='" + getTipoDeRotaDaParada() + "'" +
            "}";
    }
}
