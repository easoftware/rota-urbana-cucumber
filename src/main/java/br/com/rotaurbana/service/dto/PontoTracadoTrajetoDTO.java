package br.com.rotaurbana.service.dto;
import java.io.Serializable;
import java.util.Objects;
import br.com.rotaurbana.domain.enumeration.Tipo;

/**
 * A DTO for the {@link br.com.rotaurbana.domain.PontoTracadoTrajeto} entity.
 */
public class PontoTracadoTrajetoDTO implements Serializable {

    private Long id;

    private Double latitude;

    private Double longitude;

    private Integer posicao;

    private Tipo tipo;


    private Long linhaId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Integer getPosicao() {
        return posicao;
    }

    public void setPosicao(Integer posicao) {
        this.posicao = posicao;
    }

    public Tipo getTipo() {
        return tipo;
    }

    public void setTipo(Tipo tipo) {
        this.tipo = tipo;
    }

    public Long getLinhaId() {
        return linhaId;
    }

    public void setLinhaId(Long linhaId) {
        this.linhaId = linhaId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PontoTracadoTrajetoDTO pontoTracadoTrajetoDTO = (PontoTracadoTrajetoDTO) o;
        if (pontoTracadoTrajetoDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), pontoTracadoTrajetoDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PontoTracadoTrajetoDTO{" +
            "id=" + getId() +
            ", latitude=" + getLatitude() +
            ", longitude=" + getLongitude() +
            ", posicao=" + getPosicao() +
            ", tipo='" + getTipo() + "'" +
            ", linha=" + getLinhaId() +
            "}";
    }
}
