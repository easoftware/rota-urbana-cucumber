package br.com.rotaurbana.service;

import br.com.rotaurbana.service.dto.PontoTracadoTrajetoDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link br.com.rotaurbana.domain.PontoTracadoTrajeto}.
 */
public interface PontoTracadoTrajetoService {

    /**
     * Save a pontoTracadoTrajeto.
     *
     * @param pontoTracadoTrajetoDTO the entity to save.
     * @return the persisted entity.
     */
    PontoTracadoTrajetoDTO save(PontoTracadoTrajetoDTO pontoTracadoTrajetoDTO);

    /**
     * Get all the pontoTracadoTrajetos.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<PontoTracadoTrajetoDTO> findAll(Pageable pageable);


    /**
     * Get the "id" pontoTracadoTrajeto.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<PontoTracadoTrajetoDTO> findOne(Long id);

    /**
     * Delete the "id" pontoTracadoTrajeto.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
