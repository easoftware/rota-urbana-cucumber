package br.com.rotaurbana.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import br.com.rotaurbana.domain.PontoTracadoTrajeto;
import br.com.rotaurbana.domain.*; // for static metamodels
import br.com.rotaurbana.repository.PontoTracadoTrajetoRepository;
import br.com.rotaurbana.service.dto.PontoTracadoTrajetoCriteria;
import br.com.rotaurbana.service.dto.PontoTracadoTrajetoDTO;
import br.com.rotaurbana.service.mapper.PontoTracadoTrajetoMapper;

/**
 * Service for executing complex queries for {@link PontoTracadoTrajeto} entities in the database.
 * The main input is a {@link PontoTracadoTrajetoCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link PontoTracadoTrajetoDTO} or a {@link Page} of {@link PontoTracadoTrajetoDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class PontoTracadoTrajetoQueryService extends QueryService<PontoTracadoTrajeto> {

    private final Logger log = LoggerFactory.getLogger(PontoTracadoTrajetoQueryService.class);

    private final PontoTracadoTrajetoRepository pontoTracadoTrajetoRepository;

    private final PontoTracadoTrajetoMapper pontoTracadoTrajetoMapper;

    public PontoTracadoTrajetoQueryService(PontoTracadoTrajetoRepository pontoTracadoTrajetoRepository, PontoTracadoTrajetoMapper pontoTracadoTrajetoMapper) {
        this.pontoTracadoTrajetoRepository = pontoTracadoTrajetoRepository;
        this.pontoTracadoTrajetoMapper = pontoTracadoTrajetoMapper;
    }

    /**
     * Return a {@link List} of {@link PontoTracadoTrajetoDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<PontoTracadoTrajetoDTO> findByCriteria(PontoTracadoTrajetoCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<PontoTracadoTrajeto> specification = createSpecification(criteria);
        return pontoTracadoTrajetoMapper.toDto(pontoTracadoTrajetoRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link PontoTracadoTrajetoDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<PontoTracadoTrajetoDTO> findByCriteria(PontoTracadoTrajetoCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<PontoTracadoTrajeto> specification = createSpecification(criteria);
        return pontoTracadoTrajetoRepository.findAll(specification, page)
            .map(pontoTracadoTrajetoMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(PontoTracadoTrajetoCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<PontoTracadoTrajeto> specification = createSpecification(criteria);
        return pontoTracadoTrajetoRepository.count(specification);
    }

    /**
     * Function to convert PontoTracadoTrajetoCriteria to a {@link Specification}.
     */
    private Specification<PontoTracadoTrajeto> createSpecification(PontoTracadoTrajetoCriteria criteria) {
        Specification<PontoTracadoTrajeto> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), PontoTracadoTrajeto_.id));
            }
            if (criteria.getLatitude() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLatitude(), PontoTracadoTrajeto_.latitude));
            }
            if (criteria.getLongitude() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLongitude(), PontoTracadoTrajeto_.longitude));
            }
            if (criteria.getPosicao() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getPosicao(), PontoTracadoTrajeto_.posicao));
            }
            if (criteria.getTipo() != null) {
                specification = specification.and(buildSpecification(criteria.getTipo(), PontoTracadoTrajeto_.tipo));
            }
            if (criteria.getLinhaId() != null) {
                specification = specification.and(buildSpecification(criteria.getLinhaId(),
                    root -> root.join(PontoTracadoTrajeto_.linha, JoinType.LEFT).get(Linha_.id)));
            }
        }
        return specification;
    }
}
