package br.com.rotaurbana.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import br.com.rotaurbana.domain.Parada;
import br.com.rotaurbana.domain.*; // for static metamodels
import br.com.rotaurbana.repository.ParadaRepository;
import br.com.rotaurbana.service.dto.ParadaCriteria;
import br.com.rotaurbana.service.dto.ParadaDTO;
import br.com.rotaurbana.service.mapper.ParadaMapper;

/**
 * Service for executing complex queries for {@link Parada} entities in the database.
 * The main input is a {@link ParadaCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ParadaDTO} or a {@link Page} of {@link ParadaDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ParadaQueryService extends QueryService<Parada> {

    private final Logger log = LoggerFactory.getLogger(ParadaQueryService.class);

    private final ParadaRepository paradaRepository;

    private final ParadaMapper paradaMapper;

    public ParadaQueryService(ParadaRepository paradaRepository, ParadaMapper paradaMapper) {
        this.paradaRepository = paradaRepository;
        this.paradaMapper = paradaMapper;
    }

    /**
     * Return a {@link List} of {@link ParadaDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ParadaDTO> findByCriteria(ParadaCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Parada> specification = createSpecification(criteria);
        return paradaMapper.toDto(paradaRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ParadaDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ParadaDTO> findByCriteria(ParadaCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Parada> specification = createSpecification(criteria);
        return paradaRepository.findAll(specification, page)
            .map(paradaMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ParadaCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Parada> specification = createSpecification(criteria);
        return paradaRepository.count(specification);
    }

    /**
     * Function to convert ParadaCriteria to a {@link Specification}.
     */
    private Specification<Parada> createSpecification(ParadaCriteria criteria) {
        Specification<Parada> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Parada_.id));
            }
            if (criteria.getLatitude() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLatitude(), Parada_.latitude));
            }
            if (criteria.getLongitude() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLongitude(), Parada_.longitude));
            }
            if (criteria.getStatus() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getStatus(), Parada_.status));
            }
            if (criteria.getTitle() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTitle(), Parada_.title));
            }
            if (criteria.getComments() != null) {
                specification = specification.and(buildStringSpecification(criteria.getComments(), Parada_.comments));
            }
            if (criteria.getTipoDeRotaDaParada() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTipoDeRotaDaParada(), Parada_.tipoDeRotaDaParada));
            }
            if (criteria.getLinhasId() != null) {
                specification = specification.and(buildSpecification(criteria.getLinhasId(),
                    root -> root.join(Parada_.linhas, JoinType.LEFT).get(Linha_.id)));
            }
        }
        return specification;
    }
}
