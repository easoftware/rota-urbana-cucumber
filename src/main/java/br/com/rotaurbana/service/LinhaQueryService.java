package br.com.rotaurbana.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import br.com.rotaurbana.domain.Linha;
import br.com.rotaurbana.domain.*; // for static metamodels
import br.com.rotaurbana.repository.LinhaRepository;
import br.com.rotaurbana.service.dto.LinhaCriteria;
import br.com.rotaurbana.service.dto.LinhaDTO;
import br.com.rotaurbana.service.mapper.LinhaMapper;

/**
 * Service for executing complex queries for {@link Linha} entities in the database.
 * The main input is a {@link LinhaCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link LinhaDTO} or a {@link Page} of {@link LinhaDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class LinhaQueryService extends QueryService<Linha> {

    private final Logger log = LoggerFactory.getLogger(LinhaQueryService.class);

    private final LinhaRepository linhaRepository;

    private final LinhaMapper linhaMapper;

    public LinhaQueryService(LinhaRepository linhaRepository, LinhaMapper linhaMapper) {
        this.linhaRepository = linhaRepository;
        this.linhaMapper = linhaMapper;
    }

    /**
     * Return a {@link List} of {@link LinhaDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<LinhaDTO> findByCriteria(LinhaCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Linha> specification = createSpecification(criteria);
        return linhaMapper.toDto(linhaRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link LinhaDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<LinhaDTO> findByCriteria(LinhaCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Linha> specification = createSpecification(criteria);
        return linhaRepository.findAll(specification, page)
            .map(linhaMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(LinhaCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Linha> specification = createSpecification(criteria);
        return linhaRepository.count(specification);
    }

    /**
     * Function to convert LinhaCriteria to a {@link Specification}.
     */
    private Specification<Linha> createSpecification(LinhaCriteria criteria) {
        Specification<Linha> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Linha_.id));
            }
            if (criteria.getNome() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNome(), Linha_.nome));
            }
            if (criteria.getCodigo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCodigo(), Linha_.codigo));
            }
            if (criteria.getUrl() != null) {
                specification = specification.and(buildStringSpecification(criteria.getUrl(), Linha_.url));
            }
            if (criteria.getCompleta() != null) {
                specification = specification.and(buildSpecification(criteria.getCompleta(), Linha_.completa));
            }
            if (criteria.getLastUpdate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastUpdate(), Linha_.lastUpdate));
            }
            if (criteria.getComentario() != null) {
                specification = specification.and(buildStringSpecification(criteria.getComentario(), Linha_.comentario));
            }
            if (criteria.getItinerarioTotalEncoding() != null) {
                specification = specification.and(buildStringSpecification(criteria.getItinerarioTotalEncoding(), Linha_.itinerarioTotalEncoding));
            }
            if (criteria.getEmAvaliacao() != null) {
                specification = specification.and(buildSpecification(criteria.getEmAvaliacao(), Linha_.emAvaliacao));
            }
            if (criteria.getFaltaCadastrarPontosPesquisa() != null) {
                specification = specification.and(buildSpecification(criteria.getFaltaCadastrarPontosPesquisa(), Linha_.faltaCadastrarPontosPesquisa));
            }
            if (criteria.getSemob() != null) {
                specification = specification.and(buildSpecification(criteria.getSemob(), Linha_.semob));
            }
            if (criteria.getTipoDeRota() != null) {
                specification = specification.and(buildSpecification(criteria.getTipoDeRota(), Linha_.tipoDeRota));
            }
            if (criteria.getItinerarioId() != null) {
                specification = specification.and(buildSpecification(criteria.getItinerarioId(),
                    root -> root.join(Linha_.itinerarios, JoinType.LEFT).get(PontoTracadoTrajeto_.id)));
            }
            if (criteria.getPontosPesquisaId() != null) {
                specification = specification.and(buildSpecification(criteria.getPontosPesquisaId(),
                    root -> root.join(Linha_.pontosPesquisas, JoinType.LEFT).get(PontoPesquisa_.id)));
            }
            if (criteria.getCidadeId() != null) {
                specification = specification.and(buildSpecification(criteria.getCidadeId(),
                    root -> root.join(Linha_.cidade, JoinType.LEFT).get(Cidade_.id)));
            }
            if (criteria.getRootId() != null) {
                specification = specification.and(buildSpecification(criteria.getRootId(),
                    root -> root.join(Linha_.root, JoinType.LEFT).get(Linha_.id)));
            }
            if (criteria.getParadasId() != null) {
                specification = specification.and(buildSpecification(criteria.getParadasId(),
                    root -> root.join(Linha_.paradas, JoinType.LEFT).get(Parada_.id)));
            }
            if (criteria.getChildrenId() != null) {
                specification = specification.and(buildSpecification(criteria.getChildrenId(),
                    root -> root.join(Linha_.children, JoinType.LEFT).get(Linha_.id)));
            }
        }
        return specification;
    }
}
