package br.com.rotaurbana.service;

import br.com.rotaurbana.service.dto.PontoDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link br.com.rotaurbana.domain.Ponto}.
 */
public interface PontoService {

    /**
     * Save a ponto.
     *
     * @param pontoDTO the entity to save.
     * @return the persisted entity.
     */
    PontoDTO save(PontoDTO pontoDTO);

    /**
     * Get all the pontos.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<PontoDTO> findAll(Pageable pageable);


    /**
     * Get the "id" ponto.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<PontoDTO> findOne(Long id);

    /**
     * Delete the "id" ponto.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
