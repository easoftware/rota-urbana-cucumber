package br.com.rotaurbana.service;

import br.com.rotaurbana.service.dto.ParadaDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link br.com.rotaurbana.domain.Parada}.
 */
public interface ParadaService {

    /**
     * Save a parada.
     *
     * @param paradaDTO the entity to save.
     * @return the persisted entity.
     */
    ParadaDTO save(ParadaDTO paradaDTO);

    /**
     * Get all the paradas.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ParadaDTO> findAll(Pageable pageable);


    /**
     * Get the "id" parada.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ParadaDTO> findOne(Long id);

    /**
     * Delete the "id" parada.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
