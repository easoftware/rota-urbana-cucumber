package br.com.rotaurbana.service;

import br.com.rotaurbana.service.dto.LinhaDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link br.com.rotaurbana.domain.Linha}.
 */
public interface LinhaService {

    /**
     * Save a linha.
     *
     * @param linhaDTO the entity to save.
     * @return the persisted entity.
     */
    LinhaDTO save(LinhaDTO linhaDTO);

    /**
     * Get all the linhas.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<LinhaDTO> findAll(Pageable pageable);

    /**
     * Get all the linhas with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    Page<LinhaDTO> findAllWithEagerRelationships(Pageable pageable);
    
    /**
     * Get the "id" linha.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<LinhaDTO> findOne(Long id);

    /**
     * Delete the "id" linha.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
