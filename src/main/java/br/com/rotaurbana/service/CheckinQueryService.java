package br.com.rotaurbana.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import br.com.rotaurbana.domain.Checkin;
import br.com.rotaurbana.domain.*; // for static metamodels
import br.com.rotaurbana.repository.CheckinRepository;
import br.com.rotaurbana.service.dto.CheckinCriteria;
import br.com.rotaurbana.service.dto.CheckinDTO;
import br.com.rotaurbana.service.mapper.CheckinMapper;

/**
 * Service for executing complex queries for {@link Checkin} entities in the database.
 * The main input is a {@link CheckinCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link CheckinDTO} or a {@link Page} of {@link CheckinDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class CheckinQueryService extends QueryService<Checkin> {

    private final Logger log = LoggerFactory.getLogger(CheckinQueryService.class);

    private final CheckinRepository checkinRepository;

    private final CheckinMapper checkinMapper;

    public CheckinQueryService(CheckinRepository checkinRepository, CheckinMapper checkinMapper) {
        this.checkinRepository = checkinRepository;
        this.checkinMapper = checkinMapper;
    }

    /**
     * Return a {@link List} of {@link CheckinDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<CheckinDTO> findByCriteria(CheckinCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Checkin> specification = createSpecification(criteria);
        return checkinMapper.toDto(checkinRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link CheckinDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<CheckinDTO> findByCriteria(CheckinCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Checkin> specification = createSpecification(criteria);
        return checkinRepository.findAll(specification, page)
            .map(checkinMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(CheckinCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Checkin> specification = createSpecification(criteria);
        return checkinRepository.count(specification);
    }

    /**
     * Function to convert CheckinCriteria to a {@link Specification}.
     */
    private Specification<Checkin> createSpecification(CheckinCriteria criteria) {
        Specification<Checkin> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Checkin_.id));
            }
            if (criteria.getLatitude() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLatitude(), Checkin_.latitude));
            }
            if (criteria.getLongitude() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLongitude(), Checkin_.longitude));
            }
            if (criteria.getLinhaId() != null) {
                specification = specification.and(buildSpecification(criteria.getLinhaId(),
                    root -> root.join(Checkin_.linha, JoinType.LEFT).get(Linha_.id)));
            }
        }
        return specification;
    }
}
