package br.com.rotaurbana.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import br.com.rotaurbana.domain.PontoPesquisa;
import br.com.rotaurbana.domain.*; // for static metamodels
import br.com.rotaurbana.repository.PontoPesquisaRepository;
import br.com.rotaurbana.service.dto.PontoPesquisaCriteria;
import br.com.rotaurbana.service.dto.PontoPesquisaDTO;
import br.com.rotaurbana.service.mapper.PontoPesquisaMapper;

/**
 * Service for executing complex queries for {@link PontoPesquisa} entities in the database.
 * The main input is a {@link PontoPesquisaCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link PontoPesquisaDTO} or a {@link Page} of {@link PontoPesquisaDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class PontoPesquisaQueryService extends QueryService<PontoPesquisa> {

    private final Logger log = LoggerFactory.getLogger(PontoPesquisaQueryService.class);

    private final PontoPesquisaRepository pontoPesquisaRepository;

    private final PontoPesquisaMapper pontoPesquisaMapper;

    public PontoPesquisaQueryService(PontoPesquisaRepository pontoPesquisaRepository, PontoPesquisaMapper pontoPesquisaMapper) {
        this.pontoPesquisaRepository = pontoPesquisaRepository;
        this.pontoPesquisaMapper = pontoPesquisaMapper;
    }

    /**
     * Return a {@link List} of {@link PontoPesquisaDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<PontoPesquisaDTO> findByCriteria(PontoPesquisaCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<PontoPesquisa> specification = createSpecification(criteria);
        return pontoPesquisaMapper.toDto(pontoPesquisaRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link PontoPesquisaDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<PontoPesquisaDTO> findByCriteria(PontoPesquisaCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<PontoPesquisa> specification = createSpecification(criteria);
        return pontoPesquisaRepository.findAll(specification, page)
            .map(pontoPesquisaMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(PontoPesquisaCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<PontoPesquisa> specification = createSpecification(criteria);
        return pontoPesquisaRepository.count(specification);
    }

    /**
     * Function to convert PontoPesquisaCriteria to a {@link Specification}.
     */
    private Specification<PontoPesquisa> createSpecification(PontoPesquisaCriteria criteria) {
        Specification<PontoPesquisa> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), PontoPesquisa_.id));
            }
            if (criteria.getLatitude() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLatitude(), PontoPesquisa_.latitude));
            }
            if (criteria.getLongitude() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLongitude(), PontoPesquisa_.longitude));
            }
            if (criteria.getPosicao() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getPosicao(), PontoPesquisa_.posicao));
            }
            if (criteria.getTipo() != null) {
                specification = specification.and(buildSpecification(criteria.getTipo(), PontoPesquisa_.tipo));
            }
            if (criteria.getLinhaId() != null) {
                specification = specification.and(buildSpecification(criteria.getLinhaId(),
                    root -> root.join(PontoPesquisa_.linha, JoinType.LEFT).get(Linha_.id)));
            }
        }
        return specification;
    }
}
