package br.com.rotaurbana.service.impl;

import br.com.rotaurbana.service.ParadaService;
import br.com.rotaurbana.domain.Parada;
import br.com.rotaurbana.repository.ParadaRepository;
import br.com.rotaurbana.service.dto.ParadaDTO;
import br.com.rotaurbana.service.mapper.ParadaMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Parada}.
 */
@Service
@Transactional
public class ParadaServiceImpl implements ParadaService {

    private final Logger log = LoggerFactory.getLogger(ParadaServiceImpl.class);

    private final ParadaRepository paradaRepository;

    private final ParadaMapper paradaMapper;

    public ParadaServiceImpl(ParadaRepository paradaRepository, ParadaMapper paradaMapper) {
        this.paradaRepository = paradaRepository;
        this.paradaMapper = paradaMapper;
    }

    /**
     * Save a parada.
     *
     * @param paradaDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ParadaDTO save(ParadaDTO paradaDTO) {
        log.debug("Request to save Parada : {}", paradaDTO);
        Parada parada = paradaMapper.toEntity(paradaDTO);
        parada = paradaRepository.save(parada);
        return paradaMapper.toDto(parada);
    }

    /**
     * Get all the paradas.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ParadaDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Paradas");
        return paradaRepository.findAll(pageable)
            .map(paradaMapper::toDto);
    }


    /**
     * Get one parada by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ParadaDTO> findOne(Long id) {
        log.debug("Request to get Parada : {}", id);
        return paradaRepository.findById(id)
            .map(paradaMapper::toDto);
    }

    /**
     * Delete the parada by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Parada : {}", id);
        paradaRepository.deleteById(id);
    }
}
