package br.com.rotaurbana.service.impl;

import br.com.rotaurbana.service.PontoPesquisaService;
import br.com.rotaurbana.domain.PontoPesquisa;
import br.com.rotaurbana.repository.PontoPesquisaRepository;
import br.com.rotaurbana.service.dto.PontoPesquisaDTO;
import br.com.rotaurbana.service.mapper.PontoPesquisaMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link PontoPesquisa}.
 */
@Service
@Transactional
public class PontoPesquisaServiceImpl implements PontoPesquisaService {

    private final Logger log = LoggerFactory.getLogger(PontoPesquisaServiceImpl.class);

    private final PontoPesquisaRepository pontoPesquisaRepository;

    private final PontoPesquisaMapper pontoPesquisaMapper;

    public PontoPesquisaServiceImpl(PontoPesquisaRepository pontoPesquisaRepository, PontoPesquisaMapper pontoPesquisaMapper) {
        this.pontoPesquisaRepository = pontoPesquisaRepository;
        this.pontoPesquisaMapper = pontoPesquisaMapper;
    }

    /**
     * Save a pontoPesquisa.
     *
     * @param pontoPesquisaDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public PontoPesquisaDTO save(PontoPesquisaDTO pontoPesquisaDTO) {
        log.debug("Request to save PontoPesquisa : {}", pontoPesquisaDTO);
        PontoPesquisa pontoPesquisa = pontoPesquisaMapper.toEntity(pontoPesquisaDTO);
        pontoPesquisa = pontoPesquisaRepository.save(pontoPesquisa);
        return pontoPesquisaMapper.toDto(pontoPesquisa);
    }

    /**
     * Get all the pontoPesquisas.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PontoPesquisaDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PontoPesquisas");
        return pontoPesquisaRepository.findAll(pageable)
            .map(pontoPesquisaMapper::toDto);
    }


    /**
     * Get one pontoPesquisa by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<PontoPesquisaDTO> findOne(Long id) {
        log.debug("Request to get PontoPesquisa : {}", id);
        return pontoPesquisaRepository.findById(id)
            .map(pontoPesquisaMapper::toDto);
    }

    /**
     * Delete the pontoPesquisa by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete PontoPesquisa : {}", id);
        pontoPesquisaRepository.deleteById(id);
    }
}
