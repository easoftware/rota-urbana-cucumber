package br.com.rotaurbana.service.impl;

import br.com.rotaurbana.service.LinhaService;
import br.com.rotaurbana.domain.Linha;
import br.com.rotaurbana.repository.LinhaRepository;
import br.com.rotaurbana.service.dto.LinhaDTO;
import br.com.rotaurbana.service.mapper.LinhaMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Linha}.
 */
@Service
@Transactional
public class LinhaServiceImpl implements LinhaService {

    private final Logger log = LoggerFactory.getLogger(LinhaServiceImpl.class);

    private final LinhaRepository linhaRepository;

    private final LinhaMapper linhaMapper;

    public LinhaServiceImpl(LinhaRepository linhaRepository, LinhaMapper linhaMapper) {
        this.linhaRepository = linhaRepository;
        this.linhaMapper = linhaMapper;
    }

    /**
     * Save a linha.
     *
     * @param linhaDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public LinhaDTO save(LinhaDTO linhaDTO) {
        log.debug("Request to save Linha : {}", linhaDTO);
        Linha linha = linhaMapper.toEntity(linhaDTO);
        linha = linhaRepository.save(linha);
        return linhaMapper.toDto(linha);
    }

    /**
     * Get all the linhas.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<LinhaDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Linhas");
        return linhaRepository.findAll(pageable)
            .map(linhaMapper::toDto);
    }

    /**
     * Get all the linhas with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    public Page<LinhaDTO> findAllWithEagerRelationships(Pageable pageable) {
        return linhaRepository.findAllWithEagerRelationships(pageable).map(linhaMapper::toDto);
    }
    

    /**
     * Get one linha by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<LinhaDTO> findOne(Long id) {
        log.debug("Request to get Linha : {}", id);
        return linhaRepository.findOneWithEagerRelationships(id)
            .map(linhaMapper::toDto);
    }

    /**
     * Delete the linha by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Linha : {}", id);
        linhaRepository.deleteById(id);
    }
}
