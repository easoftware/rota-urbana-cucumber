package br.com.rotaurbana.service.impl;

import br.com.rotaurbana.service.PontoService;
import br.com.rotaurbana.domain.Ponto;
import br.com.rotaurbana.repository.PontoRepository;
import br.com.rotaurbana.service.dto.PontoDTO;
import br.com.rotaurbana.service.mapper.PontoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Ponto}.
 */
@Service
@Transactional
public class PontoServiceImpl implements PontoService {

    private final Logger log = LoggerFactory.getLogger(PontoServiceImpl.class);

    private final PontoRepository pontoRepository;

    private final PontoMapper pontoMapper;

    public PontoServiceImpl(PontoRepository pontoRepository, PontoMapper pontoMapper) {
        this.pontoRepository = pontoRepository;
        this.pontoMapper = pontoMapper;
    }

    /**
     * Save a ponto.
     *
     * @param pontoDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public PontoDTO save(PontoDTO pontoDTO) {
        log.debug("Request to save Ponto : {}", pontoDTO);
        Ponto ponto = pontoMapper.toEntity(pontoDTO);
        ponto = pontoRepository.save(ponto);
        return pontoMapper.toDto(ponto);
    }

    /**
     * Get all the pontos.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PontoDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Pontos");
        return pontoRepository.findAll(pageable)
            .map(pontoMapper::toDto);
    }


    /**
     * Get one ponto by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<PontoDTO> findOne(Long id) {
        log.debug("Request to get Ponto : {}", id);
        return pontoRepository.findById(id)
            .map(pontoMapper::toDto);
    }

    /**
     * Delete the ponto by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Ponto : {}", id);
        pontoRepository.deleteById(id);
    }
}
