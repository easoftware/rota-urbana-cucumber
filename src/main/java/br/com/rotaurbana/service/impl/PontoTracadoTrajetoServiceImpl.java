package br.com.rotaurbana.service.impl;

import br.com.rotaurbana.service.PontoTracadoTrajetoService;
import br.com.rotaurbana.domain.PontoTracadoTrajeto;
import br.com.rotaurbana.repository.PontoTracadoTrajetoRepository;
import br.com.rotaurbana.service.dto.PontoTracadoTrajetoDTO;
import br.com.rotaurbana.service.mapper.PontoTracadoTrajetoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link PontoTracadoTrajeto}.
 */
@Service
@Transactional
public class PontoTracadoTrajetoServiceImpl implements PontoTracadoTrajetoService {

    private final Logger log = LoggerFactory.getLogger(PontoTracadoTrajetoServiceImpl.class);

    private final PontoTracadoTrajetoRepository pontoTracadoTrajetoRepository;

    private final PontoTracadoTrajetoMapper pontoTracadoTrajetoMapper;

    public PontoTracadoTrajetoServiceImpl(PontoTracadoTrajetoRepository pontoTracadoTrajetoRepository, PontoTracadoTrajetoMapper pontoTracadoTrajetoMapper) {
        this.pontoTracadoTrajetoRepository = pontoTracadoTrajetoRepository;
        this.pontoTracadoTrajetoMapper = pontoTracadoTrajetoMapper;
    }

    /**
     * Save a pontoTracadoTrajeto.
     *
     * @param pontoTracadoTrajetoDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public PontoTracadoTrajetoDTO save(PontoTracadoTrajetoDTO pontoTracadoTrajetoDTO) {
        log.debug("Request to save PontoTracadoTrajeto : {}", pontoTracadoTrajetoDTO);
        PontoTracadoTrajeto pontoTracadoTrajeto = pontoTracadoTrajetoMapper.toEntity(pontoTracadoTrajetoDTO);
        pontoTracadoTrajeto = pontoTracadoTrajetoRepository.save(pontoTracadoTrajeto);
        return pontoTracadoTrajetoMapper.toDto(pontoTracadoTrajeto);
    }

    /**
     * Get all the pontoTracadoTrajetos.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PontoTracadoTrajetoDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PontoTracadoTrajetos");
        return pontoTracadoTrajetoRepository.findAll(pageable)
            .map(pontoTracadoTrajetoMapper::toDto);
    }


    /**
     * Get one pontoTracadoTrajeto by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<PontoTracadoTrajetoDTO> findOne(Long id) {
        log.debug("Request to get PontoTracadoTrajeto : {}", id);
        return pontoTracadoTrajetoRepository.findById(id)
            .map(pontoTracadoTrajetoMapper::toDto);
    }

    /**
     * Delete the pontoTracadoTrajeto by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete PontoTracadoTrajeto : {}", id);
        pontoTracadoTrajetoRepository.deleteById(id);
    }
}
