package br.com.rotaurbana.service.mapper;

import br.com.rotaurbana.domain.*;
import br.com.rotaurbana.service.dto.EstadoDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Estado} and its DTO {@link EstadoDTO}.
 */
@Mapper(componentModel = "spring", uses = {PaisMapper.class})
public interface EstadoMapper extends EntityMapper<EstadoDTO, Estado> {

    @Mapping(source = "pais.id", target = "paisId")
    @Mapping(source = "pais.nome", target = "paisNome")
    EstadoDTO toDto(Estado estado);

    @Mapping(source = "paisId", target = "pais")
    Estado toEntity(EstadoDTO estadoDTO);

    default Estado fromId(Long id) {
        if (id == null) {
            return null;
        }
        Estado estado = new Estado();
        estado.setId(id);
        return estado;
    }
}
