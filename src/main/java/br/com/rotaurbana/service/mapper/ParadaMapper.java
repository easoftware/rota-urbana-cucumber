package br.com.rotaurbana.service.mapper;

import br.com.rotaurbana.domain.*;
import br.com.rotaurbana.service.dto.ParadaDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Parada} and its DTO {@link ParadaDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ParadaMapper extends EntityMapper<ParadaDTO, Parada> {


    @Mapping(target = "linhas", ignore = true)
    @Mapping(target = "removeLinhas", ignore = true)
    Parada toEntity(ParadaDTO paradaDTO);

    default Parada fromId(Long id) {
        if (id == null) {
            return null;
        }
        Parada parada = new Parada();
        parada.setId(id);
        return parada;
    }
}
