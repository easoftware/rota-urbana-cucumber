package br.com.rotaurbana.service.mapper;

import br.com.rotaurbana.domain.*;
import br.com.rotaurbana.service.dto.PontoPesquisaDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link PontoPesquisa} and its DTO {@link PontoPesquisaDTO}.
 */
@Mapper(componentModel = "spring", uses = {LinhaMapper.class})
public interface PontoPesquisaMapper extends EntityMapper<PontoPesquisaDTO, PontoPesquisa> {

    @Mapping(source = "linha.id", target = "linhaId")
    PontoPesquisaDTO toDto(PontoPesquisa pontoPesquisa);

    @Mapping(source = "linhaId", target = "linha")
    PontoPesquisa toEntity(PontoPesquisaDTO pontoPesquisaDTO);

    default PontoPesquisa fromId(Long id) {
        if (id == null) {
            return null;
        }
        PontoPesquisa pontoPesquisa = new PontoPesquisa();
        pontoPesquisa.setId(id);
        return pontoPesquisa;
    }
}
