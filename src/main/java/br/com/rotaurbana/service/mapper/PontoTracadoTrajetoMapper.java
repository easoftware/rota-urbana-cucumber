package br.com.rotaurbana.service.mapper;

import br.com.rotaurbana.domain.*;
import br.com.rotaurbana.service.dto.PontoTracadoTrajetoDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link PontoTracadoTrajeto} and its DTO {@link PontoTracadoTrajetoDTO}.
 */
@Mapper(componentModel = "spring", uses = {LinhaMapper.class})
public interface PontoTracadoTrajetoMapper extends EntityMapper<PontoTracadoTrajetoDTO, PontoTracadoTrajeto> {

    @Mapping(source = "linha.id", target = "linhaId")
    PontoTracadoTrajetoDTO toDto(PontoTracadoTrajeto pontoTracadoTrajeto);

    @Mapping(source = "linhaId", target = "linha")
    PontoTracadoTrajeto toEntity(PontoTracadoTrajetoDTO pontoTracadoTrajetoDTO);

    default PontoTracadoTrajeto fromId(Long id) {
        if (id == null) {
            return null;
        }
        PontoTracadoTrajeto pontoTracadoTrajeto = new PontoTracadoTrajeto();
        pontoTracadoTrajeto.setId(id);
        return pontoTracadoTrajeto;
    }
}
