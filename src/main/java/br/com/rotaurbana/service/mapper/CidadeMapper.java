package br.com.rotaurbana.service.mapper;

import br.com.rotaurbana.domain.*;
import br.com.rotaurbana.service.dto.CidadeDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Cidade} and its DTO {@link CidadeDTO}.
 */
@Mapper(componentModel = "spring", uses = {EstadoMapper.class})
public interface CidadeMapper extends EntityMapper<CidadeDTO, Cidade> {

    @Mapping(source = "belongsTo.id", target = "belongsToId")
    @Mapping(source = "belongsTo.nome", target = "belongsToNome")
    @Mapping(source = "estado.id", target = "estadoId")
    @Mapping(source = "estado.nome", target = "estadoNome")
    CidadeDTO toDto(Cidade cidade);

    @Mapping(source = "belongsToId", target = "belongsTo")
    @Mapping(source = "estadoId", target = "estado")
    Cidade toEntity(CidadeDTO cidadeDTO);

    default Cidade fromId(Long id) {
        if (id == null) {
            return null;
        }
        Cidade cidade = new Cidade();
        cidade.setId(id);
        return cidade;
    }
}
