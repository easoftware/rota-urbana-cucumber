package br.com.rotaurbana.service.mapper;

import br.com.rotaurbana.domain.*;
import br.com.rotaurbana.service.dto.CheckinDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Checkin} and its DTO {@link CheckinDTO}.
 */
@Mapper(componentModel = "spring", uses = {LinhaMapper.class})
public interface CheckinMapper extends EntityMapper<CheckinDTO, Checkin> {

    @Mapping(source = "linha.id", target = "linhaId")
    @Mapping(source = "linha.nome", target = "linhaNome")
    CheckinDTO toDto(Checkin checkin);

    @Mapping(source = "linhaId", target = "linha")
    Checkin toEntity(CheckinDTO checkinDTO);

    default Checkin fromId(Long id) {
        if (id == null) {
            return null;
        }
        Checkin checkin = new Checkin();
        checkin.setId(id);
        return checkin;
    }
}
