package br.com.rotaurbana.service.mapper;

import br.com.rotaurbana.domain.*;
import br.com.rotaurbana.service.dto.PontoDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Ponto} and its DTO {@link PontoDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface PontoMapper extends EntityMapper<PontoDTO, Ponto> {



    default Ponto fromId(Long id) {
        if (id == null) {
            return null;
        }
        Ponto ponto = new Ponto();
        ponto.setId(id);
        return ponto;
    }
}
