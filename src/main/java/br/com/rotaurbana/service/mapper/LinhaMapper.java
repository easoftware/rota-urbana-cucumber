package br.com.rotaurbana.service.mapper;

import br.com.rotaurbana.domain.*;
import br.com.rotaurbana.service.dto.LinhaDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Linha} and its DTO {@link LinhaDTO}.
 */
@Mapper(componentModel = "spring", uses = {CidadeMapper.class, ParadaMapper.class})
public interface LinhaMapper extends EntityMapper<LinhaDTO, Linha> {

    @Mapping(source = "cidade.id", target = "cidadeId")
    @Mapping(source = "cidade.nome", target = "cidadeNome")
    @Mapping(source = "root.id", target = "rootId")
    LinhaDTO toDto(Linha linha);

    @Mapping(target = "itinerarios", ignore = true)
    @Mapping(target = "removeItinerario", ignore = true)
    @Mapping(target = "pontosPesquisas", ignore = true)
    @Mapping(target = "removePontosPesquisa", ignore = true)
    @Mapping(source = "cidadeId", target = "cidade")
    @Mapping(source = "rootId", target = "root")
    @Mapping(target = "removeParadas", ignore = true)
    @Mapping(target = "children", ignore = true)
    @Mapping(target = "removeChildren", ignore = true)
    Linha toEntity(LinhaDTO linhaDTO);

    default Linha fromId(Long id) {
        if (id == null) {
            return null;
        }
        Linha linha = new Linha();
        linha.setId(id);
        return linha;
    }
}
