package br.com.rotaurbana.service;

import br.com.rotaurbana.service.dto.PontoPesquisaDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link br.com.rotaurbana.domain.PontoPesquisa}.
 */
public interface PontoPesquisaService {

    /**
     * Save a pontoPesquisa.
     *
     * @param pontoPesquisaDTO the entity to save.
     * @return the persisted entity.
     */
    PontoPesquisaDTO save(PontoPesquisaDTO pontoPesquisaDTO);

    /**
     * Get all the pontoPesquisas.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<PontoPesquisaDTO> findAll(Pageable pageable);


    /**
     * Get the "id" pontoPesquisa.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<PontoPesquisaDTO> findOne(Long id);

    /**
     * Delete the "id" pontoPesquisa.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
