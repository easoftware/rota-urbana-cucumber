package br.com.rotaurbana.config;

import java.time.Duration;

import org.ehcache.config.builders.*;
import org.ehcache.jsr107.Eh107Configuration;

import org.hibernate.cache.jcache.ConfigSettings;
import io.github.jhipster.config.JHipsterProperties;

import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.boot.autoconfigure.orm.jpa.HibernatePropertiesCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.*;

@Configuration
@EnableCaching
public class CacheConfiguration {

    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        JHipsterProperties.Cache.Ehcache ehcache =
            jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class,
                ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofSeconds(ehcache.getTimeToLiveSeconds())))
                .build());
    }

    @Bean
    public HibernatePropertiesCustomizer hibernatePropertiesCustomizer(javax.cache.CacheManager cacheManager) {
        return hibernateProperties -> hibernateProperties.put(ConfigSettings.CACHE_MANAGER, cacheManager);
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            createCache(cm, br.com.rotaurbana.repository.UserRepository.USERS_BY_LOGIN_CACHE);
            createCache(cm, br.com.rotaurbana.repository.UserRepository.USERS_BY_EMAIL_CACHE);
            createCache(cm, br.com.rotaurbana.domain.User.class.getName());
            createCache(cm, br.com.rotaurbana.domain.Authority.class.getName());
            createCache(cm, br.com.rotaurbana.domain.User.class.getName() + ".authorities");
            createCache(cm, br.com.rotaurbana.domain.Checkin.class.getName());
            createCache(cm, br.com.rotaurbana.domain.Ponto.class.getName());
            createCache(cm, br.com.rotaurbana.domain.Cidade.class.getName());
            createCache(cm, br.com.rotaurbana.domain.Estado.class.getName());
            createCache(cm, br.com.rotaurbana.domain.Linha.class.getName());
            createCache(cm, br.com.rotaurbana.domain.Linha.class.getName() + ".itinerarios");
            createCache(cm, br.com.rotaurbana.domain.Linha.class.getName() + ".pontosPesquisas");
            createCache(cm, br.com.rotaurbana.domain.Linha.class.getName() + ".paradas");
            createCache(cm, br.com.rotaurbana.domain.Linha.class.getName() + ".children");
            createCache(cm, br.com.rotaurbana.domain.Pais.class.getName());
            createCache(cm, br.com.rotaurbana.domain.Parada.class.getName());
            createCache(cm, br.com.rotaurbana.domain.Parada.class.getName() + ".linhas");
            createCache(cm, br.com.rotaurbana.domain.PontoTracadoTrajeto.class.getName());
            createCache(cm, br.com.rotaurbana.domain.PontoPesquisa.class.getName());
            // jhipster-needle-ehcache-add-entry
        };
    }

    private void createCache(javax.cache.CacheManager cm, String cacheName) {
        javax.cache.Cache<Object, Object> cache = cm.getCache(cacheName);
        if (cache != null) {
            cm.destroyCache(cacheName);
        }
        cm.createCache(cacheName, jcacheConfiguration);
    }
}
