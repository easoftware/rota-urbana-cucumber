import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IPontoPesquisa } from 'app/shared/model/ponto-pesquisa.model';

@Component({
  selector: 'jhi-ponto-pesquisa-detail',
  templateUrl: './ponto-pesquisa-detail.component.html'
})
export class PontoPesquisaDetailComponent implements OnInit {
  pontoPesquisa: IPontoPesquisa;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ pontoPesquisa }) => {
      this.pontoPesquisa = pontoPesquisa;
    });
  }

  previousState() {
    window.history.back();
  }
}
