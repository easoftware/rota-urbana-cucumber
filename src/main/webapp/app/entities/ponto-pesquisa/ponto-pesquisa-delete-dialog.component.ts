import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IPontoPesquisa } from 'app/shared/model/ponto-pesquisa.model';
import { PontoPesquisaService } from './ponto-pesquisa.service';

@Component({
  selector: 'jhi-ponto-pesquisa-delete-dialog',
  templateUrl: './ponto-pesquisa-delete-dialog.component.html'
})
export class PontoPesquisaDeleteDialogComponent {
  pontoPesquisa: IPontoPesquisa;

  constructor(
    protected pontoPesquisaService: PontoPesquisaService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.pontoPesquisaService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'pontoPesquisaListModification',
        content: 'Deleted an pontoPesquisa'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-ponto-pesquisa-delete-popup',
  template: ''
})
export class PontoPesquisaDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ pontoPesquisa }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(PontoPesquisaDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.pontoPesquisa = pontoPesquisa;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/ponto-pesquisa', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/ponto-pesquisa', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
