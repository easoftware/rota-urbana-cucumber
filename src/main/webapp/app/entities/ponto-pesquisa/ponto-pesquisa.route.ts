import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { PontoPesquisa } from 'app/shared/model/ponto-pesquisa.model';
import { PontoPesquisaService } from './ponto-pesquisa.service';
import { PontoPesquisaComponent } from './ponto-pesquisa.component';
import { PontoPesquisaDetailComponent } from './ponto-pesquisa-detail.component';
import { PontoPesquisaUpdateComponent } from './ponto-pesquisa-update.component';
import { PontoPesquisaDeletePopupComponent } from './ponto-pesquisa-delete-dialog.component';
import { IPontoPesquisa } from 'app/shared/model/ponto-pesquisa.model';

@Injectable({ providedIn: 'root' })
export class PontoPesquisaResolve implements Resolve<IPontoPesquisa> {
  constructor(private service: PontoPesquisaService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IPontoPesquisa> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<PontoPesquisa>) => response.ok),
        map((pontoPesquisa: HttpResponse<PontoPesquisa>) => pontoPesquisa.body)
      );
    }
    return of(new PontoPesquisa());
  }
}

export const pontoPesquisaRoute: Routes = [
  {
    path: '',
    component: PontoPesquisaComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'PontoPesquisas'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: PontoPesquisaDetailComponent,
    resolve: {
      pontoPesquisa: PontoPesquisaResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'PontoPesquisas'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: PontoPesquisaUpdateComponent,
    resolve: {
      pontoPesquisa: PontoPesquisaResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'PontoPesquisas'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: PontoPesquisaUpdateComponent,
    resolve: {
      pontoPesquisa: PontoPesquisaResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'PontoPesquisas'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const pontoPesquisaPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: PontoPesquisaDeletePopupComponent,
    resolve: {
      pontoPesquisa: PontoPesquisaResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'PontoPesquisas'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
