import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { RotaurbanacucumberSharedModule } from 'app/shared';
import {
  PontoPesquisaComponent,
  PontoPesquisaDetailComponent,
  PontoPesquisaUpdateComponent,
  PontoPesquisaDeletePopupComponent,
  PontoPesquisaDeleteDialogComponent,
  pontoPesquisaRoute,
  pontoPesquisaPopupRoute
} from './';

const ENTITY_STATES = [...pontoPesquisaRoute, ...pontoPesquisaPopupRoute];

@NgModule({
  imports: [RotaurbanacucumberSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    PontoPesquisaComponent,
    PontoPesquisaDetailComponent,
    PontoPesquisaUpdateComponent,
    PontoPesquisaDeleteDialogComponent,
    PontoPesquisaDeletePopupComponent
  ],
  entryComponents: [
    PontoPesquisaComponent,
    PontoPesquisaUpdateComponent,
    PontoPesquisaDeleteDialogComponent,
    PontoPesquisaDeletePopupComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class RotaurbanacucumberPontoPesquisaModule {}
