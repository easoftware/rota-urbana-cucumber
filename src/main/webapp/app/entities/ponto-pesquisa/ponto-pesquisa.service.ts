import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IPontoPesquisa } from 'app/shared/model/ponto-pesquisa.model';

type EntityResponseType = HttpResponse<IPontoPesquisa>;
type EntityArrayResponseType = HttpResponse<IPontoPesquisa[]>;

@Injectable({ providedIn: 'root' })
export class PontoPesquisaService {
  public resourceUrl = SERVER_API_URL + 'api/ponto-pesquisas';

  constructor(protected http: HttpClient) {}

  create(pontoPesquisa: IPontoPesquisa): Observable<EntityResponseType> {
    return this.http.post<IPontoPesquisa>(this.resourceUrl, pontoPesquisa, { observe: 'response' });
  }

  update(pontoPesquisa: IPontoPesquisa): Observable<EntityResponseType> {
    return this.http.put<IPontoPesquisa>(this.resourceUrl, pontoPesquisa, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IPontoPesquisa>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IPontoPesquisa[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
