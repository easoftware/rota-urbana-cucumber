import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IPontoPesquisa, PontoPesquisa } from 'app/shared/model/ponto-pesquisa.model';
import { PontoPesquisaService } from './ponto-pesquisa.service';
import { ILinha } from 'app/shared/model/linha.model';
import { LinhaService } from 'app/entities/linha';

@Component({
  selector: 'jhi-ponto-pesquisa-update',
  templateUrl: './ponto-pesquisa-update.component.html'
})
export class PontoPesquisaUpdateComponent implements OnInit {
  isSaving: boolean;

  linhas: ILinha[];

  editForm = this.fb.group({
    id: [],
    latitude: [],
    longitude: [],
    posicao: [],
    tipo: [],
    linhaId: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected pontoPesquisaService: PontoPesquisaService,
    protected linhaService: LinhaService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ pontoPesquisa }) => {
      this.updateForm(pontoPesquisa);
    });
    this.linhaService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<ILinha[]>) => mayBeOk.ok),
        map((response: HttpResponse<ILinha[]>) => response.body)
      )
      .subscribe((res: ILinha[]) => (this.linhas = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(pontoPesquisa: IPontoPesquisa) {
    this.editForm.patchValue({
      id: pontoPesquisa.id,
      latitude: pontoPesquisa.latitude,
      longitude: pontoPesquisa.longitude,
      posicao: pontoPesquisa.posicao,
      tipo: pontoPesquisa.tipo,
      linhaId: pontoPesquisa.linhaId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const pontoPesquisa = this.createFromForm();
    if (pontoPesquisa.id !== undefined) {
      this.subscribeToSaveResponse(this.pontoPesquisaService.update(pontoPesquisa));
    } else {
      this.subscribeToSaveResponse(this.pontoPesquisaService.create(pontoPesquisa));
    }
  }

  private createFromForm(): IPontoPesquisa {
    const entity = {
      ...new PontoPesquisa(),
      id: this.editForm.get(['id']).value,
      latitude: this.editForm.get(['latitude']).value,
      longitude: this.editForm.get(['longitude']).value,
      posicao: this.editForm.get(['posicao']).value,
      tipo: this.editForm.get(['tipo']).value,
      linhaId: this.editForm.get(['linhaId']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPontoPesquisa>>) {
    result.subscribe((res: HttpResponse<IPontoPesquisa>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackLinhaById(index: number, item: ILinha) {
    return item.id;
  }
}
