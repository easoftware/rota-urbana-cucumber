import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { ICidade, Cidade } from 'app/shared/model/cidade.model';
import { CidadeService } from './cidade.service';
import { IEstado } from 'app/shared/model/estado.model';
import { EstadoService } from 'app/entities/estado';

@Component({
  selector: 'jhi-cidade-update',
  templateUrl: './cidade-update.component.html'
})
export class CidadeUpdateComponent implements OnInit {
  isSaving: boolean;

  cidades: ICidade[];

  estados: IEstado[];

  editForm = this.fb.group({
    id: [],
    nome: [],
    latitude: [],
    longitude: [],
    latitudeDouble: [],
    longitudeDouble: [],
    sameAs: [],
    belongsToId: [],
    estadoId: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected cidadeService: CidadeService,
    protected estadoService: EstadoService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ cidade }) => {
      this.updateForm(cidade);
    });
    this.cidadeService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<ICidade[]>) => mayBeOk.ok),
        map((response: HttpResponse<ICidade[]>) => response.body)
      )
      .subscribe((res: ICidade[]) => (this.cidades = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.estadoService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IEstado[]>) => mayBeOk.ok),
        map((response: HttpResponse<IEstado[]>) => response.body)
      )
      .subscribe((res: IEstado[]) => (this.estados = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(cidade: ICidade) {
    this.editForm.patchValue({
      id: cidade.id,
      nome: cidade.nome,
      latitude: cidade.latitude,
      longitude: cidade.longitude,
      latitudeDouble: cidade.latitudeDouble,
      longitudeDouble: cidade.longitudeDouble,
      sameAs: cidade.sameAs,
      belongsToId: cidade.belongsToId,
      estadoId: cidade.estadoId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const cidade = this.createFromForm();
    if (cidade.id !== undefined) {
      this.subscribeToSaveResponse(this.cidadeService.update(cidade));
    } else {
      this.subscribeToSaveResponse(this.cidadeService.create(cidade));
    }
  }

  private createFromForm(): ICidade {
    const entity = {
      ...new Cidade(),
      id: this.editForm.get(['id']).value,
      nome: this.editForm.get(['nome']).value,
      latitude: this.editForm.get(['latitude']).value,
      longitude: this.editForm.get(['longitude']).value,
      latitudeDouble: this.editForm.get(['latitudeDouble']).value,
      longitudeDouble: this.editForm.get(['longitudeDouble']).value,
      sameAs: this.editForm.get(['sameAs']).value,
      belongsToId: this.editForm.get(['belongsToId']).value,
      estadoId: this.editForm.get(['estadoId']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICidade>>) {
    result.subscribe((res: HttpResponse<ICidade>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackCidadeById(index: number, item: ICidade) {
    return item.id;
  }

  trackEstadoById(index: number, item: IEstado) {
    return item.id;
  }
}
