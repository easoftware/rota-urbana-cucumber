import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ILinha } from 'app/shared/model/linha.model';

type EntityResponseType = HttpResponse<ILinha>;
type EntityArrayResponseType = HttpResponse<ILinha[]>;

@Injectable({ providedIn: 'root' })
export class LinhaService {
  public resourceUrl = SERVER_API_URL + 'api/linhas';

  constructor(protected http: HttpClient) {}

  create(linha: ILinha): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(linha);
    return this.http
      .post<ILinha>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(linha: ILinha): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(linha);
    return this.http
      .put<ILinha>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<ILinha>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ILinha[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(linha: ILinha): ILinha {
    const copy: ILinha = Object.assign({}, linha, {
      lastUpdate: linha.lastUpdate != null && linha.lastUpdate.isValid() ? linha.lastUpdate.toJSON() : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.lastUpdate = res.body.lastUpdate != null ? moment(res.body.lastUpdate) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((linha: ILinha) => {
        linha.lastUpdate = linha.lastUpdate != null ? moment(linha.lastUpdate) : null;
      });
    }
    return res;
  }
}
