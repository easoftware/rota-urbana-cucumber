import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Linha } from 'app/shared/model/linha.model';
import { LinhaService } from './linha.service';
import { LinhaComponent } from './linha.component';
import { LinhaDetailComponent } from './linha-detail.component';
import { LinhaUpdateComponent } from './linha-update.component';
import { LinhaDeletePopupComponent } from './linha-delete-dialog.component';
import { ILinha } from 'app/shared/model/linha.model';

@Injectable({ providedIn: 'root' })
export class LinhaResolve implements Resolve<ILinha> {
  constructor(private service: LinhaService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ILinha> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Linha>) => response.ok),
        map((linha: HttpResponse<Linha>) => linha.body)
      );
    }
    return of(new Linha());
  }
}

export const linhaRoute: Routes = [
  {
    path: '',
    component: LinhaComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'Linhas'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: LinhaDetailComponent,
    resolve: {
      linha: LinhaResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Linhas'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: LinhaUpdateComponent,
    resolve: {
      linha: LinhaResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Linhas'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: LinhaUpdateComponent,
    resolve: {
      linha: LinhaResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Linhas'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const linhaPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: LinhaDeletePopupComponent,
    resolve: {
      linha: LinhaResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Linhas'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
