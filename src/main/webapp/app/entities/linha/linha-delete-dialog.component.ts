import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ILinha } from 'app/shared/model/linha.model';
import { LinhaService } from './linha.service';

@Component({
  selector: 'jhi-linha-delete-dialog',
  templateUrl: './linha-delete-dialog.component.html'
})
export class LinhaDeleteDialogComponent {
  linha: ILinha;

  constructor(protected linhaService: LinhaService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.linhaService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'linhaListModification',
        content: 'Deleted an linha'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-linha-delete-popup',
  template: ''
})
export class LinhaDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ linha }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(LinhaDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.linha = linha;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/linha', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/linha', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
