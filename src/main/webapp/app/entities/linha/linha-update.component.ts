import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';
import { ILinha, Linha } from 'app/shared/model/linha.model';
import { LinhaService } from './linha.service';
import { ICidade } from 'app/shared/model/cidade.model';
import { CidadeService } from 'app/entities/cidade';
import { IParada } from 'app/shared/model/parada.model';
import { ParadaService } from 'app/entities/parada';

@Component({
  selector: 'jhi-linha-update',
  templateUrl: './linha-update.component.html'
})
export class LinhaUpdateComponent implements OnInit {
  isSaving: boolean;

  cidades: ICidade[];

  linhas: ILinha[];

  paradas: IParada[];

  editForm = this.fb.group({
    id: [],
    nome: [],
    codigo: [],
    url: [],
    completa: [],
    lastUpdate: [],
    comentario: [],
    itinerarioTotalEncoding: [null, [Validators.maxLength(2000)]],
    emAvaliacao: [],
    faltaCadastrarPontosPesquisa: [],
    semob: [],
    tipoDeRota: [],
    cidadeId: [],
    rootId: [],
    paradas: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected linhaService: LinhaService,
    protected cidadeService: CidadeService,
    protected paradaService: ParadaService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ linha }) => {
      this.updateForm(linha);
    });
    this.cidadeService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<ICidade[]>) => mayBeOk.ok),
        map((response: HttpResponse<ICidade[]>) => response.body)
      )
      .subscribe((res: ICidade[]) => (this.cidades = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.linhaService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<ILinha[]>) => mayBeOk.ok),
        map((response: HttpResponse<ILinha[]>) => response.body)
      )
      .subscribe((res: ILinha[]) => (this.linhas = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.paradaService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IParada[]>) => mayBeOk.ok),
        map((response: HttpResponse<IParada[]>) => response.body)
      )
      .subscribe((res: IParada[]) => (this.paradas = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(linha: ILinha) {
    this.editForm.patchValue({
      id: linha.id,
      nome: linha.nome,
      codigo: linha.codigo,
      url: linha.url,
      completa: linha.completa,
      lastUpdate: linha.lastUpdate != null ? linha.lastUpdate.format(DATE_TIME_FORMAT) : null,
      comentario: linha.comentario,
      itinerarioTotalEncoding: linha.itinerarioTotalEncoding,
      emAvaliacao: linha.emAvaliacao,
      faltaCadastrarPontosPesquisa: linha.faltaCadastrarPontosPesquisa,
      semob: linha.semob,
      tipoDeRota: linha.tipoDeRota,
      cidadeId: linha.cidadeId,
      rootId: linha.rootId,
      paradas: linha.paradas
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const linha = this.createFromForm();
    if (linha.id !== undefined) {
      this.subscribeToSaveResponse(this.linhaService.update(linha));
    } else {
      this.subscribeToSaveResponse(this.linhaService.create(linha));
    }
  }

  private createFromForm(): ILinha {
    const entity = {
      ...new Linha(),
      id: this.editForm.get(['id']).value,
      nome: this.editForm.get(['nome']).value,
      codigo: this.editForm.get(['codigo']).value,
      url: this.editForm.get(['url']).value,
      completa: this.editForm.get(['completa']).value,
      lastUpdate:
        this.editForm.get(['lastUpdate']).value != null ? moment(this.editForm.get(['lastUpdate']).value, DATE_TIME_FORMAT) : undefined,
      comentario: this.editForm.get(['comentario']).value,
      itinerarioTotalEncoding: this.editForm.get(['itinerarioTotalEncoding']).value,
      emAvaliacao: this.editForm.get(['emAvaliacao']).value,
      faltaCadastrarPontosPesquisa: this.editForm.get(['faltaCadastrarPontosPesquisa']).value,
      semob: this.editForm.get(['semob']).value,
      tipoDeRota: this.editForm.get(['tipoDeRota']).value,
      cidadeId: this.editForm.get(['cidadeId']).value,
      rootId: this.editForm.get(['rootId']).value,
      paradas: this.editForm.get(['paradas']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ILinha>>) {
    result.subscribe((res: HttpResponse<ILinha>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackCidadeById(index: number, item: ICidade) {
    return item.id;
  }

  trackLinhaById(index: number, item: ILinha) {
    return item.id;
  }

  trackParadaById(index: number, item: IParada) {
    return item.id;
  }

  getSelected(selectedVals: Array<any>, option: any) {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
