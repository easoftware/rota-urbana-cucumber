import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { RotaurbanacucumberSharedModule } from 'app/shared';
import {
  LinhaComponent,
  LinhaDetailComponent,
  LinhaUpdateComponent,
  LinhaDeletePopupComponent,
  LinhaDeleteDialogComponent,
  linhaRoute,
  linhaPopupRoute
} from './';

const ENTITY_STATES = [...linhaRoute, ...linhaPopupRoute];

@NgModule({
  imports: [RotaurbanacucumberSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [LinhaComponent, LinhaDetailComponent, LinhaUpdateComponent, LinhaDeleteDialogComponent, LinhaDeletePopupComponent],
  entryComponents: [LinhaComponent, LinhaUpdateComponent, LinhaDeleteDialogComponent, LinhaDeletePopupComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class RotaurbanacucumberLinhaModule {}
