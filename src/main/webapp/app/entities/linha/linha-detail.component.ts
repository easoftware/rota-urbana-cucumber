import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ILinha } from 'app/shared/model/linha.model';

@Component({
  selector: 'jhi-linha-detail',
  templateUrl: './linha-detail.component.html'
})
export class LinhaDetailComponent implements OnInit {
  linha: ILinha;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ linha }) => {
      this.linha = linha;
    });
  }

  previousState() {
    window.history.back();
  }
}
