import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IParada } from 'app/shared/model/parada.model';

type EntityResponseType = HttpResponse<IParada>;
type EntityArrayResponseType = HttpResponse<IParada[]>;

@Injectable({ providedIn: 'root' })
export class ParadaService {
  public resourceUrl = SERVER_API_URL + 'api/paradas';

  constructor(protected http: HttpClient) {}

  create(parada: IParada): Observable<EntityResponseType> {
    return this.http.post<IParada>(this.resourceUrl, parada, { observe: 'response' });
  }

  update(parada: IParada): Observable<EntityResponseType> {
    return this.http.put<IParada>(this.resourceUrl, parada, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IParada>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IParada[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
