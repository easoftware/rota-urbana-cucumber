import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Parada } from 'app/shared/model/parada.model';
import { ParadaService } from './parada.service';
import { ParadaComponent } from './parada.component';
import { ParadaDetailComponent } from './parada-detail.component';
import { ParadaUpdateComponent } from './parada-update.component';
import { ParadaDeletePopupComponent } from './parada-delete-dialog.component';
import { IParada } from 'app/shared/model/parada.model';

@Injectable({ providedIn: 'root' })
export class ParadaResolve implements Resolve<IParada> {
  constructor(private service: ParadaService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IParada> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Parada>) => response.ok),
        map((parada: HttpResponse<Parada>) => parada.body)
      );
    }
    return of(new Parada());
  }
}

export const paradaRoute: Routes = [
  {
    path: '',
    component: ParadaComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'Paradas'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: ParadaDetailComponent,
    resolve: {
      parada: ParadaResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Paradas'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: ParadaUpdateComponent,
    resolve: {
      parada: ParadaResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Paradas'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: ParadaUpdateComponent,
    resolve: {
      parada: ParadaResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Paradas'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const paradaPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: ParadaDeletePopupComponent,
    resolve: {
      parada: ParadaResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Paradas'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
