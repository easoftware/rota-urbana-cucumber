import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { RotaurbanacucumberSharedModule } from 'app/shared';
import {
  ParadaComponent,
  ParadaDetailComponent,
  ParadaUpdateComponent,
  ParadaDeletePopupComponent,
  ParadaDeleteDialogComponent,
  paradaRoute,
  paradaPopupRoute
} from './';

const ENTITY_STATES = [...paradaRoute, ...paradaPopupRoute];

@NgModule({
  imports: [RotaurbanacucumberSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [ParadaComponent, ParadaDetailComponent, ParadaUpdateComponent, ParadaDeleteDialogComponent, ParadaDeletePopupComponent],
  entryComponents: [ParadaComponent, ParadaUpdateComponent, ParadaDeleteDialogComponent, ParadaDeletePopupComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class RotaurbanacucumberParadaModule {}
