import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IParada, Parada } from 'app/shared/model/parada.model';
import { ParadaService } from './parada.service';
import { ILinha } from 'app/shared/model/linha.model';
import { LinhaService } from 'app/entities/linha';

@Component({
  selector: 'jhi-parada-update',
  templateUrl: './parada-update.component.html'
})
export class ParadaUpdateComponent implements OnInit {
  isSaving: boolean;

  linhas: ILinha[];

  editForm = this.fb.group({
    id: [],
    latitude: [],
    longitude: [],
    status: [],
    title: [],
    comments: [],
    tipoDeRotaDaParada: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected paradaService: ParadaService,
    protected linhaService: LinhaService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ parada }) => {
      this.updateForm(parada);
    });
    this.linhaService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<ILinha[]>) => mayBeOk.ok),
        map((response: HttpResponse<ILinha[]>) => response.body)
      )
      .subscribe((res: ILinha[]) => (this.linhas = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(parada: IParada) {
    this.editForm.patchValue({
      id: parada.id,
      latitude: parada.latitude,
      longitude: parada.longitude,
      status: parada.status,
      title: parada.title,
      comments: parada.comments,
      tipoDeRotaDaParada: parada.tipoDeRotaDaParada
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const parada = this.createFromForm();
    if (parada.id !== undefined) {
      this.subscribeToSaveResponse(this.paradaService.update(parada));
    } else {
      this.subscribeToSaveResponse(this.paradaService.create(parada));
    }
  }

  private createFromForm(): IParada {
    const entity = {
      ...new Parada(),
      id: this.editForm.get(['id']).value,
      latitude: this.editForm.get(['latitude']).value,
      longitude: this.editForm.get(['longitude']).value,
      status: this.editForm.get(['status']).value,
      title: this.editForm.get(['title']).value,
      comments: this.editForm.get(['comments']).value,
      tipoDeRotaDaParada: this.editForm.get(['tipoDeRotaDaParada']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IParada>>) {
    result.subscribe((res: HttpResponse<IParada>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackLinhaById(index: number, item: ILinha) {
    return item.id;
  }

  getSelected(selectedVals: Array<any>, option: any) {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
