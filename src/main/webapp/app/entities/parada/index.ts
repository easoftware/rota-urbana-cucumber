export * from './parada.service';
export * from './parada-update.component';
export * from './parada-delete-dialog.component';
export * from './parada-detail.component';
export * from './parada.component';
export * from './parada.route';
