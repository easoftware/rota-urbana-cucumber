import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IParada } from 'app/shared/model/parada.model';
import { ParadaService } from './parada.service';

@Component({
  selector: 'jhi-parada-delete-dialog',
  templateUrl: './parada-delete-dialog.component.html'
})
export class ParadaDeleteDialogComponent {
  parada: IParada;

  constructor(protected paradaService: ParadaService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.paradaService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'paradaListModification',
        content: 'Deleted an parada'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-parada-delete-popup',
  template: ''
})
export class ParadaDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ parada }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(ParadaDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.parada = parada;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/parada', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/parada', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
