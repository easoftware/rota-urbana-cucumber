import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IParada } from 'app/shared/model/parada.model';

@Component({
  selector: 'jhi-parada-detail',
  templateUrl: './parada-detail.component.html'
})
export class ParadaDetailComponent implements OnInit {
  parada: IParada;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ parada }) => {
      this.parada = parada;
    });
  }

  previousState() {
    window.history.back();
  }
}
