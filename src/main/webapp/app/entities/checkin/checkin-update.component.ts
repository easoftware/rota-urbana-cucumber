import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { ICheckin, Checkin } from 'app/shared/model/checkin.model';
import { CheckinService } from './checkin.service';
import { ILinha } from 'app/shared/model/linha.model';
import { LinhaService } from 'app/entities/linha';

@Component({
  selector: 'jhi-checkin-update',
  templateUrl: './checkin-update.component.html'
})
export class CheckinUpdateComponent implements OnInit {
  isSaving: boolean;

  linhas: ILinha[];

  editForm = this.fb.group({
    id: [],
    latitude: [],
    longitude: [],
    linhaId: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected checkinService: CheckinService,
    protected linhaService: LinhaService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ checkin }) => {
      this.updateForm(checkin);
    });
    this.linhaService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<ILinha[]>) => mayBeOk.ok),
        map((response: HttpResponse<ILinha[]>) => response.body)
      )
      .subscribe((res: ILinha[]) => (this.linhas = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(checkin: ICheckin) {
    this.editForm.patchValue({
      id: checkin.id,
      latitude: checkin.latitude,
      longitude: checkin.longitude,
      linhaId: checkin.linhaId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const checkin = this.createFromForm();
    if (checkin.id !== undefined) {
      this.subscribeToSaveResponse(this.checkinService.update(checkin));
    } else {
      this.subscribeToSaveResponse(this.checkinService.create(checkin));
    }
  }

  private createFromForm(): ICheckin {
    const entity = {
      ...new Checkin(),
      id: this.editForm.get(['id']).value,
      latitude: this.editForm.get(['latitude']).value,
      longitude: this.editForm.get(['longitude']).value,
      linhaId: this.editForm.get(['linhaId']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICheckin>>) {
    result.subscribe((res: HttpResponse<ICheckin>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackLinhaById(index: number, item: ILinha) {
    return item.id;
  }
}
