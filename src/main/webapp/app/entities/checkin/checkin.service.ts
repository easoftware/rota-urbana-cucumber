import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ICheckin } from 'app/shared/model/checkin.model';

type EntityResponseType = HttpResponse<ICheckin>;
type EntityArrayResponseType = HttpResponse<ICheckin[]>;

@Injectable({ providedIn: 'root' })
export class CheckinService {
  public resourceUrl = SERVER_API_URL + 'api/checkins';

  constructor(protected http: HttpClient) {}

  create(checkin: ICheckin): Observable<EntityResponseType> {
    return this.http.post<ICheckin>(this.resourceUrl, checkin, { observe: 'response' });
  }

  update(checkin: ICheckin): Observable<EntityResponseType> {
    return this.http.put<ICheckin>(this.resourceUrl, checkin, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ICheckin>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICheckin[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
