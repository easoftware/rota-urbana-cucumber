import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Checkin } from 'app/shared/model/checkin.model';
import { CheckinService } from './checkin.service';
import { CheckinComponent } from './checkin.component';
import { CheckinDetailComponent } from './checkin-detail.component';
import { CheckinUpdateComponent } from './checkin-update.component';
import { CheckinDeletePopupComponent } from './checkin-delete-dialog.component';
import { ICheckin } from 'app/shared/model/checkin.model';

@Injectable({ providedIn: 'root' })
export class CheckinResolve implements Resolve<ICheckin> {
  constructor(private service: CheckinService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ICheckin> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Checkin>) => response.ok),
        map((checkin: HttpResponse<Checkin>) => checkin.body)
      );
    }
    return of(new Checkin());
  }
}

export const checkinRoute: Routes = [
  {
    path: '',
    component: CheckinComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'Checkins'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: CheckinDetailComponent,
    resolve: {
      checkin: CheckinResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Checkins'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: CheckinUpdateComponent,
    resolve: {
      checkin: CheckinResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Checkins'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: CheckinUpdateComponent,
    resolve: {
      checkin: CheckinResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Checkins'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const checkinPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: CheckinDeletePopupComponent,
    resolve: {
      checkin: CheckinResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Checkins'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
