import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { RotaurbanacucumberSharedModule } from 'app/shared';
import {
  CheckinComponent,
  CheckinDetailComponent,
  CheckinUpdateComponent,
  CheckinDeletePopupComponent,
  CheckinDeleteDialogComponent,
  checkinRoute,
  checkinPopupRoute
} from './';

const ENTITY_STATES = [...checkinRoute, ...checkinPopupRoute];

@NgModule({
  imports: [RotaurbanacucumberSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    CheckinComponent,
    CheckinDetailComponent,
    CheckinUpdateComponent,
    CheckinDeleteDialogComponent,
    CheckinDeletePopupComponent
  ],
  entryComponents: [CheckinComponent, CheckinUpdateComponent, CheckinDeleteDialogComponent, CheckinDeletePopupComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class RotaurbanacucumberCheckinModule {}
