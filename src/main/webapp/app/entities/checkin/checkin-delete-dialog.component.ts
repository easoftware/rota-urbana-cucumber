import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ICheckin } from 'app/shared/model/checkin.model';
import { CheckinService } from './checkin.service';

@Component({
  selector: 'jhi-checkin-delete-dialog',
  templateUrl: './checkin-delete-dialog.component.html'
})
export class CheckinDeleteDialogComponent {
  checkin: ICheckin;

  constructor(protected checkinService: CheckinService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.checkinService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'checkinListModification',
        content: 'Deleted an checkin'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-checkin-delete-popup',
  template: ''
})
export class CheckinDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ checkin }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(CheckinDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.checkin = checkin;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/checkin', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/checkin', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
