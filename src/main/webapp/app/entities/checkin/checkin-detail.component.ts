import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICheckin } from 'app/shared/model/checkin.model';

@Component({
  selector: 'jhi-checkin-detail',
  templateUrl: './checkin-detail.component.html'
})
export class CheckinDetailComponent implements OnInit {
  checkin: ICheckin;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ checkin }) => {
      this.checkin = checkin;
    });
  }

  previousState() {
    window.history.back();
  }
}
