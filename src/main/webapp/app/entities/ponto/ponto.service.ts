import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IPonto } from 'app/shared/model/ponto.model';

type EntityResponseType = HttpResponse<IPonto>;
type EntityArrayResponseType = HttpResponse<IPonto[]>;

@Injectable({ providedIn: 'root' })
export class PontoService {
  public resourceUrl = SERVER_API_URL + 'api/pontos';

  constructor(protected http: HttpClient) {}

  create(ponto: IPonto): Observable<EntityResponseType> {
    return this.http.post<IPonto>(this.resourceUrl, ponto, { observe: 'response' });
  }

  update(ponto: IPonto): Observable<EntityResponseType> {
    return this.http.put<IPonto>(this.resourceUrl, ponto, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IPonto>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IPonto[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
