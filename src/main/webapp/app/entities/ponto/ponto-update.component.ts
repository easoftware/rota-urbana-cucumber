import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { IPonto, Ponto } from 'app/shared/model/ponto.model';
import { PontoService } from './ponto.service';

@Component({
  selector: 'jhi-ponto-update',
  templateUrl: './ponto-update.component.html'
})
export class PontoUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    latitude: [],
    longitude: [],
    codigoAndroid: []
  });

  constructor(protected pontoService: PontoService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ ponto }) => {
      this.updateForm(ponto);
    });
  }

  updateForm(ponto: IPonto) {
    this.editForm.patchValue({
      id: ponto.id,
      latitude: ponto.latitude,
      longitude: ponto.longitude,
      codigoAndroid: ponto.codigoAndroid
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const ponto = this.createFromForm();
    if (ponto.id !== undefined) {
      this.subscribeToSaveResponse(this.pontoService.update(ponto));
    } else {
      this.subscribeToSaveResponse(this.pontoService.create(ponto));
    }
  }

  private createFromForm(): IPonto {
    const entity = {
      ...new Ponto(),
      id: this.editForm.get(['id']).value,
      latitude: this.editForm.get(['latitude']).value,
      longitude: this.editForm.get(['longitude']).value,
      codigoAndroid: this.editForm.get(['codigoAndroid']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPonto>>) {
    result.subscribe((res: HttpResponse<IPonto>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
