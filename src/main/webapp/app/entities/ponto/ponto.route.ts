import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Ponto } from 'app/shared/model/ponto.model';
import { PontoService } from './ponto.service';
import { PontoComponent } from './ponto.component';
import { PontoDetailComponent } from './ponto-detail.component';
import { PontoUpdateComponent } from './ponto-update.component';
import { PontoDeletePopupComponent } from './ponto-delete-dialog.component';
import { IPonto } from 'app/shared/model/ponto.model';

@Injectable({ providedIn: 'root' })
export class PontoResolve implements Resolve<IPonto> {
  constructor(private service: PontoService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IPonto> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Ponto>) => response.ok),
        map((ponto: HttpResponse<Ponto>) => ponto.body)
      );
    }
    return of(new Ponto());
  }
}

export const pontoRoute: Routes = [
  {
    path: '',
    component: PontoComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'Pontos'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: PontoDetailComponent,
    resolve: {
      ponto: PontoResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Pontos'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: PontoUpdateComponent,
    resolve: {
      ponto: PontoResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Pontos'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: PontoUpdateComponent,
    resolve: {
      ponto: PontoResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Pontos'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const pontoPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: PontoDeletePopupComponent,
    resolve: {
      ponto: PontoResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Pontos'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
