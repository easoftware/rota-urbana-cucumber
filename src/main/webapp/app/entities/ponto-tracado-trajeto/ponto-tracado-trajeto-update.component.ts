import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IPontoTracadoTrajeto, PontoTracadoTrajeto } from 'app/shared/model/ponto-tracado-trajeto.model';
import { PontoTracadoTrajetoService } from './ponto-tracado-trajeto.service';
import { ILinha } from 'app/shared/model/linha.model';
import { LinhaService } from 'app/entities/linha';

@Component({
  selector: 'jhi-ponto-tracado-trajeto-update',
  templateUrl: './ponto-tracado-trajeto-update.component.html'
})
export class PontoTracadoTrajetoUpdateComponent implements OnInit {
  isSaving: boolean;

  linhas: ILinha[];

  editForm = this.fb.group({
    id: [],
    latitude: [],
    longitude: [],
    posicao: [],
    tipo: [],
    linhaId: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected pontoTracadoTrajetoService: PontoTracadoTrajetoService,
    protected linhaService: LinhaService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ pontoTracadoTrajeto }) => {
      this.updateForm(pontoTracadoTrajeto);
    });
    this.linhaService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<ILinha[]>) => mayBeOk.ok),
        map((response: HttpResponse<ILinha[]>) => response.body)
      )
      .subscribe((res: ILinha[]) => (this.linhas = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(pontoTracadoTrajeto: IPontoTracadoTrajeto) {
    this.editForm.patchValue({
      id: pontoTracadoTrajeto.id,
      latitude: pontoTracadoTrajeto.latitude,
      longitude: pontoTracadoTrajeto.longitude,
      posicao: pontoTracadoTrajeto.posicao,
      tipo: pontoTracadoTrajeto.tipo,
      linhaId: pontoTracadoTrajeto.linhaId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const pontoTracadoTrajeto = this.createFromForm();
    if (pontoTracadoTrajeto.id !== undefined) {
      this.subscribeToSaveResponse(this.pontoTracadoTrajetoService.update(pontoTracadoTrajeto));
    } else {
      this.subscribeToSaveResponse(this.pontoTracadoTrajetoService.create(pontoTracadoTrajeto));
    }
  }

  private createFromForm(): IPontoTracadoTrajeto {
    const entity = {
      ...new PontoTracadoTrajeto(),
      id: this.editForm.get(['id']).value,
      latitude: this.editForm.get(['latitude']).value,
      longitude: this.editForm.get(['longitude']).value,
      posicao: this.editForm.get(['posicao']).value,
      tipo: this.editForm.get(['tipo']).value,
      linhaId: this.editForm.get(['linhaId']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPontoTracadoTrajeto>>) {
    result.subscribe((res: HttpResponse<IPontoTracadoTrajeto>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackLinhaById(index: number, item: ILinha) {
    return item.id;
  }
}
