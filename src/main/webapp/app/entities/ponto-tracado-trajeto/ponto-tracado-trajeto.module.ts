import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { RotaurbanacucumberSharedModule } from 'app/shared';
import {
  PontoTracadoTrajetoComponent,
  PontoTracadoTrajetoDetailComponent,
  PontoTracadoTrajetoUpdateComponent,
  PontoTracadoTrajetoDeletePopupComponent,
  PontoTracadoTrajetoDeleteDialogComponent,
  pontoTracadoTrajetoRoute,
  pontoTracadoTrajetoPopupRoute
} from './';

const ENTITY_STATES = [...pontoTracadoTrajetoRoute, ...pontoTracadoTrajetoPopupRoute];

@NgModule({
  imports: [RotaurbanacucumberSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    PontoTracadoTrajetoComponent,
    PontoTracadoTrajetoDetailComponent,
    PontoTracadoTrajetoUpdateComponent,
    PontoTracadoTrajetoDeleteDialogComponent,
    PontoTracadoTrajetoDeletePopupComponent
  ],
  entryComponents: [
    PontoTracadoTrajetoComponent,
    PontoTracadoTrajetoUpdateComponent,
    PontoTracadoTrajetoDeleteDialogComponent,
    PontoTracadoTrajetoDeletePopupComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class RotaurbanacucumberPontoTracadoTrajetoModule {}
