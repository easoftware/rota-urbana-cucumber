export * from './ponto-tracado-trajeto.service';
export * from './ponto-tracado-trajeto-update.component';
export * from './ponto-tracado-trajeto-delete-dialog.component';
export * from './ponto-tracado-trajeto-detail.component';
export * from './ponto-tracado-trajeto.component';
export * from './ponto-tracado-trajeto.route';
