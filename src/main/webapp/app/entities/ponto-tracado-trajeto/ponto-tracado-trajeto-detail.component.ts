import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IPontoTracadoTrajeto } from 'app/shared/model/ponto-tracado-trajeto.model';

@Component({
  selector: 'jhi-ponto-tracado-trajeto-detail',
  templateUrl: './ponto-tracado-trajeto-detail.component.html'
})
export class PontoTracadoTrajetoDetailComponent implements OnInit {
  pontoTracadoTrajeto: IPontoTracadoTrajeto;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ pontoTracadoTrajeto }) => {
      this.pontoTracadoTrajeto = pontoTracadoTrajeto;
    });
  }

  previousState() {
    window.history.back();
  }
}
