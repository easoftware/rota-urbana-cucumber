import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { PontoTracadoTrajeto } from 'app/shared/model/ponto-tracado-trajeto.model';
import { PontoTracadoTrajetoService } from './ponto-tracado-trajeto.service';
import { PontoTracadoTrajetoComponent } from './ponto-tracado-trajeto.component';
import { PontoTracadoTrajetoDetailComponent } from './ponto-tracado-trajeto-detail.component';
import { PontoTracadoTrajetoUpdateComponent } from './ponto-tracado-trajeto-update.component';
import { PontoTracadoTrajetoDeletePopupComponent } from './ponto-tracado-trajeto-delete-dialog.component';
import { IPontoTracadoTrajeto } from 'app/shared/model/ponto-tracado-trajeto.model';

@Injectable({ providedIn: 'root' })
export class PontoTracadoTrajetoResolve implements Resolve<IPontoTracadoTrajeto> {
  constructor(private service: PontoTracadoTrajetoService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IPontoTracadoTrajeto> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<PontoTracadoTrajeto>) => response.ok),
        map((pontoTracadoTrajeto: HttpResponse<PontoTracadoTrajeto>) => pontoTracadoTrajeto.body)
      );
    }
    return of(new PontoTracadoTrajeto());
  }
}

export const pontoTracadoTrajetoRoute: Routes = [
  {
    path: '',
    component: PontoTracadoTrajetoComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'PontoTracadoTrajetos'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: PontoTracadoTrajetoDetailComponent,
    resolve: {
      pontoTracadoTrajeto: PontoTracadoTrajetoResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'PontoTracadoTrajetos'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: PontoTracadoTrajetoUpdateComponent,
    resolve: {
      pontoTracadoTrajeto: PontoTracadoTrajetoResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'PontoTracadoTrajetos'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: PontoTracadoTrajetoUpdateComponent,
    resolve: {
      pontoTracadoTrajeto: PontoTracadoTrajetoResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'PontoTracadoTrajetos'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const pontoTracadoTrajetoPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: PontoTracadoTrajetoDeletePopupComponent,
    resolve: {
      pontoTracadoTrajeto: PontoTracadoTrajetoResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'PontoTracadoTrajetos'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
