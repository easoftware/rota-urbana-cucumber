import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IPontoTracadoTrajeto } from 'app/shared/model/ponto-tracado-trajeto.model';
import { PontoTracadoTrajetoService } from './ponto-tracado-trajeto.service';

@Component({
  selector: 'jhi-ponto-tracado-trajeto-delete-dialog',
  templateUrl: './ponto-tracado-trajeto-delete-dialog.component.html'
})
export class PontoTracadoTrajetoDeleteDialogComponent {
  pontoTracadoTrajeto: IPontoTracadoTrajeto;

  constructor(
    protected pontoTracadoTrajetoService: PontoTracadoTrajetoService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.pontoTracadoTrajetoService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'pontoTracadoTrajetoListModification',
        content: 'Deleted an pontoTracadoTrajeto'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-ponto-tracado-trajeto-delete-popup',
  template: ''
})
export class PontoTracadoTrajetoDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ pontoTracadoTrajeto }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(PontoTracadoTrajetoDeleteDialogComponent as Component, {
          size: 'lg',
          backdrop: 'static'
        });
        this.ngbModalRef.componentInstance.pontoTracadoTrajeto = pontoTracadoTrajeto;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/ponto-tracado-trajeto', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/ponto-tracado-trajeto', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
