import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'checkin',
        loadChildren: './checkin/checkin.module#RotaurbanacucumberCheckinModule'
      },
      {
        path: 'ponto',
        loadChildren: './ponto/ponto.module#RotaurbanacucumberPontoModule'
      },
      {
        path: 'cidade',
        loadChildren: './cidade/cidade.module#RotaurbanacucumberCidadeModule'
      },
      {
        path: 'estado',
        loadChildren: './estado/estado.module#RotaurbanacucumberEstadoModule'
      },
      {
        path: 'linha',
        loadChildren: './linha/linha.module#RotaurbanacucumberLinhaModule'
      },
      {
        path: 'pais',
        loadChildren: './pais/pais.module#RotaurbanacucumberPaisModule'
      },
      {
        path: 'parada',
        loadChildren: './parada/parada.module#RotaurbanacucumberParadaModule'
      },
      {
        path: 'ponto-tracado-trajeto',
        loadChildren: './ponto-tracado-trajeto/ponto-tracado-trajeto.module#RotaurbanacucumberPontoTracadoTrajetoModule'
      },
      {
        path: 'ponto-pesquisa',
        loadChildren: './ponto-pesquisa/ponto-pesquisa.module#RotaurbanacucumberPontoPesquisaModule'
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ],
  declarations: [],
  entryComponents: [],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class RotaurbanacucumberEntityModule {}
