import { ILinha } from 'app/shared/model/linha.model';

export interface IParada {
  id?: number;
  latitude?: number;
  longitude?: number;
  status?: number;
  title?: string;
  comments?: string;
  tipoDeRotaDaParada?: string;
  linhas?: ILinha[];
}

export class Parada implements IParada {
  constructor(
    public id?: number,
    public latitude?: number,
    public longitude?: number,
    public status?: number,
    public title?: string,
    public comments?: string,
    public tipoDeRotaDaParada?: string,
    public linhas?: ILinha[]
  ) {}
}
