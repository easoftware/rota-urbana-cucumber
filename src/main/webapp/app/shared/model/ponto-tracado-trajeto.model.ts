export const enum Tipo {
  IDA = 'IDA',
  VOLTA = 'VOLTA',
  TRACADO = 'TRACADO'
}

export interface IPontoTracadoTrajeto {
  id?: number;
  latitude?: number;
  longitude?: number;
  posicao?: number;
  tipo?: Tipo;
  linhaId?: number;
}

export class PontoTracadoTrajeto implements IPontoTracadoTrajeto {
  constructor(
    public id?: number,
    public latitude?: number,
    public longitude?: number,
    public posicao?: number,
    public tipo?: Tipo,
    public linhaId?: number
  ) {}
}
