import { Moment } from 'moment';
import { IPontoTracadoTrajeto } from 'app/shared/model/ponto-tracado-trajeto.model';
import { IPontoPesquisa } from 'app/shared/model/ponto-pesquisa.model';
import { IParada } from 'app/shared/model/parada.model';
import { ILinha } from 'app/shared/model/linha.model';

export const enum TipoDeRota {
  ONIBUS = 'ONIBUS',
  CIRCUITO = 'CIRCUITO',
  PATINS = 'PATINS',
  BICICLETA = 'BICICLETA',
  FLUVIAL = 'FLUVIAL',
  INTERMUNICIPAL = 'INTERMUNICIPAL'
}

export interface ILinha {
  id?: number;
  nome?: string;
  codigo?: string;
  url?: string;
  completa?: boolean;
  lastUpdate?: Moment;
  comentario?: string;
  itinerarioTotalEncoding?: string;
  emAvaliacao?: boolean;
  faltaCadastrarPontosPesquisa?: boolean;
  semob?: boolean;
  tipoDeRota?: TipoDeRota;
  itinerarios?: IPontoTracadoTrajeto[];
  pontosPesquisas?: IPontoPesquisa[];
  cidadeNome?: string;
  cidadeId?: number;
  rootId?: number;
  paradas?: IParada[];
  children?: ILinha[];
}

export class Linha implements ILinha {
  constructor(
    public id?: number,
    public nome?: string,
    public codigo?: string,
    public url?: string,
    public completa?: boolean,
    public lastUpdate?: Moment,
    public comentario?: string,
    public itinerarioTotalEncoding?: string,
    public emAvaliacao?: boolean,
    public faltaCadastrarPontosPesquisa?: boolean,
    public semob?: boolean,
    public tipoDeRota?: TipoDeRota,
    public itinerarios?: IPontoTracadoTrajeto[],
    public pontosPesquisas?: IPontoPesquisa[],
    public cidadeNome?: string,
    public cidadeId?: number,
    public rootId?: number,
    public paradas?: IParada[],
    public children?: ILinha[]
  ) {
    this.completa = this.completa || false;
    this.emAvaliacao = this.emAvaliacao || false;
    this.faltaCadastrarPontosPesquisa = this.faltaCadastrarPontosPesquisa || false;
    this.semob = this.semob || false;
  }
}
