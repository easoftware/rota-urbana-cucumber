export interface ICheckin {
  id?: number;
  latitude?: number;
  longitude?: number;
  linhaNome?: string;
  linhaId?: number;
}

export class Checkin implements ICheckin {
  constructor(
    public id?: number,
    public latitude?: number,
    public longitude?: number,
    public linhaNome?: string,
    public linhaId?: number
  ) {}
}
