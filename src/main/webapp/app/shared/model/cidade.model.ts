export interface ICidade {
  id?: number;
  nome?: string;
  latitude?: string;
  longitude?: string;
  latitudeDouble?: number;
  longitudeDouble?: number;
  sameAs?: string;
  belongsToNome?: string;
  belongsToId?: number;
  estadoNome?: string;
  estadoId?: number;
}

export class Cidade implements ICidade {
  constructor(
    public id?: number,
    public nome?: string,
    public latitude?: string,
    public longitude?: string,
    public latitudeDouble?: number,
    public longitudeDouble?: number,
    public sameAs?: string,
    public belongsToNome?: string,
    public belongsToId?: number,
    public estadoNome?: string,
    public estadoId?: number
  ) {}
}
