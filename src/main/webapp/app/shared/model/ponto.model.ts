export interface IPonto {
  id?: number;
  latitude?: number;
  longitude?: number;
  codigoAndroid?: string;
}

export class Ponto implements IPonto {
  constructor(public id?: number, public latitude?: number, public longitude?: number, public codigoAndroid?: string) {}
}
