export interface IEstado {
  id?: number;
  nome?: string;
  uf?: string;
  paisNome?: string;
  paisId?: number;
}

export class Estado implements IEstado {
  constructor(public id?: number, public nome?: string, public uf?: string, public paisNome?: string, public paisId?: number) {}
}
