import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RotaurbanacucumberSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective } from './';

@NgModule({
  imports: [RotaurbanacucumberSharedCommonModule],
  declarations: [JhiLoginModalComponent, HasAnyAuthorityDirective],
  entryComponents: [JhiLoginModalComponent],
  exports: [RotaurbanacucumberSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class RotaurbanacucumberSharedModule {
  static forRoot() {
    return {
      ngModule: RotaurbanacucumberSharedModule
    };
  }
}
