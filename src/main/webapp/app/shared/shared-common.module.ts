import { NgModule } from '@angular/core';

import { RotaurbanacucumberSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
  imports: [RotaurbanacucumberSharedLibsModule],
  declarations: [JhiAlertComponent, JhiAlertErrorComponent],
  exports: [RotaurbanacucumberSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class RotaurbanacucumberSharedCommonModule {}
